package com.walmart.supervisor.model.order;

import com.walmart.supervisor.model.product.Product;

import java.io.Serializable;

public class OrderLineId implements Serializable {
    public int OrderLineId;
    public int OrderId;
    public int ProductId;
    public int VariantProductId;
    public boolean IsParentProduct;
    public String Description;
    public int Quantity;
    public float ShippingCost;
    public float ProductPrice;
    public float TotalPromotionDiscount;
    public String ReturnReason;
    public String ReturnAction;
    public String StockAction;
    public int ReturnQty;
    public boolean IsBackOrder;
    public float TotalVoucherDiscount;
    public float TotalTaxAmount;
    public float ShippingVoucherDiscount;
    public String CustomFields;
    public int VariantMasterProductId;
    public String LocationCode;
    public int LocationId;
    public String ShippingStatus;
    public String DeliveryMode;
    public String VendorId;
    public String ItemStatus;
    public String SKU;
    public String VariantSku;
    public String ProductTitle;
    public String BundleProductId;
    public String ParentReDetailsId;
    public String ImageUrl;
    public String IsPrimaryProduct;
    public String Portion;
    public String MRP;
    public String CategoryName, CategoryId;
    public int CancelQuantity;
    public boolean IsRemoved = false;
    public int UpdatedQuantity;
    public float UpdatedPrice;
    public boolean IsAdded = false;
    public String DerivedStatus;


    public Product product;


}
