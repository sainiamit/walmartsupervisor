package com.walmart.supervisor.model.order;


import com.walmart.supervisor.model.LedgerBalanceResponse;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class Orders implements Serializable {
    public int OrderId;
    public String Status;
    //public String sku;
    //TODO
    //public String shipdate;
    //TODO
    public String MerchantId;
    public String UserId;
    public float TotalAmount;
    public float AmountPayable;
    public String OrderDate;
    public String BillFirstname;
    public String BillLastname;
    public String BillCountry;
    public String BillingState;
    public String BillCity;
    public String BillEmail;
    public String BillZIP;
    public String BillPhone;
    public String BillMobile;
    public String BillAddress1;
    public String ShipFirstname;
    public String ShipLastname;
    public String ShipCountry;
    public String ShipState;
    public String ShipCity;
    public String ShipEmail;
    public String ShipZip;
    public String ShipPhone;
    public String ShipMobile;
    public String ShipAddress1;
    public String ShipOtherCity;
    public String DeliveryOption;
    public float TAXTotal;
    public String VoucherCode;
    public String LeadTime;
    public String BillAddress2;
    public String ShipAddress2;
    public boolean IsGift;
    public String SupplierID;
    public String ConversionFactor;
    public String cpuserid;
    public List<OrderLineId> OrderLineId;
    public List<PaymentDetails> PaymentDetails;
    //public String Rewards;
    //TODO
    public float ShippingDiscount;
    public float VoucherDiscount;
    public float PromotionDiscount;
    public String OriginalOrderId;
    public String ReturnOrderId;
    public String ReferenceNo;
    public String DemandedDeliveryDate;
    public Deliveryslots deliveryslots;
    public String GiftMessage;
    //TODO
    public String LanguageCode;
    public String ShipCityCode;
    public String ShipStateCode;
    public String BillCityCode;
    public String ShipCountryCode;
    public String BillStateCode;
    public String RefundAmount;
    public String PickupFirstName;
    public String PickupLastName;
    public String PickupEmail;
    public String PickupMobile;
    public String Latitude;
    public String Longitude;
    public String ShippingMode;
    public String ChannelID;
    public String IsSelfShip;
    public String ChannelOrderID;
    public String BDAComments;
    public int LineItemCount;
    //public String Promotions;
    //TODO
    public LedgerBalanceResponse ledgerBalanceResponse;
}

