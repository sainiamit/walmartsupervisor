package com.walmart.supervisor.model.product;


import com.walmart.supervisor.model.slabprice.VolumetricPricing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by riteshdubey on 1/16/17.
 */
public class Product implements Serializable {
    public int StoreID;
    public int ID;
    public int RelatedProductID;
    public String TagID;
    public String Brand;
    public String BrandID;
    public String DeliveryMode;
    public String H1CategoryName;
    public String H2CategoryName;
    public String H3CategoryName;
    public String H4CategoryName;
    public String Hierarchy1CategoryID;
    public String Hierarchy2CategoryID;
    public String Hierarchy3CategoryID;
    public String Hierarchy4CategoryID;
    public String LargeImage;
    public String LongDescription;
    public float MRP;
    public String Title;
    public String ShortDescription;
    public String SmallImage;
    public String VariantProperty;
    public float WebPrice;
    public float Price;
    public float Discount;
    public int Inventory;
    public Map<Integer, Variant> Variants;
    public ArrayList<Attribute> Attributes;
    public ArrayList<MultipleImage> MultipleImages;
    public String Sku;
    public String OfferDescription;
    public String Tags;
    public int AttributeCount;
    public int VariantProductIDSelected = 0;
    public VariantProduct VariantProduct;
    public ArrayList<VariantProduct> VariantProductList;
    public ArrayList<VariantProperty> VariantProperties;
    public List<VolumetricPricing> VolumetricPricing = new ArrayList<VolumetricPricing>();
    //    public String BundleGroups;
    //TODO
    public int PrimaryProductId;
    public String ProductType;
    public int Sequence;

    public String selectedVariantName;
    public int orderId;
}
