package com.walmart.supervisor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by M1045813 on 7/20/2018.
 */

public class AddOrderLineItemRequest {
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("SKU")
    @Expose
    private String sKU;
    @SerializedName("VarientSKU")
    @Expose
    private String varientSKU;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("LocationCode")
    @Expose
    private String locationCode;
    @SerializedName("DeliveryMode")
    @Expose
    private String deliveryMode;
    @SerializedName("ItemAction")
    @Expose
    private Integer itemAction;

    @SerializedName("ProductID")
    @Expose
    private Integer productid;

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSKU() {
        return sKU;
    }

    public void setSKU(String sKU) {
        this.sKU = sKU;
    }

    public String getVarientSKU() {
        return varientSKU;
    }

    public void setVarientSKU(String varientSKU) {
        this.varientSKU = varientSKU;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Integer getItemAction() {
        return itemAction;
    }

    public void setItemAction(Integer itemAction) {
        this.itemAction = itemAction;
    }
}
