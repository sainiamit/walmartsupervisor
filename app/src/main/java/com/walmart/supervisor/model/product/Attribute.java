package com.walmart.supervisor.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by riteshdubey on 1/18/17.
 */
public class Attribute implements Parcelable, Serializable {
    public String Name;
    public String Value;
    public String Group;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Name);
        dest.writeString(this.Value);
        dest.writeString(this.Group);
    }

    public Attribute() {
    }

    protected Attribute(Parcel in) {
        this.Name = in.readString();
        this.Value = in.readString();
        this.Group = in.readString();
    }

    public static final Creator<Attribute> CREATOR = new Creator<Attribute>() {
        @Override
        public Attribute createFromParcel(Parcel source) {
            return new Attribute(source);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };
}
