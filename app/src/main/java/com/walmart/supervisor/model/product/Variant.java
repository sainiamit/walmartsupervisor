package com.walmart.supervisor.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by riteshdubey on 1/16/17.
 */

public class Variant implements Parcelable, Serializable {
    public int VariantProductID;
    public int RelatedVariantProductID;
    public float MRP;
    public float WebPrice;
    public float Price;
    public int Inventory;
    public String Value;
    public float Discount;
    public String Description;
    public String SmallImage;
    public String VariantProperty;
    public static Comparator<Variant> WebpriceComparator = new Comparator<Variant>() {

        public int compare(Variant s1, Variant s2) {
            Float Weprice1 = s1.getWebprice();
            Float Weprice2 = s2.getWebprice();

            //ascending order
            return Weprice1.compareTo(Weprice2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }};

    public Float getWebprice() {
        return WebPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.VariantProductID);
        dest.writeInt(this.RelatedVariantProductID);
        dest.writeFloat(this.MRP);
        dest.writeFloat(this.WebPrice);
        dest.writeFloat(this.Price);
        dest.writeInt(this.Inventory);
        dest.writeString(this.Value);
        dest.writeFloat(this.Discount);
        dest.writeString(this.Description);
        dest.writeString(this.SmallImage);
        dest.writeString(this.VariantProperty);
    }

    public Variant() {
    }

    protected Variant(Parcel in) {
        this.VariantProductID = in.readInt();
        this.RelatedVariantProductID = in.readInt();
        this.MRP = in.readFloat();
        this.WebPrice = in.readFloat();
        this.Price = in.readFloat();
        this.Inventory = in.readInt();
        this.Value = in.readString();
        this.Discount = in.readFloat();
        this.Description = in.readString();
        this.SmallImage = in.readString();
        this.VariantProperty = in.readString();
    }

    public static final Creator<Variant> CREATOR = new Creator<Variant>() {
        @Override
        public Variant createFromParcel(Parcel source) {
            return new Variant(source);
        }

        @Override
        public Variant[] newArray(int size) {
            return new Variant[size];
        }
    };
}
