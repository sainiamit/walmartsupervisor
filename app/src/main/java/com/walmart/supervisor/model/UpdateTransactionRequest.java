package com.walmart.supervisor.model;

/**
 * Created by M1045813 on 9/27/2018.
 */

public class UpdateTransactionRequest {
    public Transaction Transaction;

    public static class Transaction {
        public String OrderId, PaymentStatus, RespCode, CurrencyCode, TxnID, RespMsg, PaymentType, DeliverOption, GatewayOption,Amount;
    }
}
