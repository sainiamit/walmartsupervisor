package com.walmart.supervisor.model;

/**
 * Created by M1045813 on 7/14/2018.
 */

public class CancelResponse {
    public String messageCode;
    public String Message;

    @Override
    public String toString() {
        return "CancelResponse{" +
                "messageCode='" + messageCode + '\'' +
                ", Message='" + Message + '\'' +
                '}';
    }
}
