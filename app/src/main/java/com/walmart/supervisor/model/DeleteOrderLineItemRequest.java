package com.walmart.supervisor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by M1045813 on 7/20/2018.
 */

public class DeleteOrderLineItemRequest {
    @SerializedName("merchantid")
    @Expose
    private String merchantid;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("PGResponse")
    @Expose
    private String pGResponse;
    @SerializedName("OperatorID")
    @Expose
    private String operatorID;
    @SerializedName("DisplayCommentToUser")
    @Expose
    private String displayCommentToUser;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("CancelReason")
    @Expose
    private String cancelReason;
    @SerializedName("TobeCancelledOrderItems")
    @Expose
    private List<TobeCancelledOrderItem> tobeCancelledOrderItems = null;

    public String getMerchantid() {
        return merchantid;
    }

    public void setMerchantid(String merchantid) {
        this.merchantid = merchantid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPGResponse() {
        return pGResponse;
    }

    public void setPGResponse(String pGResponse) {
        this.pGResponse = pGResponse;
    }

    public String getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }

    public String getDisplayCommentToUser() {
        return displayCommentToUser;
    }

    public void setDisplayCommentToUser(String displayCommentToUser) {
        this.displayCommentToUser = displayCommentToUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public List<TobeCancelledOrderItem> getTobeCancelledOrderItems() {
        return tobeCancelledOrderItems;
    }

    public void setTobeCancelledOrderItems(List<TobeCancelledOrderItem> tobeCancelledOrderItems) {
        this.tobeCancelledOrderItems = tobeCancelledOrderItems;
    }


    public static class TobeCancelledOrderItem {
        @SerializedName("OrderItemID")
        @Expose
        private Integer orderItemID;
        @SerializedName("CancelQuantity")
        @Expose
        private Integer cancelQuantity;

        public Integer getOrderItemID() {
            return orderItemID;
        }

        public void setOrderItemID(Integer orderItemID) {
            this.orderItemID = orderItemID;
        }

        public Integer getCancelQuantity() {
            return cancelQuantity;
        }

        public void setCancelQuantity(Integer cancelQuantity) {
            this.cancelQuantity = cancelQuantity;
        }
    }
}
