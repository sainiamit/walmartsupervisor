package com.walmart.supervisor.model.slabprice;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by M1045813 on 7/11/2018.
 */

public class VolumetricLists {

    @SerializedName("VolumetricPricing")
    public List<VolumetricPricing> volumetricPricing = new ArrayList<>();

    @SerializedName("MessageCode")
    public String MessageCode;

    @SerializedName("Message")
    public String Message;

}