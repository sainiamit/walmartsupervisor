package com.walmart.supervisor.model;

/**
 * Created by M1044639 on 01-Aug-18.
 */

public class EditOrderLineItemRequest {
    public String OrderID, OrderItemID, Price, MRP, ConsiderCurrentPriceandMRP, ReasonForPriceOverride, ItemAction;
}
