package com.walmart.supervisor.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by riteshdubey on 1/18/17.
 */
public class MultipleImage implements Parcelable, Serializable {
    public String Sequence;
    public String LargeImage;
    public int VariantProductId;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Sequence);
        dest.writeString(this.LargeImage);
    }

    public MultipleImage() {
    }

    public MultipleImage(String largeImage) {
        LargeImage = largeImage;
    }

    protected MultipleImage(Parcel in) {
        this.Sequence = in.readString();
        this.LargeImage = in.readString();
    }

    public static final Creator<MultipleImage> CREATOR = new Creator<MultipleImage>() {
        @Override
        public MultipleImage createFromParcel(Parcel source) {
            return new MultipleImage(source);
        }

        @Override
        public MultipleImage[] newArray(int size) {
            return new MultipleImage[size];
        }
    };
}
