package com.walmart.supervisor.model.login;

import java.util.List;

/**
 * Created by M1027564 on 6/26/2018.
 */

public class AgentDetails {

    public String AgentId;
    public String Email;
    public String FirstName;
    public String LastName;
    public List<LocationDetails> LocationDetails;
    public List<RoleDetails> RoleDetails;
}
