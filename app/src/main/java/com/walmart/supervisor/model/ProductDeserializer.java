package com.walmart.supervisor.model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.model.product.VariantProduct;
import com.walmart.supervisor.utils.GsonHelper;

import java.lang.reflect.Type;

/**
 * Created by riteshdubey on 3/30/17.
 */

public class ProductDeserializer implements JsonDeserializer<Product> {

    @Override
    public Product deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Product Product = new Gson().fromJson(json, Product.class);
        JsonObject jsonObject = json.getAsJsonObject();

        if (jsonObject.has("VariantProducts")) {
            JsonElement elem = jsonObject.get("VariantProducts");
            if (elem != null && !elem.isJsonNull() && elem.isJsonArray()) {
                String valuesString = elem.toString();
                if (!TextUtils.isEmpty(valuesString)) {
                    Product.VariantProductList = GsonHelper.getVariantProductList(valuesString);
                }
            } else if (elem != null && !elem.isJsonNull() && !elem.isJsonArray()) {
                String valuesString = elem.toString();
                if (!TextUtils.isEmpty(valuesString)) {
                    Product.VariantProduct = (VariantProduct) GsonHelper.getGson(valuesString, VariantProduct.class);
                }
            }
        }
        return Product;
    }
}
