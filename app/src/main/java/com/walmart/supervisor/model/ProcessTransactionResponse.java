package com.walmart.supervisor.model;

import java.util.List;

/**
 * Created by srihari on 28/11/2017.
 */

public class ProcessTransactionResponse {
    public Transaction Transaction;
    public String Message;
    public String messageCode;

    public static class Transaction {
        public int TransactionID; // PreTransaction
        public float Amount; // Amount in Rupees
        public List<PaymentMessage> PaymentMessage; // PreTransaction
    }

    public static class PaymentMessage {
        public String Key, Value;
    }
}
