package com.walmart.supervisor.model;

/**
 * Created by riteshdubey on 2/19/17.
 */

public class CreateShoppingList {

    public String UserId;
    public String ShoppingListName;
    public String privacyLevel;
    public String RefCode;

    public CreateShoppingList(String shoppingListName, String privacyLevel, String refCode) {
        ShoppingListName = shoppingListName;
        this.privacyLevel = privacyLevel;
        RefCode = refCode;
    }
}
