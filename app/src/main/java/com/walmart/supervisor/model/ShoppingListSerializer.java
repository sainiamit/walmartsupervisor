package com.walmart.supervisor.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by riteshdubey on 3/2/17.
 */

public class ShoppingListSerializer implements JsonSerializer<CreateShoppingList> {

    @Override
    public JsonElement serialize(CreateShoppingList src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.add("UserId", context.serialize(src.UserId));
        object.add("ShoppingListName", context.serialize(src.ShoppingListName));
        object.add("privacyLevel", context.serialize(src.privacyLevel));
        object.add("RefCode", context.serialize(src.RefCode));
        return object;
    }
}
