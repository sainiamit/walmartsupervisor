package com.walmart.supervisor.model.product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by riteshdubey on 12/30/16.
 */
public class SearchRefine {
    public String MessageCode;
    public String Message;
    public ArrayList<Product> Products = new ArrayList<>();
    public int TotalRecords;

    public List<Product> getProductList() {
        if (Products != null)
            return Products;
        else
            return new ArrayList<>();
    }
}
