package com.walmart.supervisor.model;

import com.walmart.supervisor.model.login.AgentDetails;

/**
 * Created by Srihari on 08/06/2018.
 */

public class LoginResponse {
    public String messageCode;
    public String Message;
    public AccessToken Token;
    public AgentDetails AgentDetails;


    @Override
    public String toString() {
        return " MessageCode "+messageCode+" Message "+Message;
    }
}
