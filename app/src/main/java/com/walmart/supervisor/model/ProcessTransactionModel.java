package com.walmart.supervisor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by M1045813 on 9/27/2018.
 */

public class ProcessTransactionModel {
    @SerializedName("Transaction")
    @Expose
    private Transaction transaction;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public static class Transaction {

        @SerializedName("GatewayID")
        @Expose
        private String gatewayID;
        @SerializedName("MerchantID")
        @Expose
        private String MerchantID;
        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("OrderID")
        @Expose
        private Integer orderID;
        @SerializedName("PaymentDetails")
        @Expose
        private List<PaymentDetail> paymentDetails = null;
        @SerializedName("PaymentType")
        @Expose
        private String paymentType;
        @SerializedName("Provider")
        @Expose
        private String provider;

        public String getGatewayID() {
            return gatewayID;
        }

        public void setGatewayID(String gatewayID) {
            this.gatewayID = gatewayID;
        }

        public String getID() {
            return iD;
        }

        public void setID(String iD) {
            this.iD = iD;
        }

        public Integer getOrderID() {
            return orderID;
        }

        public void setOrderID(Integer orderID) {
            this.orderID = orderID;
        }

        public List<PaymentDetail> getPaymentDetails() {
            return paymentDetails;
        }

        public void setPaymentDetails(List<PaymentDetail> paymentDetails) {
            this.paymentDetails = paymentDetails;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getMerchantID() {
            return MerchantID;
        }

        public void setMerchantID(String merchantID) {
            MerchantID = merchantID;
        }
    }

    public static class PaymentDetail {

        @SerializedName("BankCode")
        @Expose
        private String bankCode;
        @SerializedName("BankEMICharges")
        @Expose
        private String bankEMICharges;
        @SerializedName("BankName")
        @Expose
        private String bankName;
        @SerializedName("ChequeRefNo")
        @Expose
        private String chequeRefNo;
        @SerializedName("DrawnOn")
        @Expose
        private String drawnOn;
        @SerializedName("GVCode")
        @Expose
        private String gVCode;
        @SerializedName("IFSC")
        @Expose
        private String iFSC;
        @SerializedName("PGReferenceId")
        @Expose
        private String pGReferenceId;
        @SerializedName("PaidAmount")
        @Expose
        private String paidAmount;
        @SerializedName("PointsBurned")
        @Expose
        private String pointsBurned;
        @SerializedName("RespCode")
        @Expose
        private String respCode;
        @SerializedName("RespMessage")
        @Expose
        private String respMessage;

        public String getBankCode() {
            return bankCode;
        }

        public void setBankCode(String bankCode) {
            this.bankCode = bankCode;
        }

        public String getBankEMICharges() {
            return bankEMICharges;
        }

        public void setBankEMICharges(String bankEMICharges) {
            this.bankEMICharges = bankEMICharges;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getChequeRefNo() {
            return chequeRefNo;
        }

        public void setChequeRefNo(String chequeRefNo) {
            this.chequeRefNo = chequeRefNo;
        }

        public String getDrawnOn() {
            return drawnOn;
        }

        public void setDrawnOn(String drawnOn) {
            this.drawnOn = drawnOn;
        }

        public String getGVCode() {
            return gVCode;
        }

        public void setGVCode(String gVCode) {
            this.gVCode = gVCode;
        }

        public String getIFSC() {
            return iFSC;
        }

        public void setIFSC(String iFSC) {
            this.iFSC = iFSC;
        }

        public String getPGReferenceId() {
            return pGReferenceId;
        }

        public void setPGReferenceId(String pGReferenceId) {
            this.pGReferenceId = pGReferenceId;
        }

        public String getPaidAmount() {
            return paidAmount;
        }

        public void setPaidAmount(String paidAmount) {
            this.paidAmount = paidAmount;
        }

        public String getPointsBurned() {
            return pointsBurned;
        }

        public void setPointsBurned(String pointsBurned) {
            this.pointsBurned = pointsBurned;
        }

        public String getRespCode() {
            return respCode;
        }

        public void setRespCode(String respCode) {
            this.respCode = respCode;
        }

        public String getRespMessage() {
            return respMessage;
        }

        public void setRespMessage(String respMessage) {
            this.respMessage = respMessage;
        }

    }
}
