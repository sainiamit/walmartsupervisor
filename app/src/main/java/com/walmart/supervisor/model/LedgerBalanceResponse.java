package com.walmart.supervisor.model;

import java.util.List;

/**
 * Created by srihari on 31/10/2017.
 */

public class LedgerBalanceResponse {
    public String messageCode, Message;
    public int ErrorCode;
    public BalanceEnquiryResponseView BalanceEnquiryResponseView;

    public static class BalanceEnquiryResponseView {
        public String Balance, Status, Code, Message;
    }

    public static class LedgerAdvanceStatement {
        public Amount Amount;
    }
    public static class Amount {
        public List < TenderTypes > TenderTypes;
    }

    public static class TenderTypes {
        public String TENDER_NAME;
        public float REM_LIMIT;
    }
}
