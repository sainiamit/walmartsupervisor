package com.walmart.supervisor.model;

import com.walmart.supervisor.model.product.Product;

import java.util.ArrayList;

/**
 * Created by M1045813 on 7/15/2018.
 */

public class ProductResponse {
    public int MessageCode;
    public String Message;
    public ArrayList<Product> Products;
}
