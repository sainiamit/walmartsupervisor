package com.walmart.supervisor.model.product;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by riteshdubey on 1/25/17.
 */
public class VariantProduct implements Parcelable, Serializable {

    public ArrayList<Variant> Size;
    public ArrayList<Variant> Color;
    public int VariantProductID;
    public float VariantMRP;
    public float VariantWebPrice;
    public int Inventory;
    public String Description;
    public String SmallImage;
    public boolean IsDefault;
    public ArrayList<VariantProperty> VariantProperties;


    @Override
    public boolean equals(Object other) {
        return !(other == null || other.getClass() != getClass()) && this.VariantProductID == ((VariantProduct) other).VariantProductID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Size);
        dest.writeTypedList(this.Color);
        dest.writeInt(this.VariantProductID);
        dest.writeFloat(this.VariantMRP);
        dest.writeFloat(this.VariantWebPrice);
        dest.writeInt(this.Inventory);
        dest.writeString(this.Description);
        dest.writeString(this.SmallImage);
        dest.writeByte(this.IsDefault ? (byte) 1 : (byte) 0);
        dest.writeList(this.VariantProperties);
    }

    public VariantProduct() {
    }

    protected VariantProduct(Parcel in) {
        this.Size = in.createTypedArrayList(Variant.CREATOR);
        this.Color = in.createTypedArrayList(Variant.CREATOR);
        this.VariantProductID = in.readInt();
        this.VariantMRP = in.readFloat();
        this.VariantWebPrice = in.readFloat();
        this.Inventory = in.readInt();
        this.Description = in.readString();
        this.SmallImage = in.readString();
        this.IsDefault = in.readByte() != 0;
        this.VariantProperties = new ArrayList<VariantProperty>();
        in.readList(this.VariantProperties, VariantProperty.class.getClassLoader());
    }

    public static final Creator<VariantProduct> CREATOR = new Creator<VariantProduct>() {
        @Override
        public VariantProduct createFromParcel(Parcel source) {
            return new VariantProduct(source);
        }

        @Override
        public VariantProduct[] newArray(int size) {
            return new VariantProduct[size];
        }
    };
}
