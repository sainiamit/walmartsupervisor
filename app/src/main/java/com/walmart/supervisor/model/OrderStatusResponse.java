package com.walmart.supervisor.model;

/**
 * Created by M1045813 on 10/2/2018.
 */

public class OrderStatusResponse {
    public String messageCode, Message;
    public int ErrorCode;
    public OrderStatus OrderStatus;

    public static class OrderStatus {
        public int OrderId;
        public String status;
        public String subStatus;
    }

}
