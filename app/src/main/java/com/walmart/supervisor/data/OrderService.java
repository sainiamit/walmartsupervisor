/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.data;


import com.walmart.supervisor.model.CancelResponse;
import com.walmart.supervisor.model.DeveloperAPIResponse;
import com.walmart.supervisor.model.LedgerBalanceResponse;
import com.walmart.supervisor.model.LoginResponse;
import com.walmart.supervisor.model.OrderStatusResponse;
import com.walmart.supervisor.model.ProcessTransactionResponse;
import com.walmart.supervisor.model.ProductResponse;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.model.product.SearchRefine;
import com.walmart.supervisor.model.slabprice.VolumetricLists;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface OrderService {

    @Headers("Accept: application/json")
    @POST
    @FormUrlEncoded
    Observable<LoginResponse> checkLoginValidity(@Url String url, @Field("username") String username, @Field("password") String password);

    @GET
    Observable<OrderResponse> fetchOrder(@Url String url);


    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<OrderResponse> fetchOrderHistory(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Content-Type: application/json")
    @GET
    Observable<OrderResponse> getOrderDetails(@Url String url);

    @Headers("Content-Type: application/json")
    @GET
    Observable<SearchRefine> searchRefine(@Url String iUrl, @Query(QueryConstants.QUERY_QUERY_TEXT) String query_text, @Query(QueryConstants.QUERY_CATEGORY_IDS) String categoryids, @Query(QueryConstants.QUERY_BRAND_IDS) String brandids, @Query(QueryConstants.QUERY_ATTRIBUTE_VALUE_IDS) String attributeValueids, @Query(QueryConstants.QUERY_VARIANT_PROPERTY_IDS) String variantpropertyids, @Query(QueryConstants.QUERY_TAG_IDS) String tagIds, @Query(QueryConstants.QUERY_GROUP_IDS) String groupids, @Query(QueryConstants.QUERY_SUPPLIER_IDS) String supplierids, @Query(QueryConstants.QUERY_MIN_PRICE) String minprice, @Query(QueryConstants.QUERY_MAX_PRICE) String maxprice, @Query(QueryConstants.QUERY_FROM) int from, @Query(QueryConstants.QUERY_LIMIT) int limit, @Query(QueryConstants.QUERY_IS_REFINE) boolean IsRefine, @Query(QueryConstants.QUERY_STORE_ID) String storeid, @Query(QueryConstants.QUERY_ORDER_SEQ) String orderseq, @Query(QueryConstants.QUERY_OUTPUT_FORMAT) String outputformat);

    @Headers("Accept: application/json")
    @POST
    @FormUrlEncoded
    Observable<CancelResponse> cancelOrder(@Url String url, @Field("MerchantId") String username, @Field("OrderId") String password);
    //Observable<CancelResponse> cancelOrder(@Url String url, @Body Map<String, Object> params);

    @Headers("Content-Type: application/json")
    @GET
    Observable<VolumetricLists> getVolumetricPrice(@Url String url, @Query(QueryConstants.QUERY_STORE_ID) String storeid, @Query(QueryConstants.QUERY_PRODUCT_ID) String productid);

    @Headers("Content-Type: application/json")
    @GET
    Observable<Product> getProducts(@Url String url, @Query(QueryConstants.QUERY_PRODUCT_ID) String productid, @Query(QueryConstants.QUERY_STORE_ID) String storeid, @Query(QueryConstants.OUTPUT_FORMAT) String output, @Query(QueryConstants.QUERY_VOLUMETRIC) boolean volumetric);

    @Headers("Content-Type: application/json")
    @GET
    Observable<ProductResponse> loadProduct(@Url String url, @Query(QueryConstants.QUERY_PRODUCT_ID) int productid, @Query(QueryConstants.QUERY_STORE_ID) String storeid, @Query(QueryConstants.QUERY_HS_ID) String hsid, @Query(QueryConstants.QUERY_OUTPUT_FORMAT) String outputformat, @Query(QueryConstants.QUERY_VOLUMETRIC) boolean volumetric);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<DeveloperAPIResponse> addOrderLineItem(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<DeveloperAPIResponse> deleteOrderLineItem(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Accept: application/json")
    @POST
    @FormUrlEncoded
    Observable<LedgerBalanceResponse> getLedgerBalance(@Url String url, @Field("MerchantId") String merchantId, @Field("provider") String provider, @Field("userId") String userID, @Field("gatewayId") String gatewayID);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<DeveloperAPIResponse> editOrderLineItem(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<DeveloperAPIResponse> authorizeOrder(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<OrderResponse> editOrderFields(@Url String url, @FieldMap(encoded = true) Map<String, Object> params);
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
//    @POST
//    Observable<OrderResponse> editOrderFields(@Url String url, @Field("GiftMessage") String giftMessage);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST
    Observable<ProcessTransactionResponse> processTransaction(@Url String iUrl, @FieldMap(encoded = true) Map<String, Object> params);

    @Headers("Content-Type: application/json")
    @GET
    Observable<OrderStatusResponse> getOrderStatus(@Url String url);
}
