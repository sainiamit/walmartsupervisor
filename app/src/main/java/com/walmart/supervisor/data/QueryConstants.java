package com.walmart.supervisor.data;

/**
 * Created by M1044639 on 20-Jun-18.
 */

public class QueryConstants {
   // public static final String STORE_ID = "248";
    //cut over
    public static final String STORE_ID = "282";
    public static final String SORT_CATALOG = "orderseq";
    public static final String OUTPUT_FORMAT = "outputformat";

    public static final String SEARCH_REFINE = "Search/Refine";

    static final String QUERY_NAME = "name";
    static final String QUERY_DELIVERY_MODE = "deliverymode";
    static final String QUERY_LATITUDE = "latitude";
    static final String QUERY_LONGITUDE = "longitude";
    static final String QUERY_REF_CODE = "refcode";
    static final String QUERY_STORE_ID = "storeid";
    static final String QUERY_LANGUAGE_CODE = "languagecode";
    static final String QUERY_H1CAT = "h1cat";
    static final String QUERY_H2CAT = "h2cat";
    static final String QUERY_H3CAT = "h3cat";
    static final String QUERY_H4CAT = "h4cat";
    static final String QUERY_TAG_ID = "tagid";
    static final String QUERY_FROM = "from";
    static final String QUERY_TO = "to";
    static final String QUERY_ORDER_SEQ = "orderseq";
    static final String QUERY_BRAND_ID = "brandid";
    static final String QUERY_PRODUCT_ID = "productid";
    static final String QUERY_OUTPUT_FORMAT = "outputformat";
    static final String QUERY_HS_ID = "hsid";
    static final String QUERY_SEARCH_KEY = "searchKey";
    static final String QUERY_CATEGORY_ID = "categoryid";
    static final String QUERY_LIMIT = "limit";
    static final String QUERY_QUERY_TEXT = "query_text";
    static final String QUERY_CATEGORY_IDS = "categoryids";
    static final String QUERY_BRAND_IDS = "brandids";
    static final String QUERY_ATTRIBUTE_VALUE_IDS = "attributeValueids";
    static final String QUERY_VARIANT_PROPERTY_IDS = "variantpropertyids";
    static final String QUERY_TAG_IDS = "tagIds";
    static final String QUERY_GROUP_IDS = "groupids";
    static final String QUERY_SUPPLIER_IDS = "supplierids";
    static final String QUERY_MIN_PRICE = "minprice";
    static final String QUERY_MAX_PRICE = "maxprice";
    static final String QUERY_IS_REFINE = "IsRefine";
    static final String QUERY_VOLUMETRIC = "isVolumetric";

}
