package com.walmart.supervisor.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Srihari Mani on 20-06-2018.
 */

public class DateHelper {


    /**
     * @param isTodaysDate boolean to check to return Todays date or Tomorrow's
     *                     \     * @return Date required
     */
    public static String getDate(boolean isTodaysDate) {
        Calendar calendar = Calendar.getInstance();
        if (!isTodaysDate)
            calendar.add(Calendar.DAY_OF_YEAR, 1);

        SimpleDateFormat aSdformat = new SimpleDateFormat("dd");
        String strDate = aSdformat.format(calendar.getTime());
        return strDate;
    }

    /**
     * @param isToday boolean to check to return Todays day or Tomorrow's
     * @return Day required
     */
    public static String getDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE");
        Date d = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            d = calendar.getTime();

        String dayOfTheWeek = sdf.format(d);

        return dayOfTheWeek;
    }

    /**
     * @param iDate
     */
    public static String getDeliverDate(String iDate) {
        String aDeliveryDate = iDate.substring(0, 2) + "-" + iDate.substring(2, 4) + "-" + iDate.substring(4);
        return aDeliveryDate;
    }

    public static String getDate(String iMiilis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(iMiilis));

        //SimpleDateFormat aSdformat = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat aSdformat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        String strDate = aSdformat.format(calendar.getTime());
        return strDate;
    }

    public static String getDateFromLong(String iMiilis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(iMiilis));

        SimpleDateFormat aSdformat = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = aSdformat.format(calendar.getTime());
        return strDate;
    }
}
