package com.walmart.supervisor.utils;

import android.util.Log;

import com.walmart.supervisor.BuildConfig;

/**
 * Created by Srihari on 08/06/2018.
 */

public class Logger {

    public static void logVerbose(String TAG, String iTitle, String iLogMessage) {
        if (BuildConfig.DEBUG)
            Log.v(TAG, iTitle + " : " + iLogMessage);
    }

    public static void logInfo(String TAG, String iTitle, String iLogMessage) {
        if (BuildConfig.DEBUG)
            Log.i(TAG, iTitle + " : " + iLogMessage);
    }

    public static void logError(String TAG, String iTitle, String iLogMessage, Exception iException) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, iTitle + " : " + iLogMessage, iException);

    }

    public static void logWarn(String TAG, String iTitle, String iLogMessage) {
        if (BuildConfig.DEBUG)
            Log.w(TAG, iTitle + " : " + iLogMessage);
    }

    public static void logWarn(String TAG, String iLogMessage) {
        if (BuildConfig.DEBUG)
            Log.w(TAG, iLogMessage);
    }

    public static void logDebug(String TAG, String iTitle, String iLogMessage) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, iTitle + " : " + iLogMessage);

    }
}
