package com.walmart.supervisor.utils;

import android.util.Log;

import com.walmart.supervisor.constants.Constants;

/**
 * Created by M1045813 on 8/1/2018.
 */

public class Logs {
    private static boolean isLogEnable = Constants.PRINT_LOGS;

    public static void d(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.d(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.e(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.i(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.v(tag, msg);
    }

    public static void w(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.w(tag, msg);
    }

    public static void wtf(String tag, String msg) {
        if (msg != null && isLogEnable)
            Log.wtf(tag, msg);
    }
}
