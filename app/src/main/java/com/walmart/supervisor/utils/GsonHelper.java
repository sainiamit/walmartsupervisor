package com.walmart.supervisor.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.walmart.supervisor.model.AccessToken;
import com.walmart.supervisor.model.CreateShoppingList;
import com.walmart.supervisor.model.ShoppingListSerializer;
import com.walmart.supervisor.model.product.VariantProduct;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class GsonHelper {
    public static Gson mGson = new GsonBuilder()
            .registerTypeAdapter(CreateShoppingList.class, new ShoppingListSerializer()).create();

    private static Type aProductListType = new TypeToken<ArrayList<VariantProduct>>() {}.getType();

    public static Object getGson(String json, Class<?> class1) {

        return mGson.fromJson(json, class1);
    }

    public static String toUserDetails(AccessToken iUserDetail){
        return mGson.toJson(iUserDetail);
    }

    public static ArrayList<VariantProduct> getVariantProductList(String iData){
        return mGson.fromJson(iData, aProductListType);
    }

    public static <T> String toJson(T iData){
        return mGson.toJson(iData);
    }
}
