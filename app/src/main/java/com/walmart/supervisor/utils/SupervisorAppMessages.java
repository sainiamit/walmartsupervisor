package com.walmart.supervisor.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.walmart.supervisor.R;
import com.walmart.supervisor.view.LoginActivity;
import com.walmart.supervisor.view.OrderActivity;
import com.walmart.supervisor.view.OrderDetailActivity;

/**
 * Created by Srihari on 08/06/2018.
 */

public class SupervisorAppMessages {

    public static void showAlertYesNo(String iMessage, Context iContext, DialogInterface.OnClickListener iYesListener, DialogInterface.OnClickListener iNoListener) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iContext.getString(R.string.alert_title_info));
        dialog.setMessage(iMessage);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(R.string.yes), iYesListener);
        dialog.setButton(AlertDialog.BUTTON_NEUTRAL, iContext.getString(R.string.no), iNoListener);
        dialog.show();
    }

    public static void showAlertOkCallback(String iMessage, Context iContext, DialogInterface.OnClickListener iYesListener) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iContext.getString(R.string.alert_title_info));
        dialog.setMessage(iMessage);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(android.R.string.ok), iYesListener);
        dialog.show();
    }

    public void showAlertRetryCallback(String iMessage, Context iContext, DialogInterface.OnClickListener iYesListener) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iContext.getString(R.string.alert_title_info));
        dialog.setMessage(iMessage);
        dialog.setCancelable(false);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(R.string.retry), iYesListener);
        dialog.show();
    }

    public void showAlertRetryWithCancelCallback(String iMessage, Context iContext, DialogInterface.OnClickListener iYesListener, DialogInterface.OnClickListener iNoListener) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iContext.getString(R.string.alert_title_info));
        dialog.setMessage(iMessage);
        dialog.setCancelable(false);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(R.string.retry), iYesListener);
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, iContext.getString(R.string.cancel), iNoListener);
        dialog.show();
    }

    public void showAlertOkCallback(String iTitle, String iMessage, Context iContext, DialogInterface.OnClickListener iYesListener) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iTitle);
        dialog.setCancelable(false);
        dialog.setMessage(iMessage);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(android.R.string.ok), iYesListener);
        dialog.show();
    }

    public void showAlertOkCallback(String iTitle, String iMessage, Context iContext) {
        AlertDialog dialog = new AlertDialog.Builder(iContext).create();
        dialog.setTitle(iTitle);
        dialog.setMessage(iMessage);
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, iContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog.show();
    }

    /**
     * Shows toast with dynamic message
     *
     * @param iMessage
     */
    public static void showToast(String iMessage, Context iContext) {
        Toast.makeText(iContext, iMessage, Toast.LENGTH_SHORT).show();
    }

    public void showDynamicToast(String iMessage, Context iContext, int duration) {
        if (duration == 0) {
            Toast.makeText(iContext, iMessage, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(iContext, iMessage, Toast.LENGTH_LONG).show();
        }
    }

    public void showStyleableToast(Context iContext, String iMessage, int textColorResId, int bgColorResId) {
        new StyleableToast
                .Builder(iContext)
                .text(iMessage)
                .textColor(iContext.getResources().getColor(textColorResId))
                .backgroundColor(iContext.getResources().getColor(bgColorResId))
                .show();
    }

    public void showExitConfirmDialog(AppCompatActivity appCompatActivity) {
        new AlertDialog.Builder(appCompatActivity)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        appCompatActivity.finish();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void showLogoutConfirmDialog(AppCompatActivity appCompatActivity) {
        new AlertDialog.Builder(appCompatActivity)
                .setMessage("Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferences pref = appCompatActivity.getSharedPreferences("MyPref", appCompatActivity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.clear();
                        editor.apply();

                        appCompatActivity.finishAffinity();
                        appCompatActivity.startActivity(new Intent(appCompatActivity, LoginActivity.class));
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

}
