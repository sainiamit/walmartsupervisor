package com.walmart.supervisor.httpclient;

import android.text.TextUtils;
import com.walmart.supervisor.BuildConfig;


public class FrontEndApiHelper {
    private String endPoint;
    private String finalUrl = "";
    private String extraQueryParams = "";

    public String getEndPoint() {
        return endPoint;
    }

    public String getFinalUrl() {
        return finalUrl;
    }

    public static class Builder {
        String endPoint = "";
        String extraQueryParams = "";

        /**
         * Builder method for setting endpoint
         *
         * @param iEndPoint API endpoint
         * @return Builder instance
         */
        public Builder endPoint(String iEndPoint) {
            this.endPoint = iEndPoint;
            return this;
        }

        /**
         * Builder method for setting default query params
         *
         * @param iQueryParam API endpoint
         * @return Builder instance
         */
        public Builder addQueryParams(String iQueryParam) {
            this.extraQueryParams += iQueryParam;
            return this;
        }

        //return fully build object
        public FrontEndApiHelper build() {
            return new FrontEndApiHelper(this);
        }
    }

    private FrontEndApiHelper(Builder builder) {
        this.endPoint = builder.endPoint;
        this.finalUrl = finalUrl + BuildConfig.FRONT_API_BASE_URL;
        if (!TextUtils.isEmpty(endPoint)) {
            this.finalUrl = finalUrl + endPoint + "/";
        }
        this.finalUrl = finalUrl + BuildConfig.MERCHANT_ID;

        if (!TextUtils.isEmpty(extraQueryParams)) {
            this.finalUrl += extraQueryParams;
        }
    }
}
