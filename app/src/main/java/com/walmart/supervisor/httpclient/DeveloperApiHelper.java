package com.walmart.supervisor.httpclient;

import android.text.TextUtils;

import com.walmart.supervisor.BuildConfig;


public class DeveloperApiHelper {
    private String endPoint;
    private String finalUrl = "";
    private String serviceName;
    private APIMethod method;
    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";

    public String getEndPoint() {
        return endPoint;
    }

    public String getFinalUrl() {
        return finalUrl;
    }

    public static class Builder {
        String endPoint = "";
        String serviceName;
        APIMethod method;

        /**
         * Builder method for setting endpoint
         *
         * @param iEndPoint API endpoint
         * @return Builder instance
         */
        public Builder endPoint(String iEndPoint) {
            this.endPoint = iEndPoint;
            return this;
        }

        /**
         * Builder method for setting service name
         *
         * @param iServiceName service name
         * @return Builder instance
         */
        public Builder setServiceName(String iServiceName) {
            if (iServiceName != null) {
                this.serviceName = iServiceName;
            }
            return this;
        }

        /**
         * Builder method for setting HTTP Request Methods
         *
         * @param iMethod Use {@link APIMethod#GET} or {@link APIMethod#POST}
         * @return Builder instance
         */
        public Builder setMethod(APIMethod iMethod) {
            if (iMethod != null)
                this.method = iMethod;
            return this;
        }

        //return fully build object
        public DeveloperApiHelper build() {
            return new DeveloperApiHelper(this);
        }
    }

    private DeveloperApiHelper(Builder builder) {
        this.endPoint = builder.endPoint;
        this.finalUrl = finalUrl + BuildConfig.DEV_API_BASE_URL;
        this.serviceName = builder.serviceName;
        this.method = builder.method;
        if (!TextUtils.isEmpty(endPoint)) {
            this.finalUrl = finalUrl + endPoint + "/";
        }
        if (finalUrl.contains("Cancel") || finalUrl.contains("CancelItem") || finalUrl.contains("Authorize")) {
            this.finalUrl = finalUrl;
        } else {
            this.finalUrl = finalUrl + BuildConfig.MERCHANT_ID;
        }
        if (!TextUtils.isEmpty(serviceName)) {
            this.finalUrl += "/" + serviceName;
        }

        String aMethod = "";
        switch (method) {
            case GET:
                aMethod = METHOD_GET;
                break;
            case POST:
                aMethod = METHOD_POST;
                break;
        }
        this.finalUrl = Utilities.getFinalUrl(this.finalUrl, BuildConfig.PUBLIC_KEY, BuildConfig.SECRET_KEY, aMethod);
    }

}
