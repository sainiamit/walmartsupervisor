package com.walmart.supervisor.constants;

/**
 * Created by M1044639 on 23-Jul-18.
 */

public class Constants {
    //public static final String MERCHANT_ID = "b7095ff2-12fe-4c18-9b0a-946980d12202";
    //cut over
    public static final String MERCHANT_ID = "a541161e-a030-4d88-9696-c6b48e332f6e";
    //prod
    //public static final String MERCHANT_ID = "2f9b7c67-7c32-4267-9132-abb503dc4c83";

    public static final String startDate = "";
    public static final String endDate = "";
    public static final int days = 2;
    public static final long timeinMiliSecond = 86400000L;
    public static final long dayinMillisecond = days * timeinMiliSecond;
    public static final String ORDER_MODEL = "orderModel";

    public static final String SUCCESS_CODE = "1004";
    public static final String Try_Again = "Something went wrong.Please try again later";
    public static final boolean PRINT_LOGS = true;
    public static final int Max_Range = 30000;

    public static final String PENDING_ORDER = "PENDING ORDERS";
    public static final String BDA_COMMENTS_ORDER = "ORDERS PENDING FOR AUTHORIZATION";

    public static final String LOCATION_CODE = "locationCode";
    public static final String LOCATION_ID = "locationID";

    public static final boolean ISLOTS = true;


}
