package com.walmart.supervisor.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.slabprice.VolumetricLists;
import com.walmart.supervisor.model.slabprice.VolumetricPricing;
import com.walmart.supervisor.view.fragments.BaseDialogFragment;

import java.util.List;

/**
 * Created by M1044639 on 05-Oct-18.
 */

public class ReasonDialog extends BaseDialogFragment {

    //    private ReasonSelectedListener reasonSelectedListener;
//    private boolean isSlab = false;
    LinearLayout volumetric_table;
    LinearLayout volumetric_table_parent;
    String Volproduct, orderLineIdString;
    VolumetricLists volumetricPricing;
    private OrderLineId orderLineId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.volumertic_parent_table, null);

        if (getArguments() != null) {
//            isSlab = getArguments().getBoolean("isSlab");
            Volproduct = getArguments().getString("Volproduct");
            orderLineIdString = getArguments().getString("orderLineId");
            Gson gson = new Gson();
            volumetricPricing = gson.fromJson(Volproduct, VolumetricLists.class);
            orderLineId = gson.fromJson(orderLineIdString, OrderLineId.class);

        }

        volumetric_table = (LinearLayout) view.findViewById(R.id.volumetric_table);
        volumetric_table_parent = (LinearLayout) view.findViewById(R.id.volumetric_table_parent);
        volumetric_table.setVisibility(View.VISIBLE);
        volumetric_table_parent.setVisibility(View.VISIBLE);
        updateSlabPrice(volumetricPricing.volumetricPricing, orderLineId);

        return view;
    }

    private void updateSlabPrice(List<VolumetricPricing> volumetricPricingList, OrderLineId orderLineId) {
        Gson gson = new Gson();
        String Discount = "";
        //orderLineId.product.isVolumetricVisible = true;

        VolumetricPricing volumetricPricing = volumetricPricingList.get(0);
        volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
        volumetricPricing.Discount = String.valueOf(Discount);
        for (int i = 0; i < volumetricPricingList.size(); i++) {
            volumetricPricing = volumetricPricingList.get(i);
            volumetricPricing.Discount = String.format("%.2f", 100 - ((volumetricPricing.WebPrice / orderLineId.product.MRP) * 100));
            if ((i + 1) == volumetricPricingList.size()) {
                volumetricPricing.DisplayQty = volumetricPricing.Quantity + " +";
            } else {
//                    if (volumetricPricing.Quantity == 1) {
//                        volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
//                    } else {
                volumetricPricing.DisplayQty = volumetricPricing.Quantity + " to " + ((volumetricPricingList.get(i + 1).Quantity) - 1);
//                    }
            }

        }
        int i = 0;
        volumetric_table.removeAllViews();
        for (VolumetricPricing volumetricpricing : volumetricPricingList) {
            View volumetric_view = LayoutInflater.from(getContext()).inflate(R.layout.volumetric_table, null);
            TextView txt1 = (TextView) volumetric_view.findViewById(R.id.txt1);
            TextView txt2 = (TextView) volumetric_view.findViewById(R.id.txt2);
            TextView txt3 = (TextView) volumetric_view.findViewById(R.id.txt3);
            txt1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            txt2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            txt3.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            View View1 = (View) volumetric_view.findViewById(R.id.View1);

            if (i == 0) {
                View1.setVisibility(View.GONE);
//                    product.isVolumetricVisible = false;
            } else {
                View1.setVisibility(View.VISIBLE);
//                    product.isVolumetricVisible = true;

            }
            if (volumetricpricing != null) {
                txt1.setText(String.valueOf(volumetricpricing.DisplayQty));
                txt2.setText(String.valueOf("₹ " + volumetricpricing.WebPrice));
                txt3.setText(String.valueOf(volumetricpricing.Discount + "%"));
                volumetric_table.addView(volumetric_view);

            }
            i++;
            if (i == 3)
                break;
        }
        //orderLineId.product.isLoaded = true;

    }
//    private void updateValuematricPrice(List<VolumetricLists> volumetricLists, LinearLayout volumetric_table_parent, LinearLayout volumetric_table) {
//        String Discount = "";
//        if (volumetricLists != null && volumetricLists.size() > 1) {
//            volumetric_table.setVisibility(View.VISIBLE);
//            volumetric_table_parent.setVisibility(View.VISIBLE);
//            VolumetricLists volumetricPricing = volumetricLists.VolumetricPricing.get(0);
//            volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
//            volumetricPricing.Discount = String.valueOf(Discount);
//            for (int i = 0; i < volumetricLists.VolumetricPricing.size(); i++) {
//                volumetricPricing = volumetricLists.VolumetricPricing.get(i);
//
////                volumetricPricing.Discount = String.format("%.2f", 100 - ((volumetricPricing.WebPrice / product.MRP) * 100));
//                volumetricPricing.Discount = String.valueOf(Math.round(100 - ((volumetricPricing.WebPrice / Float.parseFloat(volumetricLists.MRP)) * 100)));
//                if ((i + 1) == volumetricLists.VolumetricPricing.size()) {
//                    volumetricPricing.DisplayQty = volumetricPricing.Quantity + " +";
//                } else {
//                    if (volumetricPricing.Quantity == ((volumetricLists.VolumetricPricing.get(i + 1).Quantity) - 1))
//                        volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
//                    else
//                        volumetricPricing.DisplayQty = volumetricPricing.Quantity + " to " + ((volumetricLists.VolumetricPricing.get(i + 1).Quantity) - 1);
//                }
//
//            }
//            int i = 0;
//            volumetric_table.removeAllViews();
//            for (Product.VolumetricPricing volumetricpricing : volumetricLists.VolumetricPricing) {
//                View volumetric_view = LayoutInflater.from(getActivity()).inflate(R.layout.volumetric_table, null);
//                TextView txt1 = (TextView) volumetric_view.findViewById(R.id.txt1);
//                TextView txt2 = (TextView) volumetric_view.findViewById(R.id.txt2);
//                TextView txt3 = (TextView) volumetric_view.findViewById(R.id.txt3);
//                txt1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
//                txt2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
//                txt3.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
//                View View1 = volumetric_view.findViewById(R.id.View1);
//                if (i == 0) {
//                    View1.setVisibility(View.GONE);
////                    product.isVolumetricVisible = false;
//                } else {
//                    View1.setVisibility(View.VISIBLE);
////                    product.isVolumetricVisible = true;
//
//                }
//                if (volumetricpricing != null) {
//                    txt1.setText(String.valueOf(volumetricpricing.DisplayQty));
//                    txt2.setText(String.valueOf("₹ " + volumetricpricing.WebPrice));
//                    txt3.setText(String.valueOf(volumetricpricing.Discount + "%"));
//                    volumetric_table.addView(volumetric_view);
//                }
//                i++;
//                if (i == 5)
//                    break;
//            }
//        }
//    }


//    public void reasonSelectedLisener(ReasonSelectedListener reasonSelectedListener) {
//        this.reasonSelectedListener = reasonSelectedListener;
//    }
//
//    public interface ReasonSelectedListener {
//        void onReasonSelected(String reason);
//    }
}
