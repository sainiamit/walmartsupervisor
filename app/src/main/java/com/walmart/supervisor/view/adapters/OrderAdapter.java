/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import android.databinding.DataBindingUtil;
import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.ItemOrderBinding;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.viewmodel.ItemOrderViewModel;

import java.util.Collections;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderAdapterViewHolder> {

  private List<Orders> orderList;

  public OrderAdapter() {
    this.orderList = Collections.emptyList();
  }

  @Override public OrderAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemOrderBinding itemOrderBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_order,
                    parent, false);
    return new OrderAdapterViewHolder(itemOrderBinding);
  }

  @Override public void onBindViewHolder(OrderAdapterViewHolder holder, int position) {
    holder.bindOrder(orderList.get(position));
  }

  @Override public int getItemCount() {
    return orderList.size();
  }

  public void setOrderList(List<Orders> orderList) {
    this.orderList = orderList;
    notifyDataSetChanged();
  }

  public static class OrderAdapterViewHolder extends RecyclerView.ViewHolder {
    ItemOrderBinding mItemOrderBinding;

    public OrderAdapterViewHolder(ItemOrderBinding itemOrderBinding) {
      super(itemOrderBinding.itemOrder);
      this.mItemOrderBinding = itemOrderBinding;
    }

    void bindOrder(Orders order) {
      Logger.logInfo("OrderAdapter","--- order.Status "+order.Status,"  "+order.Status);
      if (mItemOrderBinding.getOrderViewModel() == null) {
        mItemOrderBinding.setOrderViewModel(
                new ItemOrderViewModel(order, itemView.getContext()));
      } else {
        mItemOrderBinding.getOrderViewModel().setOrder(order);
      }
    }
  }
}
