package com.walmart.supervisor.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.OrderFragmentBinding;
import com.walmart.supervisor.view.adapters.OrderAdapter;
import com.walmart.supervisor.viewmodel.OrderViewModel;

import java.util.Observable;
import java.util.Observer;

public class OrdersFragment extends Fragment implements Observer {

    int position;
    OrderFragmentBinding orderFragmentBinding;
    private OrderViewModel orderViewModel;
    String mLocationId;

    public static Fragment getInstance(int position, String LocationId) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        bundle.putString("LocationId", LocationId);
        OrdersFragment ordersFragment = new OrdersFragment();
        ordersFragment.setArguments(bundle);
        return ordersFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
        mLocationId = getArguments().getString("LocationId");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.order_fragment, container, false);
        orderFragmentBinding = OrderFragmentBinding.bind(layout);
        initDataBinding();
        setupObserver(orderViewModel);
        setupListOrderView(orderFragmentBinding.listOrder);
        return layout;
    }

    private void setupListOrderView(RecyclerView listOrder) {
        OrderAdapter adapter = new OrderAdapter();
        listOrder.setAdapter(adapter);
        listOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initDataBinding() {
        orderViewModel = new OrderViewModel(getActivity(), position, mLocationId);
        orderFragmentBinding.setMainFragmentViewModel(orderViewModel);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }


    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof OrderViewModel) {
            OrderAdapter orderAdapter = (OrderAdapter) orderFragmentBinding.listOrder.getAdapter();
            OrderViewModel orderViewModel = (OrderViewModel) observable;
            orderAdapter.setOrderList(orderViewModel.getOrderList());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        orderViewModel.reset();
        Log.d("destoryFragment","Destroyed");
    }

}