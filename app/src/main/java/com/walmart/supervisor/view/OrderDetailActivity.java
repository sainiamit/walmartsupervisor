/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.OrderDetailActivityBinding;
import com.walmart.supervisor.model.Order;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.adapters.OrderDetailItemsAdapter;
import com.walmart.supervisor.viewmodel.BottomSheetViewModel;
import com.walmart.supervisor.viewmodel.OrderDetailViewModel;

import java.util.Observable;
import java.util.Observer;

public class OrderDetailActivity extends AppCompatActivity implements Observer {

    private static final String EXTRA_ORDER = "EXTRA_ORDER";
    Orders order;
    private OrderDetailActivityBinding orderDetailActivityBinding;
    OrderDetailViewModel orderDetailViewModel;
    BottomSheetBehavior sheetBehavior;
    //bottom sheet function
    BottomSheetViewModel bottomSheetViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        setSupportActionBar(orderDetailActivityBinding.toolbar);
        displayHomeAsUpEnabled();
        getExtrasFromIntent();
        orderDetailViewModel = new OrderDetailViewModel(this, this.order);
        orderDetailActivityBinding.setOrderDetailViewModel(orderDetailViewModel);
        setupListOrderItemsView(orderDetailActivityBinding.listOrderItems);
        setupObserver(orderDetailViewModel);
        //bottom sheet
        //bottomSheetViewModel = new BottomSheetViewModel(OrderDetailActivity.this);
        //orderDetailActivityBinding = OrderDetailActivityBinding.inflate(getLayoutInflater());

    }

    private void initDataBinding() {
        orderDetailActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.order_detail_activity);
        LinearLayout layoutBottomSheet = (LinearLayout) findViewById(R.id.orderdetails_bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_logout) {
            new SupervisorAppMessages().showLogoutConfirmDialog(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupListOrderItemsView(RecyclerView listOrder) {
        OrderDetailItemsAdapter adapter = new OrderDetailItemsAdapter();
        listOrder.setAdapter(adapter);
        listOrder.setLayoutManager(new LinearLayoutManager(this));
    }


    public static Intent launchDetail(Context context, Orders order) {
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra(EXTRA_ORDER, new Gson().toJson(order, Orders.class).toString());
        return intent;
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof OrderDetailViewModel) {
            OrderDetailItemsAdapter orderDetailItemsAdapter = (OrderDetailItemsAdapter) orderDetailActivityBinding.listOrderItems.getAdapter();
            OrderDetailViewModel orderDetailViewModel = (OrderDetailViewModel) observable;
            orderDetailItemsAdapter.setOrderList(orderDetailViewModel.getOrderList());
        }
    }

    private void getExtrasFromIntent() {
        String jsonString = getIntent().getStringExtra(EXTRA_ORDER);
        //String jsonString = "{\"AmountPayable\":5997.63,\"BillAddress1\":\"JUNA BAZAR ROAD NEAR BAROTOTI MALIWADI AHMADNAGAR\",\"BillAddress2\":\"\",\"BillCity\":\"AHMADNAGAR\",\"BillCountry\":\"India\",\"BillEmail\":\"pradeep.zawar@yahoo.co.in\",\"BillFirstname\":\"PRADEEP\",\"BillLastname\":\"ZAWAR\",\"BillMobile\":\"91-9422265210\",\"BillPhone\":\"91-9422265210\",\"BillZIP\":\"414001\",\"BillingState\":\"Maharashtra\",\"ChannelID\":\"0\",\"ChannelOrderID\":\"0\",\"ConversionFactor\":\"1\",\"DeliveryOption\":\"ship\",\"DemandedDeliveryDate\":\"\",\"IsGift\":false,\"IsSelfShip\":\"False\",\"Latitude\":\"0\",\"LeadTime\":\"\",\"Longitude\":\"0\",\"MerchantId\":\"2f9b7c67-7c32-4267-9132-abb503dc4c83\",\"OrderDate\":\"/Date(1529430799000+0530)/\",\"OrderId\":1552546,\"OriginalOrderId\":\"\",\"PaymentDetails\":[{\"AgentId\":\"\",\"Amount\":\"5997.63\",\"Channel\":\"W\",\"ClientUserAgent\":\"API\",\"CurrencyCode\":\"INR\",\"GV\":\"\",\"OrderId\":1552546,\"PaymentDate\":\"6/19/2018 11:23:19 PM\",\"PaymentDetailsId\":1725960,\"PaymentOption\":\"pats\",\"PaymentResponse\":\"\",\"PaymentStatus\":\"P\",\"PaymentType\":\"pats\",\"PointsBurned\":\"0\",\"ResponseCode\":\"N\",\"checkOutGroup\":\"\",\"clientIP\":\"\"}],\"PickupFirstName\":\"\",\"PickupLastName\":\"\",\"PromotionDiscount\":0.0,\"ReferenceNo\":\"\",\"ReturnOrderId\":\"\",\"ShipAddress1\":\"Wal-Mart India Private Limited.S no.54/3, Near Mahanubhav Ashram,Opp. Krishi Vigyan Kendra Paithan Road,\",\"ShipAddress2\":\"\",\"ShipCity\":\"Aurangabad\",\"ShipCountry\":\"India\",\"ShipEmail\":\"\",\"ShipFirstname\":\"Aurangabad\",\"ShipLastname\":\"\",\"ShipMobile\":\"91-1204878888\",\"ShipPhone\":\"null\",\"ShipState\":\"Maharashtra\",\"ShipZip\":\"431005\",\"ShippingDiscount\":0.0,\"ShippingMode\":\"360\",\"Status\":\"P\",\"SupplierID\":\"00000000-0000-0000-0000-000000000000\",\"TAXTotal\":0.0,\"TotalAmount\":5997.63,\"UserId\":\"99677493-eb45-42b0-97a0-bcb360fcad2e\",\"VoucherDiscount\":0.0,\"cpuserid\":\"00000000-0000-0000-0000-000000000000\",\"deliveryslots\":{\"EndTime\":\"\",\"StartTime\":\"\"}}";
        Logger.logInfo("getExtrasFromIntent", "--- jsonString - " + jsonString, "");
        Gson gson = new Gson();
        this.order = gson.fromJson(jsonString, Orders.class);
        orderDetailActivityBinding.setOrderDetailViewModel(orderDetailViewModel);
        setTitle("Order ID - " + this.order.OrderId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        orderDetailViewModel.reset();
    }

}
