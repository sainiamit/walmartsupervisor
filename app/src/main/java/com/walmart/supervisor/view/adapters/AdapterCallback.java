package com.walmart.supervisor.view.adapters;

import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.product.Product;

/**
 * Created by M1045813 on 7/22/2018.
 */

public interface AdapterCallback {

    void hideAdapterItem(int pos, OrderLineId orderLineId);

    void updateQtyChange(int qty, int pos);

    void refreshProductLineItems(int pos, float upadtedPrice, float oldPrice, int orderId, int orderLineId, String SKU, Product product);
}
