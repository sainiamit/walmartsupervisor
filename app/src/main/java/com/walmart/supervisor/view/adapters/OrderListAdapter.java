package com.walmart.supervisor.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.walmart.supervisor.R;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.view.DetailOrderActivity;

import java.util.List;

import am.appwise.components.ni.NoInternetDialog;

/**
 * Created by M1045813 on 7/21/2018.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListAdapterViewHolder> {
    private String orderDetailName;
    private List<Orders> orderList;
    private Context context;


    public class OrderListAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView orderId, orderDate, memberName, totalAmount, giftmsg,lineItems,commentName,lineName,amountPayble;
        LinearLayout commentLayout;

        public OrderListAdapterViewHolder(View view) {
            super(view);
            orderId = (TextView) view.findViewById(R.id.order_id);
            orderDate = (TextView) view.findViewById(R.id.order_date);
            memberName = (TextView) view.findViewById(R.id.member_name);
            totalAmount = (TextView) view.findViewById(R.id.total_amount);
            giftmsg = (TextView) view.findViewById(R.id.giftmsg);
            lineItems =(TextView)view.findViewById(R.id.line_items);
            commentName=(TextView)view.findViewById(R.id.comment_name);
            lineName = (TextView)view.findViewById(R.id.line_item_name);
            commentLayout =(LinearLayout)view.findViewById(R.id.comment_layout);
            amountPayble =(TextView)view.findViewById(R.id.amount_payble);

        }
    }

    public OrderListAdapter(Context context, List<Orders> ordersList, String orderDetailName) {
        this.orderList = ordersList;
        this.context = context;
        this.orderDetailName = orderDetailName;
    }

    @Override
    public OrderListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item, parent, false);

        return new OrderListAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.OrderListAdapterViewHolder holder, int position) {
        Orders orders = orderList.get(position);
        Log.d("orderlist", "" + orderList.size());
        holder.orderId.setText(String.valueOf(orders.OrderId));
        String[] aSplittedArray = orders.OrderDate.split("\\(");
        String aTime = aSplittedArray[1].split("\\+")[0];
        holder.orderDate.setText(DateHelper.getDate(aTime));
        if (orders.BillFirstname != null && orders.BillFirstname.trim().length() > 0) {
            holder.memberName.setText(orders.BillFirstname + " " + orders.BillLastname);
        } else if (orders.ShipFirstname != null && orders.ShipFirstname.trim().length() > 0) {
            holder.memberName.setText(orders.ShipFirstname + " " + orders.ShipLastname);
        } else {
            holder.memberName.setText("Name not available");
        }
        holder.totalAmount.setText(String.format("%.2f", orders.TotalAmount));
        if(orders.AmountPayable!= 0f)
        {
            holder.amountPayble.setText(String.format("%.2f", orders.AmountPayable));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isOnline(context)) {
                    context.startActivity(DetailOrderActivity.launchDetail(v.getContext(), orders, orderDetailName));
                } else {
                    NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                    noInternetDialog.show();
                }

            }
        });
        if(orderDetailName.equalsIgnoreCase(Constants.PENDING_ORDER))
        {
            holder.lineName.setVisibility(View.GONE);
            holder.commentName.setVisibility(View.GONE);
            holder.giftmsg.setVisibility(View.GONE);
            holder.lineItems.setVisibility(View.GONE);
            holder.commentLayout.setVisibility(View.GONE);
        }
        holder.giftmsg.setText(orders.BDAComments != null ? orders.BDAComments : "comments will display");
        holder.lineItems.setText(orders.LineItemCount!=0 ? String.valueOf(orders.LineItemCount):"12");

    }

    @Override
    public int getItemCount() {
        Log.d("orderlist", "" + orderList.size());
        return orderList.size();

    }


}
