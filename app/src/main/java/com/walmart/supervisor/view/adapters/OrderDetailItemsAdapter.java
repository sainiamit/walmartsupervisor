/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.ItemOrderDetailsBinding;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.viewmodel.ItemOrderDetailViewModel;

import java.util.Collections;
import java.util.List;

public class OrderDetailItemsAdapter extends RecyclerView.Adapter<OrderDetailItemsAdapter.OrderDetailItemsAdapterViewHolder> {

    private List<OrderLineId> orderLineIdList;

    public OrderDetailItemsAdapter() {
        this.orderLineIdList = Collections.emptyList();
    }

    @Override
    public OrderDetailItemsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderDetailsBinding itemOrderDetailsBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_order_details,
                parent, false);
        return new OrderDetailItemsAdapterViewHolder(itemOrderDetailsBinding);
    }

    @Override
    public void onBindViewHolder(OrderDetailItemsAdapterViewHolder holder, int position) {
        holder.bindOrder(orderLineIdList.get(position));
    }

    @Override
    public int getItemCount() {
        return orderLineIdList.size();
    }

    public void setOrderList(List<OrderLineId> orderLineIdList) {
        this.orderLineIdList = orderLineIdList;
        notifyDataSetChanged();
    }


    public static class OrderDetailItemsAdapterViewHolder extends RecyclerView.ViewHolder {
        ItemOrderDetailsBinding mItemOrderDetailsBinding;

        public OrderDetailItemsAdapterViewHolder(ItemOrderDetailsBinding itemOrderDetailsBinding) {
            super(itemOrderDetailsBinding.itemOrderDetails);
            this.mItemOrderDetailsBinding = itemOrderDetailsBinding;
        }

        void bindOrder(OrderLineId orderLineId) {
            if (mItemOrderDetailsBinding.getItemOrderDetailViewModel() == null) {
                mItemOrderDetailsBinding.setItemOrderDetailViewModel(
                        new ItemOrderDetailViewModel(orderLineId, itemView.getContext()));
            } else {
                mItemOrderDetailsBinding.getItemOrderDetailViewModel().setOrderLineId(orderLineId);
            }
        }
    }
}
