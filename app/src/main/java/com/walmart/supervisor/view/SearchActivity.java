package com.walmart.supervisor.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.databinding.ActivitySearchBinding;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.view.adapters.SearchAdapter;
import com.walmart.supervisor.viewmodel.ProductViewModel;

import java.util.Observable;
import java.util.Observer;

public class SearchActivity extends AppCompatActivity implements Observer {

    private ActivitySearchBinding activitySearchBinding;
    private ProductViewModel productViewModel;
    private AutoCompleteTextView autoCompleteTextView;
    private String orderModel;
    private Orders order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if (getIntent().hasExtra(Constants.ORDER_MODEL))
            orderModel = getIntent().getStringExtra(Constants.ORDER_MODEL);

        order = new Gson().fromJson(orderModel, Orders.class);

        initDataBinding();
        // setSupportActionBar(activitySearchBinding.toolbar);
        setupListProductView(activitySearchBinding.listProducts);
        setupObserver(productViewModel);

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);

        autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                Logger.logInfo("SearchActivity", "setOnEditorActionListener", " " + actionId);
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND || actionId == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    InputMethodManager inputMethodManger = (InputMethodManager) getSystemService(getApplicationContext()
                            .INPUT_METHOD_SERVICE);
                    inputMethodManger.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
                    if (autoCompleteTextView.getText().toString().trim().length() > 2) {
                        searchQuery(autoCompleteTextView.getText().toString());
                        handled = true;
                    }
                }

                return handled;
            }
        });

        autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                Logger.logInfo("SearchActivity", "setOnTouchListener", " " + event.getAction());
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (autoCompleteTextView.getRight() - autoCompleteTextView.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        InputMethodManager inputMethodManger = (InputMethodManager) getSystemService(getApplicationContext()
                                .INPUT_METHOD_SERVICE);
                        inputMethodManger.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
                        if (autoCompleteTextView.getText().toString().trim().length() > 2) {
                            searchQuery(autoCompleteTextView.getText().toString());
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void searchQuery(String searchQuery) {
        Logger.logInfo("SearchActivity", "searchQuery ", searchQuery);
        int storeID = 0;
        if(order!=null && order.OrderLineId!=null && order.OrderLineId.size()>0) {
           if( order.OrderLineId.get(0).LocationId!=0) {
               storeID = order.OrderLineId.get(0).LocationId;
           }
        }
        if (storeID == 0) {
            SharedPreferences pref = getSharedPreferences("MyPref", SearchActivity.MODE_PRIVATE);
            storeID = pref.getInt(Constants.LOCATION_ID, 0);
        }
        productViewModel.fetchProductList(searchQuery, storeID);
    }

    private void setupListProductView(RecyclerView listProducts) {
        SearchAdapter adapter = new SearchAdapter();
        Log.d("order", "" + orderModel);
        adapter.setOrder(order);
        listProducts.setAdapter(adapter);
        listProducts.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initDataBinding() {
        activitySearchBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        productViewModel = new ProductViewModel(this);
        activitySearchBinding.setMainViewModel(productViewModel);
    }


    public static Intent launchSearch(Context context, String orderModel) {
        Intent intent = new Intent(context, SearchActivity.class);
        if (orderModel != null)
            intent.putExtra(Constants.ORDER_MODEL, orderModel);
        // intent.putExtra(EXTRA_ORDER, new Gson().toJson(order, Orders.class).toString());
        return intent;
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ProductViewModel) {
            SearchAdapter searchAdapter = (SearchAdapter) activitySearchBinding.listProducts.getAdapter();
            ProductViewModel productViewModel = (ProductViewModel) o;
            searchAdapter.setProductList(productViewModel.getProductList());
        }

    }
}
