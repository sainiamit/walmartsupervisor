package com.walmart.supervisor.view;

import android.content.res.TypedArray;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.walmart.supervisor.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;


/**
 * Created by M1027564 on 7/24/2018.
 */

    public class ProgressView extends ImageView {

        private Animation staggered;

        public ProgressView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            setAnimation(attrs);
        }

        public ProgressView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public ProgressView(Context context) {
            this(context, null);
        }

        private void setAnimation(AttributeSet attrs) {
            setImageResource(R.drawable.wallmart_loading);
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ProgressView);
            int frameCount = a.getInt(R.styleable.ProgressView_frameCount, 12);
            int duration = a.getInt(R.styleable.ProgressView_duration, 2000);
            a.recycle();
            setAnimation(frameCount, duration);
        }

        public void setAnimation(final int frameCount, final int duration) {
            Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.load_rotate_animation);
            a.setDuration(duration);
            a.setInterpolator(new LinearInterpolator());
            staggered = a;
            startAnimation(a);
        }

        @Override
        public void setVisibility(int visibility) {
            if (visibility == View.VISIBLE)
                startAnimation(staggered);
            else
                clearAnimation();
            super.setVisibility(visibility);
        }

    }

