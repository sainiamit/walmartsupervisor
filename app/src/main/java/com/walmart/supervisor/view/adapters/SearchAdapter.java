/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.ItemProductBinding;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.viewmodel.ItemProductViewModel;

import java.util.Collections;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchAdapterViewHolder> {

    private List<Product> productList;
    private Context context;
    private Orders order;

    public SearchAdapter() {
        this.productList = Collections.emptyList();
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    @Override
    public SearchAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductBinding activitySearchBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product,
                        parent, false);
        context = parent.getContext();

        return new SearchAdapterViewHolder(activitySearchBinding, order);
    }

    @Override
    public void onBindViewHolder(SearchAdapterViewHolder holder, int position) {
        holder.bindOrder(productList.get(position));
        Log.d("image", productList.get(position).LargeImage);
        Glide.with(context).load("http:" + productList.get(position).LargeImage).placeholder(R.mipmap.ic_no_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    public static class SearchAdapterViewHolder extends RecyclerView.ViewHolder {
        private Orders order;
        ItemProductBinding activitySearchBinding;
        ImageView img;

        public SearchAdapterViewHolder(ItemProductBinding itemProductBinding, Orders order) {
            super(itemProductBinding.itemProduct);
            this.activitySearchBinding = itemProductBinding;
            img = (ImageView) activitySearchBinding.productImage;
            this.order = order;

        }

        //new ItemProductViewModel(product, itemView.getContext())
        void bindOrder(Product product) {
            if (activitySearchBinding.getProductViewModel() == null) {
                activitySearchBinding.setProductViewModel(new ItemProductViewModel(product, itemView.getContext(), order));
            } else {
                activitySearchBinding.getProductViewModel().setProduct(product);
            }
        }
    }
}
