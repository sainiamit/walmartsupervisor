/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.OrderActivityBinding;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.viewmodel.OrderViewModel;

import java.util.Observable;
import java.util.Observer;

public class OrderActivity extends AppCompatActivity implements Observer {

    private OrderActivityBinding orderActivityBinding;
    private OrderViewModel orderViewModel;
    private static String LocationId;
    private static final String EXTRA_LOCATION_ID = "EXTRA_LOCATION_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtrasFromIntent();
        initDataBinding();
        setSupportActionBar(orderActivityBinding.toolbar);
        setupObserver(orderViewModel);
    }

    private void initDataBinding() {
        orderActivityBinding = DataBindingUtil.setContentView(this, R.layout.order_activity);
        orderViewModel = new OrderViewModel(this, 0, LocationId);
        orderActivityBinding.setMainViewModel(orderViewModel);
        orderActivityBinding.setHandler(this);
        orderActivityBinding.setManager(getSupportFragmentManager());
    }


    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        orderViewModel.reset();
        Log.d("destoryActivity", "Destroyed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public static Intent launchOrder(Context context, String storeID) {
        Intent intent = new Intent(context, OrderActivity.class);
        intent.putExtra(EXTRA_LOCATION_ID, storeID);
        return intent;
    }

    private void getExtrasFromIntent() {
        LocationId = getIntent().getStringExtra(EXTRA_LOCATION_ID);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_logout) {
            new SupervisorAppMessages().showLogoutConfirmDialog(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @BindingAdapter({"bind:handler"})
    public static void bindViewPagerAdapter(final ViewPager view, final OrderActivity activity) {
        final OrderSectionsAdapter adapter = new OrderSectionsAdapter(view.getContext(), activity.getSupportFragmentManager(), LocationId);
        view.setAdapter(adapter);
    }

    @BindingAdapter({"bind:pager"})
    public static void bindViewPagerTabs(final TabLayout view, final ViewPager pagerView) {
        view.setupWithViewPager(pagerView, true);
    }

    @Override
    public void onBackPressed() {
        new SupervisorAppMessages().showExitConfirmDialog(this);
    }

    @Override
    public void update(Observable observable, Object arg) {
    }


}
