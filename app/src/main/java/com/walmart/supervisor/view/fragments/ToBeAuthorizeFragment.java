package com.walmart.supervisor.view.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.LedgerBalanceResponse;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.order.PaymentDetails;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.Logs;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.utils.RangeSeekBar;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.adapters.OrderListAdapter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by M1045813 on 7/21/2018.
 */

public class ToBeAuthorizeFragment extends Fragment {
    private View rootView;
    private TextView labelStatus;
    private RecyclerView recyclerView;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    String mLocationId;
    private List<Orders> orderList = new ArrayList<>();
    OrderListAdapter orderListAdapter;
    FloatingActionButton fab;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    Date date = new Date();
    private SimpleDateFormat dateFormatter;
    private RangeSeekBar rangeSeekbar;
    String astartDate, aendDate;
    private List<Orders> backUpAllPendingOrders = new ArrayList<>();
    int testMin, testMax;
    boolean sdateChange = false, edateChange = false;
    String startDate, endDate;
    boolean isFiltered, isPriceRangeModified = false;
    int filterClickCount = 0;
    private List<Orders> updatedfilterorderList = new ArrayList<>();
    private long totalTime = 0;
    BaseLoadingFragment baseLoadingFragment;
    private static List<Orders> tobefragmentOrderList = new ArrayList<>();


    public static ToBeAuthorizeFragment newInstance(List<Orders> ordersList) {
        // Required empty public constructor
        ToBeAuthorizeFragment fragment = new ToBeAuthorizeFragment();
        tobefragmentOrderList = ordersList;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", getActivity().MODE_PRIVATE);
        if (pref != null) {
            mLocationId = pref.getString("Location_Id", null);
        }
        Log.d("days", "" + Constants.dayinMillisecond);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_order, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        endDate = dateFormatter.format(System.currentTimeMillis());
        startDate = dateFormatter.format(System.currentTimeMillis() - (Constants.dayinMillisecond));
        Log.d("dates", "date" + startDate + "" + endDate);
        setUpViews();
        orderListAdapter = new OrderListAdapter(getContext(), orderList, Constants.PENDING_ORDER);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(orderListAdapter);
    }

    private void setUpViews() {
        labelStatus = (TextView) rootView.findViewById(R.id.label_status);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.list_order);

        if (NetworkUtil.isOnline(getContext())) {
            baseLoadingFragment = BaseLoadingFragment.newInstance("Loading Orders", "Please wait...", !Constants.ISLOTS);
            baseLoadingFragment.show(getActivity().getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
//            if(tobefragmentOrderList!=null) {
//                updateOrderList(tobefragmentOrderList, true);
//                Log.d("count",""+tobefragmentOrderList.size());
            fetchOrderList();
//            }
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(getContext()).build();
            noInternetDialog.show();
        }
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPriceRangeModified = false;
                sdateChange = false;
                edateChange = false;
                SharedPreferences pref = getContext().getSharedPreferences("MyPref", getContext().MODE_PRIVATE);
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.filter_popup);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                ImageButton canceldialog = (ImageButton) dialog.findViewById(R.id.cancel_filter_dialog);
                canceldialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                TextView txtMin = (TextView) dialog.findViewById(R.id.min_range);
                TextView txtMax = (TextView) dialog.findViewById(R.id.max_range);
                RangeSeekBar<Integer> rangeSeekBar = (RangeSeekBar<Integer>) dialog.findViewById(R.id.rangeSeekbar);
                rangeSeekBar.setRangeValues(0, Constants.Max_Range);
                rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                        testMin = minValue;
                        testMax = maxValue;

                        DecimalFormat df = new DecimalFormat("0");
                        // df.setMaximumFractionDigits(2);
                        txtMin.setText(df.format(testMin));
                        txtMax.setText(df.format(testMax));
                        if (testMin != 0 || testMax != 30000) {
                            isPriceRangeModified = true;
                        }


                    }
                });

                Button btnStartDate = (Button) dialog.findViewById(R.id.start_date);
                //btnStartDate.setText(startDate);
                btnStartDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showStartDateDialog(btnStartDate);
                        fromDatePickerDialog.show();
                    }
                });
                Button btnEndDate = (Button) dialog.findViewById(R.id.end_date);
                //btnEndDate.setText(endDate);
                btnEndDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showEndDateDialog(btnEndDate);
                        toDatePickerDialog.show();
                    }
                });
                //filter state display
                if (pref != null && pref.getString("start_date", null) != null || pref.getString("end_date", null) != null || pref.getString("min_range", null) != null || pref.getString("max_range", null) != null) {
                    btnStartDate.setText(pref.getString("start_date", null));
                    btnEndDate.setText(pref.getString("end_date", null));
                    DecimalFormat df1 = new DecimalFormat("0");
                    String selectedMinValue = pref.getString("min_range", null);
                    String selectedMaxValue = pref.getString("max_range", null);

                    if (selectedMinValue != null && selectedMinValue.trim().length() > 0) {
                        int minValue = Integer.parseInt(selectedMinValue.trim());
                        rangeSeekBar.setSelectedMinValue(minValue);
                        txtMin.setText(df1.format((Math.round(Double.valueOf(selectedMinValue) * 100.0) / 100.0)));
                    }
                    if (selectedMaxValue != null && selectedMaxValue.trim().length() > 0) {
                        int maxValue = Integer.parseInt(selectedMaxValue.trim());
                        rangeSeekBar.setSelectedMaxValue(maxValue);
                        txtMax.setText(df1.format((Math.round(Double.valueOf(selectedMaxValue) * 100.0) / 100.0)));
                    }

                }

                Button reset = (Button) dialog.findViewById(R.id.btn_reset);
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //updateOrderList(backUpAllPendingOrders, false);
                        changeOrderDataSet(backUpAllPendingOrders);
                        dialog.dismiss();

                        isPriceRangeModified = false;
                        sdateChange = false;
                        edateChange = false;
                        if (pref != null) {
                            SharedPreferences.Editor editor = pref.edit();
                            editor.remove("start_date");
                            editor.remove("end_date");
                            editor.remove("min_range");
                            editor.remove("max_range");
                            editor.apply();
                            testMin = 0;
                            testMax = Constants.Max_Range;

                        }
                    }
                });
                Button applyFilter = (Button) dialog.findViewById(R.id.btn_apply_filter);
                applyFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AdapterCallback state store
                        SharedPreferences pref = getContext().getSharedPreferences("MyPref", getContext().MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("start_date", btnStartDate.getText().toString());
                        editor.putString("end_date", btnEndDate.getText().toString());
                        editor.putString("min_range", String.valueOf(testMin));
                        editor.putString("max_range", String.valueOf(testMax));
                        editor.apply();

                        astartDate = btnStartDate.getText().toString();
                        aendDate = btnEndDate.getText().toString();

                        if ((sdateChange || edateChange || isPriceRangeModified)) {
                            filterOrders();
                            dialog.dismiss();
                        } else {
                            isPriceRangeModified = false;
                            sdateChange = false;
                            edateChange = false;
                        }
                    }
                });

                dialog.show();
            }
        });


    }

    private void filterOrders() {
        List<Orders> temp_arraylist = new ArrayList<Orders>();
        Date orderedDate = null, sdate = null, edate = null;
        for (int i = 0; i < backUpAllPendingOrders.size(); i++) {
            Orders currentOrder = backUpAllPendingOrders.get(i);
            float currentOrderPrice = currentOrder.AmountPayable;
            try {
                String[] aSplittedArray = currentOrder.OrderDate.split("\\(");
                String aTime = aSplittedArray[1].split("\\+")[0];
                orderedDate = dateFormatter.parse(DateHelper.getDateFromLong(aTime));
                sdate = dateFormatter.parse(astartDate);
                edate = dateFormatter.parse(aendDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if ((sdateChange || edateChange) && !isPriceRangeModified) {
                if (orderedDate != null && (orderedDate.after(sdate) || orderedDate.equals(sdate)) && (orderedDate.before(edate) || orderedDate.equals(edate))) {
                    if (!temp_arraylist.contains(currentOrder)) {
                        temp_arraylist.add(currentOrder);
                    }
                }
                isPriceRangeModified = false;
            } else if (isPriceRangeModified && !sdateChange && !edateChange) {
                if (currentOrderPrice >= testMin && currentOrderPrice <= testMax) {
                    if (!temp_arraylist.contains(currentOrder))
                        temp_arraylist.add(currentOrder);
                }
                sdateChange = false;
                edateChange = false;
            } else {
                if (isPriceRangeModified && (sdateChange || edateChange)) {
                    if (orderedDate != null && (orderedDate.after(sdate) || orderedDate.equals(sdate)) && (orderedDate.before(edate) || orderedDate.equals(edate))) {
                        if (!temp_arraylist.contains(currentOrder)) {
                            temp_arraylist.add(currentOrder);
                        }
                    }
                }
            }

            Log.d("list", "" + temp_arraylist);
        }

        if (isPriceRangeModified && (sdateChange || edateChange) && temp_arraylist.size() > 0) {
            List<Orders> finalOrders = new ArrayList<>();
            for (Orders order : temp_arraylist) {
                if (order.AmountPayable >= testMin && order.AmountPayable <= testMax) {
                    if (!finalOrders.contains(order)) {
                        finalOrders.add(order);
                    }
                }
            }

            temp_arraylist = new ArrayList<>();
            temp_arraylist = finalOrders;
        }

        if (temp_arraylist.size() > 0) {
            //updateOrderList(temp_arraylist, false);
            labelStatus.setVisibility(View.GONE);
            changeOrderDataSet(temp_arraylist);
        } else {
            SupervisorAppMessages.showToast("No Orders found for selected Criteria", getContext());
            labelStatus.setVisibility(View.VISIBLE);
            labelStatus.setText("No Orders Available");
            recyclerView.setVisibility(View.GONE);

        }
    }

    private void showEndDateDialog(Button endDate) {
        Calendar endCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                endDate.setText(dateFormatter.format(newDate.getTime()));
                edateChange = true;
            }

        }, endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONTH), endCalendar.get(Calendar.DAY_OF_MONTH));
        toDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        toDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - (Constants.dayinMillisecond));
    }

    private void showStartDateDialog(Button startDate) {
        Calendar startCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                sdateChange = true;
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                startDate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis()));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - (Constants.dayinMillisecond));
    }

    private void fetchOrderList() {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(getContext());
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("FromDate", startDate);
        aParams.put("ToDate", endDate);
        aParams.put("UserId", "");
        aParams.put("LocationId", mLocationId);
        //aParams.put("Status","P");
        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/History").setMethod(APIMethod.POST).build();
        Disposable disposable = orderService.fetchOrderHistory(aDAPIHelper.getFinalUrl(), aParams)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        updateOrderList(orderResponse.getOrderList(), true);
//                        backUpAllPendingOrders = new ArrayList<>();
//                        backUpAllPendingOrders = orderResponse.getOrderList();
                        Logs.e("OrderTotal",new Gson().toJson(orderResponse.getOrderList()));
                        Logger.logInfo("fetchOrderHistory", "--- Order History Success MESSAGE CODE " + orderResponse.messageCode, "");

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        Logger.logInfo("fetchOrderHistory", "--- Error " + throwable.getMessage(), "");
                        throwable.printStackTrace();
                        if (baseLoadingFragment != null)
                            baseLoadingFragment.dismiss();
                        labelStatus.setVisibility(View.VISIBLE);
                        labelStatus.setText(getContext().getString(R.string.error_loading_order));
                    }
                });
        compositeDisposable.add(disposable);
    }

    public void updateOrderList(List<Orders> ordersList, boolean isbackReq) {
        //Logger.logInfo("updateOrderList", "--- ordersList.size() " + ordersList.size(), "");
        List<Orders> filteredOrdersList = new ArrayList<>();
        for (Orders order : ordersList) {
            if (order.Status.equalsIgnoreCase("P")) {
                Logs.e("***", "Pending OrderID=" + order.OrderId);
                boolean isPaymentCredit = false;
                PaymentLoop:
                for (int i = 0; i < order.PaymentDetails.size(); i++) {
                    PaymentDetails paymentDetails = order.PaymentDetails.get(i);
                    if (paymentDetails.PaymentType.equalsIgnoreCase("TPG")) {
                        isPaymentCredit = true;
                        break PaymentLoop;
                    }
                }
                if (!isPaymentCredit) {
                    filteredOrdersList.add(order);
                    Logs.e("***", "Size=" + filteredOrdersList.size() + " Current Order=" + order.OrderId);
                }
            }
        }
        //changeOrderDataSet(filteredOrdersList);
        updatedfilterorderList.clear();
        cacluateLedgerBalance(filteredOrdersList, isbackReq);
        Log.d("count", "" + filteredOrdersList.size());


    }

    private void cacluateLedgerBalance(List<Orders> filteredOrdersList, boolean isbackReq) {
        new Thread(new Runnable() {
            public void run() {
                for (Orders orders : filteredOrdersList) {
                    SupervisorApplication supervisorApplication = SupervisorApplication.create(getContext());
                    OrderService orderService = supervisorApplication.getOrderService();
                    DeveloperApiHelper GET_LEDGER_BALANCE_URL = new DeveloperApiHelper.Builder().endPoint("Order/BalanceEnquiry").setMethod(APIMethod.POST).build();
                    Logs.e("***", "Get Ledger URL - " + GET_LEDGER_BALANCE_URL.getFinalUrl());
                    Logs.e("***", "Get Ledger Inputs - provider=LEDGER, UserID=" + orders.UserId);
                    Disposable disposable = orderService.getLedgerBalance(GET_LEDGER_BALANCE_URL.getFinalUrl(), Constants.MERCHANT_ID, "LEDGER", orders.UserId, "")
                            .subscribeOn(supervisorApplication.subscribeScheduler())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<LedgerBalanceResponse>() {
                                @Override
                                public void accept(LedgerBalanceResponse ledgerBalanceResponse) throws Exception {
                                    Logs.e("***", "Get Ledger Response - " + new Gson().toJson(ledgerBalanceResponse));
                                    if (ledgerBalanceResponse != null && ledgerBalanceResponse.messageCode.equalsIgnoreCase("1004") && ledgerBalanceResponse.BalanceEnquiryResponseView != null) {
                                        orders.ledgerBalanceResponse = ledgerBalanceResponse;
                                        float balance = Float.parseFloat(ledgerBalanceResponse.BalanceEnquiryResponseView.Balance);
                                        //before build
//                                        float percentOfbalance = (float) (balance * (70.0f / 100.0f));
//                                        if (orders.TotalAmount >= percentOfbalance && orders.TotalAmount < balance) {
                                        float percentOfOV = (float) (orders.AmountPayable * (70.0f / 100.0f));
                                        if(balance!=0f) {
                                            if ((balance >= percentOfOV) && (balance < orders.AmountPayable)) {
                                                //if (balance != 0f) {
                                                updatedfilterorderList.add(orders);
                                                Log.d("size", "" + updatedfilterorderList.size());
                                            } else {
                                                Logs.e("***", "Pending Order total is not meeting Ledger Balance condition, OT=" + orders.AmountPayable + ", LB(70-100)=" + balance + ", OID=" + orders.OrderId);
                                            }
                                        }
                                    } else {
                                        Logs.e("***", "Error so skipping this step n adding order without ledger");
                                        //updatedfilterorderList.add(orders);
                                    }
                                }
                            }, new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    if (baseLoadingFragment != null)
                                        baseLoadingFragment.dismiss();
                                }
                            });
                    compositeDisposable.add(disposable);
                }
            }
        }).start();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                changeOrderDataSet(updatedfilterorderList);
                if (isbackReq) {
                    backUpAllPendingOrders = new ArrayList<>();
                    backUpAllPendingOrders = updatedfilterorderList;
                }
            }
        }, 15000);

    }

    private void changeOrderDataSet(List<Orders> orders) {
        Log.d("yes", "first");
        if (orders.size() > 0) {
            this.orderList.clear();
            this.orderList.addAll(orders);
            Collections.sort(orderList, new Comparator<Orders>() {
                public int compare(Orders m1, Orders m2) {
                    return m2.OrderDate.compareTo(m1.OrderDate);
                }
            });
            orderListAdapter.notifyDataSetChanged();
            labelStatus.setVisibility(View.GONE);
            if (baseLoadingFragment != null)
                baseLoadingFragment.dismiss();
            recyclerView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else {
            if (baseLoadingFragment != null)
                baseLoadingFragment.dismiss();
            labelStatus.setVisibility(View.VISIBLE);
            labelStatus.setText("No Orders Available");
        }
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
