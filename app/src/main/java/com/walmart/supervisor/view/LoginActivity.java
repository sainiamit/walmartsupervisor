/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.walmart.supervisor.R;
import com.walmart.supervisor.databinding.LoginActivityBinding;
import com.walmart.supervisor.viewmodel.LoginViewModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Srihari on 08/06/2018.
 */

public class LoginActivity extends AppCompatActivity implements Observer {

    private static final String EXTRA_LOGIN = "EXTRA_LOGIN";

    private LoginActivityBinding loginActivityBinding;
    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        setupObserver(loginViewModel);
    }

    private void initDataBinding() {
        loginActivityBinding = DataBindingUtil.setContentView(this, R.layout.login_activity);
        loginViewModel = new LoginViewModel(this,this);
        loginActivityBinding.setLoginViewModel(loginViewModel);
    }

    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    public static Intent launchLogin(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    public void update(Observable observable, Object o) {

    }
}
