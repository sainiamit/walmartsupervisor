package com.walmart.supervisor.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.adapters.AdapterCallback;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;
import com.walmart.supervisor.view.fragments.InpickingFragment;
import com.walmart.supervisor.view.fragments.ToBeAuthorizeFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class OrderListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private static String LocationId;
    private static final String EXTRA_LOCATION_ID = "EXTRA_LOCATION_ID";
    private Context context;
    private AdapterCallback listener;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    String startDate, endDate;
    private SimpleDateFormat dateFormatter;
    BaseLoadingFragment baseLoadingFragment;
    List<Orders> ordersList = new ArrayList<>();

    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        context = OrderListActivity.this;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getExtrasFromIntent();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        endDate = dateFormatter.format(System.currentTimeMillis());
        startDate = dateFormatter.format(System.currentTimeMillis() - (Constants.dayinMillisecond));
        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
        if (pref != null) {
            LocationId = pref.getString("Location_Id", null);
        }
        viewPager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(viewPager);
//        if (NetworkUtil.isOnline(context)) {
//            baseLoadingFragment = BaseLoadingFragment.newInstance("Loading Orders", "Please wait...", true);
//            baseLoadingFragment.show(OrderListActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
//            fetchOrderList();
//        } else {
//            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
//            noInternetDialog.show();
//        }
        //setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setListener(listener);
    }

    public void setListener(AdapterCallback listener) {
        this.listener = listener;
    }

    private void getExtrasFromIntent() {
        LocationId = getIntent().getStringExtra(EXTRA_LOCATION_ID);
    }

    public static Intent launchOrder(Context context) {
        Intent intent = new Intent(context, OrderListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.putExtra(EXTRA_LOCATION_ID, storeID);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                new SupervisorAppMessages().showLogoutConfirmDialog(this);
                return true;
            case R.id.menu_refresh:
                SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                if (pref != null) {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.remove("start_date");
                    editor.remove("end_date");
                    editor.remove("min_range");
                    editor.remove("max_range");
                    editor.remove("in_start_date");
                    editor.remove("in_end_date");
                    editor.remove("in_min_range");
                    editor.remove("in_max_range");
                    editor.apply();
                }
//                if (NetworkUtil.isOnline(context)) {
//                    baseLoadingFragment = BaseLoadingFragment.newInstance("Loading Orders", "Please wait...", true);
//                    baseLoadingFragment.show(OrderListActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
//                    fetchOrderList();
//                } else {
//                    NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
//                    noInternetDialog.show();
//                }
                setupViewPager(viewPager);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        new SupervisorAppMessages().showExitConfirmDialog(this);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(ToBeAuthorizeFragment.newInstance(ordersList), Constants.PENDING_ORDER);
        adapter.addFragment(InpickingFragment.newInstance(ordersList), Constants.BDA_COMMENTS_ORDER);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        onTabSelectedListener(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void fetchOrderList() {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(this);
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("FromDate", startDate);
        aParams.put("ToDate", endDate);
        aParams.put("UserId", "");
        aParams.put("LocationId", LocationId);
        //aParams.put("LocationId", ",248,250,212");
        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/History").setMethod(APIMethod.POST).build();
        Disposable disposable = orderService.fetchOrderHistory(aDAPIHelper.getFinalUrl(), aParams)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        ordersList = orderResponse.getOrderList();
                        Log.d("count", "" + ordersList.size());
                        Logger.logInfo("fetchOrderHistory", "--- Order History Success MESSAGE CODE " + orderResponse.messageCode, "");
                        if (baseLoadingFragment != null) {
                            baseLoadingFragment.dismiss();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        Logger.logInfo("fetchOrderHistory", "--- Error " + throwable.getMessage(), "");
                        throwable.printStackTrace();
                        if (baseLoadingFragment != null)
                            baseLoadingFragment.dismiss();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}
