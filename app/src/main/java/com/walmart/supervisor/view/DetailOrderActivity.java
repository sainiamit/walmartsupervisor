package com.walmart.supervisor.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.data.QueryConstants;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.httpclient.FrontEndApiHelper;
import com.walmart.supervisor.model.AddOrderLineItemRequest;
import com.walmart.supervisor.model.CancelResponse;
import com.walmart.supervisor.model.DeleteOrderLineItemRequest;
import com.walmart.supervisor.model.DeveloperAPIResponse;
import com.walmart.supervisor.model.LedgerBalanceResponse;
import com.walmart.supervisor.model.OrderStatusResponse;
import com.walmart.supervisor.model.ProcessTransactionModel;
import com.walmart.supervisor.model.ProcessTransactionResponse;
import com.walmart.supervisor.model.ProductResponse;
import com.walmart.supervisor.model.UpdateTransactionRequest;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.order.PaymentDetails;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.Logs;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.adapters.AdapterCallback;
import com.walmart.supervisor.view.adapters.DetailOrderItemsAdapter;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by M1045813 on 7/12/2018.
 */

public class DetailOrderActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AdapterCallback {

    private static final String EXTRA_ORDER = "EXTRA_ORDER";
    private static final String EXTRA_DETAIL_ACTIVITY_NAME = "activityName";
    Orders order;
    String[] reasons = {"Order not in delivery Area", "Incorrect Contact information", "Order less than order delivery minimum value", "Order not delivered",
            "SLA expired in Acknowledgment", "SLA expired in New Order", "Order Expired due to Quantity Mismatch from Aggregator", "Retailer did Not allocate items to aggregator",
            "Cancellation as per customer request", "Item Not available/Unsellelable", "Item Not available/Unsellelable", "Order placed by mistake", "Order delivery Is taking too much time",
            "By mistake ordered extra quantity", "Bought it from somewhere else", "I want to change my shipping address", "I want to change my mode of payment",
            "Product cost Is high compared to another website", "Shipping charges are too high", "Payment not received", "Product Not required", "Going Out Of station",
            "Other"};
    String usercomment;
    String merchantID;
    List<OrderLineId> orderLineIdList = new ArrayList<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DetailOrderItemsAdapter detailOrderItemsAdapter;

    private RecyclerView recyclerView;
    private TextView orderid, orderdate, recalculated_charge, orderDeliveryCharge, ledgerBalance;
    private AppCompatImageButton add, refresh;
    Context context;
    Toolbar toolbar;
    // private Product product;
    List<Product> productList = new ArrayList<>();
    private List<OrderLineId> orderLineIdRemovedList = new ArrayList<>();
    private List<OrderLineId> orderLineIdPriceList = new ArrayList<>();
    EditText comment;
    private TextView cancelOrder, confirmOrder, recalculateOrder, noofItem, recalculateItem, grandTotal, recalculategrandTotal, memberName, phoneNo, paymentMode, paymentReceived, memberComment, order_promotion, recalculated_promotion;
    BaseLoadingFragment baseLoadingFragment;
    private BaseLoadingFragment baseLoadingF;
    private String today_date;
    private SimpleDateFormat dateFormatter;
    private String agentId;
    private String advn = "", advc = "";
    BottomSheetBehavior sheetBehavior;
    OrderLineId neworderLineId;
    private Product product;
    String title;
    private ImageView imgUpDown;
    private boolean isChangeLoss = false;
    float shippingCost = 0f;
    private int listSizeEditOrderLineItem = 0, listLoopSizeEditOrderLineItem = 0;
    private List<Orders> orderInfoAPIRespAfterAddRemoveItem = new ArrayList<>();
    private int orderPaymentListSize = 0, orderPaymentLoopSize = 0;
    private String locationCode = null;
    private boolean isDeleteItemAPICalling = false;
    private boolean isAddItemAPICalling = false;
    private boolean isEditOrderLinePriceOverrideAPICalled = false;
    private boolean isRequiredToUpdateAdapter = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_order_activity);
        try {
            context = DetailOrderActivity.this;
            setUpViews();
            if (NetworkUtil.isOnline(context)) {
                fetchOrderDetailList();
            } else {
                NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                noInternetDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
            if (pref != null) {
                String productinfo = pref.getString("productInfo", null);
                if (productinfo != null) {
                    product = new Gson().fromJson(productinfo, Product.class);
                    if (product != null && product.orderId == order.OrderId) {
                        neworderLineId = new OrderLineId();
                        neworderLineId.SKU = product.Sku;
                        neworderLineId.Quantity = 1;
                        neworderLineId.MRP = String.valueOf(product.MRP);
                        neworderLineId.ProductPrice = product.WebPrice;
                        neworderLineId.ImageUrl = "http:" + product.SmallImage;
                        neworderLineId.ProductTitle = product.Title;
                        neworderLineId.OrderId = order.OrderId;
                        neworderLineId.UpdatedQuantity = 1;
                        neworderLineId.UpdatedPrice = product.WebPrice;
                        neworderLineId.ProductId = product.ID;
                        neworderLineId.product = product;
                        neworderLineId.IsAdded = true;
                        confirmOrder.setEnabled(false);
                        recalculateOrder.setEnabled(true);
                        isChangeLoss = true;

                        boolean productIsAlreadyAvailable = false;

                        for (OrderLineId lineId : orderLineIdList) {
                            if (lineId.SKU.equalsIgnoreCase(neworderLineId.SKU)) {
                                productIsAlreadyAvailable = true;
                                if (lineId.UpdatedQuantity <= neworderLineId.product.Inventory) {
                                    lineId.UpdatedQuantity = lineId.UpdatedQuantity + 1;
                                }
                            }
                        }
                        if (!productIsAlreadyAvailable) {
                            orderLineIdList.add(neworderLineId);
                            detailOrderItemsAdapter.setOrders(orderLineIdList);
                            detailOrderItemsAdapter.notifyDataSetChanged();
                        } else {
                            detailOrderItemsAdapter.setOrders(orderLineIdList);
                            detailOrderItemsAdapter.notifyDataSetChanged();
                        }
                    }
                }

                SharedPreferences.Editor editor = pref.edit();
                editor.remove("productInfo");
                editor.apply();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);//hide title
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.list_order_items);
        orderid = (TextView) findViewById(R.id.order_id);
        orderdate = (TextView) findViewById(R.id.order_date);
        add = (AppCompatImageButton) findViewById(R.id.btn_add);
        refresh = (AppCompatImageButton) findViewById(R.id.btn_refresh);
        cancelOrder = (TextView) findViewById(R.id.cancel_order);
        confirmOrder = (TextView) findViewById(R.id.confirm_order);
        recalculateOrder = (TextView) findViewById(R.id.recalculate_order);
        noofItem = (TextView) findViewById(R.id.noOfItem);
        recalculateItem = (TextView) findViewById(R.id.recalculatedNoOfItem);
        grandTotal = (TextView) findViewById(R.id.grandTotalValue);
        recalculategrandTotal = (TextView) findViewById(R.id.recalculatedGrandTotal);
        memberName = (TextView) findViewById(R.id.member_name);
        phoneNo = (TextView) findViewById(R.id.phone_no);
        memberComment = (TextView) findViewById(R.id.member_comment);
        paymentMode = (TextView) findViewById(R.id.payment_type);
        paymentReceived = (TextView) findViewById(R.id.payment_received);
        recalculated_charge = (TextView) findViewById(R.id.recalculated_delivery_charge);
        orderDeliveryCharge = (TextView) findViewById(R.id.order_delivery_charge);
        ledgerBalance = (TextView) findViewById(R.id.ledger_balane);
        LinearLayout layoutBottomSheet = (LinearLayout) findViewById(R.id.orderdetails_bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        imgUpDown = (ImageView) findViewById(R.id.imageUpDown);
        order_promotion = (TextView) findViewById(R.id.order_promotion);
        recalculated_promotion = (TextView) findViewById(R.id.recalculated_promotion);
        //TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        recalculateOrder.setEnabled(false);
        confirmOrder.setEnabled(true);

        String orderSt = getIntent().hasExtra(EXTRA_ORDER) ? getIntent().getStringExtra(EXTRA_ORDER) : null;
        title = getIntent().hasExtra(EXTRA_DETAIL_ACTIVITY_NAME) ? getIntent().getStringExtra(EXTRA_DETAIL_ACTIVITY_NAME) : "Order Details";
        //toolbarTitle.setText(title);
        toolbar.setTitle(title);


        if (orderSt != null) {
            order = new Gson().fromJson(orderSt, Orders.class);
            orderid.setText(String.valueOf(order.OrderId));
            String[] aSplittedArray = order.OrderDate.split("\\(");
            String aTime = aSplittedArray[1].split("\\+")[0];
            orderdate.setText(DateHelper.getDate(aTime));
            merchantID = String.valueOf(order.MerchantId);
            if (order.BillFirstname != null && order.BillFirstname.trim().length() > 0) {
                memberName.setText(order.BillFirstname + " " + order.BillLastname);
            } else if (order.ShipFirstname != null && order.ShipFirstname.trim().length() > 0) {
                memberName.setText(order.ShipFirstname + " " + order.ShipLastname);
            } else {
                memberName.setText("Name not available");
            }

            phoneNo.setText(order.ShipPhone);
            if (order.PaymentDetails.size() > 0) {
                paymentMode.setText(order.PaymentDetails.get(0).PaymentType);
            }
            paymentReceived.setText(String.format("%.2f", order.AmountPayable));
//            if (title.equalsIgnoreCase(Constants.BDA_COMMENTS_ORDER)) {
//            if (NetworkUtil.isOnline(context)) {
//                CalculateLedgerBalance(order);
//            } else {
//                NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
//                noInternetDialog.show();
//            }
//            }

            if (order.GiftMessage != null) {
                memberComment.setText(order.GiftMessage);
            } else {
                memberComment.setText("NA");
            }

            ledgerBalanceSet();

        }
        detailOrderItemsAdapter = new DetailOrderItemsAdapter(this, orderLineIdList, order, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        baseLoadingFragment = BaseLoadingFragment.newInstance("Loading Order", "Please wait...", !Constants.ISLOTS);
        baseLoadingFragment.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
        if (Constants.ISLOTS)
            add.setVisibility(View.GONE);
        else
            add.setVisibility(View.VISIBLE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isOnline(context)) {
                    SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.remove("productInfo");
                    editor.apply();
                    proceedToSearchActivity();
                } else {
                    NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                    noInternetDialog.show();
                }
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (baseLoadingFragment != null)
                    baseLoadingFragment.dismiss();

                if (isChangeLoss) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);

                    alert.setMessage("Your Changes Will Be Lost?");
                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            baseLoadingFragment = BaseLoadingFragment.newInstance("Refreshing Order items", "Please wait...", !Constants.ISLOTS);
                            baseLoadingFragment.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            if (NetworkUtil.isOnline(context)) {
                                orderLineIdRemovedList = new ArrayList<>();
                                orderLineIdPriceList = new ArrayList<>();
                                fetchOrderDetailList();
                                isChangeLoss = false;
                            } else {
                                NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                                noInternetDialog.show();
                            }
                        }
                    });
                    alert.create().show();
                } else {
                    baseLoadingFragment = BaseLoadingFragment.newInstance("Refreshing Order items", "Please wait...", !Constants.ISLOTS);
                    baseLoadingFragment.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                    if (NetworkUtil.isOnline(context)) {
                        fetchOrderDetailList();
                        isChangeLoss = false;
                    } else {
                        NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                        noInternetDialog.show();
                    }
                }

            }
        });

        recalculateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    float removedItemPromotionTotal = 0f, totalPromotion = 0f;

//                    for (OrderLineId lineId : orderLineIdRemovedList) {
//                        if ((lineId.Quantity > lineId.CancelQuantity) && lineId.TotalPromotionDiscount > 0f) {
//                            if (removedItemPromotionTotal > 0f)
//                                removedItemPromotionTotal = removedItemPromotionTotal + lineId.TotalPromotionDiscount;
//                            else
//                                removedItemPromotionTotal = lineId.TotalPromotionDiscount;
//
//                            Logs.e("***", "RemovedItemPromotion - " + removedItemPromotionTotal);
//                        }
//                    }

                    float rgt = 0;
                    int totalCount = 0;

                    if (orderLineIdList.size() > 0) {
                        for (OrderLineId orderLineId : orderLineIdList) {
                            if (orderLineId.Quantity > orderLineId.CancelQuantity) {
                                totalCount++;

                                float price = orderLineId.UpdatedPrice;
                                if (price == 0f) {
                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Alert", "Since updated price is 0, So we are considering Ordered price as updated price", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            orderLineId.UpdatedPrice = orderLineId.ProductPrice;
                                            isRequiredToUpdateAdapter = true;
                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                    price = orderLineId.ProductPrice;
                                }

                                int qty = orderLineId.UpdatedQuantity;
                                if (qty == 0) {
                                    orderLineId.UpdatedQuantity = orderLineId.Quantity;
                                    isRequiredToUpdateAdapter = true;
                                } else {
                                    if (orderLineId.product != null)
                                        if (qty > orderLineId.product.Inventory) {
                                            orderLineId.UpdatedQuantity = orderLineId.product.Inventory;
                                            isRequiredToUpdateAdapter = true;
                                        }
                                }

                                rgt = rgt + (orderLineId.UpdatedQuantity * price);

                                totalPromotion = totalPromotion + orderLineId.TotalPromotionDiscount;

                                if (totalPromotion > 0f)
                                    if (totalPromotion > rgt) {
                                        if (baseLoadingFragment != null)
                                            baseLoadingFragment.dismiss();

                                        BaseLoadingFragment.newInstance("Alert", "Recalculated unit price should be more than promotion value, Please update/check your item & its available promotion", false, true)
                                                .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                    }
                            }
                        }

                        if (isRequiredToUpdateAdapter) {
                            detailOrderItemsAdapter.setOrders(orderLineIdList);
                            detailOrderItemsAdapter.notifyDataSetChanged();
                        }
                    }
                    recalculateItem.setText(String.valueOf(totalCount));

                    recalculated_promotion.setText(String.format("%.2f", totalPromotion));
                    float recalTotal = (rgt + shippingCost);
                    //Logs.e("***", "Recalculated Total = updated (Price * Qty) + ShippingCost - removedItemPromotion = " + (recalTotal));
                    recalculategrandTotal.setText(String.format("%.2f", recalTotal - totalPromotion));
                    if (!confirmOrder.isEnabled()) {
                        confirmOrder.setEnabled(true);
                        recalculateOrder.setEnabled(false);
                    }

                    if (baseLoadingFragment != null)
                        baseLoadingFragment.dismiss();

                    baseLoadingFragment = BaseLoadingFragment.newInstance("Recalculating Order Total", "Please wait...", !Constants.ISLOTS);
                    baseLoadingFragment.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (baseLoadingFragment != null)
                                baseLoadingFragment.dismiss();

                            if (recalTotal <= 0) {
                                BaseLoadingFragment.newInstance("Alert", "Recalculated Order Total value should be more than 1, Please update/check your item & its available promotion", false, true)
                                        .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            }
                        }
                    }, 400);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);

                alert.setMessage("Are you sure you want to Confirm Order?");
                alert.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });
                alert.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (baseLoadingFragment != null)
                                    baseLoadingFragment.dismiss();

                                boolean isPromotionValidationMatched = false;
                                float rgt = 0f, totalPromotion = 0f;
                                if (orderLineIdList.size() > 0) {
                                    for (OrderLineId orderLineId : orderLineIdList) {
                                        if (orderLineId.Quantity > orderLineId.CancelQuantity) {

                                            float price = orderLineId.UpdatedPrice;
                                            if (price == 0f) {
                                                price = orderLineId.ProductPrice;
                                            }
                                            rgt = rgt + (orderLineId.UpdatedQuantity * price);

                                            totalPromotion = totalPromotion + orderLineId.TotalPromotionDiscount;

                                            if (totalPromotion > 0f)
                                                if (totalPromotion > rgt) {
                                                    isPromotionValidationMatched = true;
                                                }
                                        }
                                    }


                                    if (isPromotionValidationMatched) {
                                        if (baseLoadingFragment != null)
                                            baseLoadingFragment.dismiss();

                                        BaseLoadingFragment.newInstance("Alert", "Recalculated unit price should be more than promotion value, Please update/check your item & its available promotion", false, true)
                                                .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                    } else {

                                        boolean isWebPriceIsMoreThanMRP = false;
                                        if (orderLineIdPriceList != null && orderLineIdPriceList.size() > 0) {
                                            for (OrderLineId lineId : orderLineIdPriceList) {
                                                float mrp = 0f;
                                                if (lineId.MRP != null)
                                                    mrp = Float.parseFloat(lineId.MRP);
                                                if (mrp == 0)
                                                    mrp = lineId.product.MRP;
                                                if (mrp < lineId.UpdatedPrice) {
                                                    Logs.e("***", "MRP = " + mrp + ", UpdatedPrice = " + lineId.UpdatedPrice);
                                                    isWebPriceIsMoreThanMRP = true;
                                                }
                                            }
                                        }

                                        if (isWebPriceIsMoreThanMRP) {
                                            if (baseLoadingFragment != null)
                                                baseLoadingFragment.dismiss();

                                            BaseLoadingFragment.newInstance("Alert", "Line item Price should not be more then MRP", false, true)
                                                    .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                        } else {
                                            float recalculatedFloat = Float.parseFloat(recalculategrandTotal.getText().toString());
                                            if (recalculatedFloat > 0) {
                                                if (Constants.ISLOTS) {
                                                    validateRemovedItems();
                                                } else {
                                                    if (recalculatedFloat <= Float.parseFloat(order.ledgerBalanceResponse.BalanceEnquiryResponseView.Balance)) {
                                                        baseLoadingF = BaseLoadingFragment.newInstance("Authorizing Order", "Please wait...", !Constants.ISLOTS);
                                                        baseLoadingF.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                                        //authorizeOrderAPI();
                                                        if (NetworkUtil.isOnline(context)) {
                                                            validateRemovedItems();
                                                            isAddItemAPICalling = false; // Initially its false
//                                        validateRemoveandQtyChange(orderLineIdList);
                                                        } else {
                                                            if (baseLoadingF != null)
                                                                baseLoadingF.dismiss();
                                                            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                                                            noInternetDialog.show();
                                                        }

                                                    } else {
                                                        if (baseLoadingFragment != null)
                                                            baseLoadingFragment.dismiss();

                                                        BaseLoadingFragment.newInstance("Alert", "Recalculated Order Total should be less than or equal to ledger balance.", false, true)
                                                                .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                                    }
                                                }
                                            } else {
                                                if (baseLoadingFragment != null)
                                                    baseLoadingFragment.dismiss();

                                                BaseLoadingFragment.newInstance("Alert", "Recalculated Order Total should be more than zero, Can't authorize order with Order Total as zero", false, true)
                                                        .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                            }
                                        }
                                    }
                                } else {
                                    if (baseLoadingFragment != null)
                                        baseLoadingFragment.dismiss();

                                    BaseLoadingFragment.newInstance("Alert", "No Items available to Authorize this Order", false, true)
                                            .show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            }
                        });

                alert.create().show();
            }
        });

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                // Include dialog.xml file
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.cancel_order_popup);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner);
                spinner.setOnItemSelectedListener(DetailOrderActivity.this);

                ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, reasons);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinner.setAdapter(aa);

                comment = (EditText) dialog.findViewById(R.id.edit_text_comment);
                Button cancel = (Button) dialog.findViewById(R.id.btn_cancel_order);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (spinner.isSelected()) {
                            SupervisorAppMessages.showToast("Please select type", context);
                        } else if (comment.getText().toString().length() == 0) {
                            SupervisorAppMessages.showToast("Please comment", context);
                        } else {
                            usercomment = comment.getText().toString();
                            baseLoadingFragment = BaseLoadingFragment.newInstance("Cancel Order", "Please wait...", true);
                            baseLoadingFragment.show(DetailOrderActivity.this.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            if (NetworkUtil.isOnline(context)) {
                                doCancelOrder();
                            } else {
                                NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
                                noInternetDialog.show();
                            }
                            dialog.dismiss();
                        }
                    }
                });

                ImageButton canceldialog = (ImageButton) dialog.findViewById(R.id.cancel_dialog);
                canceldialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        today_date = dateFormatter.format(System.currentTimeMillis());
        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
        if (pref != null) {
            agentId = pref.getString("Agent_Id", null);
        }

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        imgUpDown.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        imgUpDown.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_black_24dp));
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    private void ledgerBalanceSet() {
        String advcAdvn = "";
        if (order != null && order.ledgerBalanceResponse != null && order.ledgerBalanceResponse.BalanceEnquiryResponseView != null && order.ledgerBalanceResponse.BalanceEnquiryResponseView.Balance != null) {
            String ledgerBalanceFloat = order.ledgerBalanceResponse.BalanceEnquiryResponseView.Balance;
            if (order.ledgerBalanceResponse.BalanceEnquiryResponseView.Message != null && order.ledgerBalanceResponse.BalanceEnquiryResponseView.Message.trim().length() > 0) {
                LedgerBalanceResponse.LedgerAdvanceStatement BGStatement;
                BGStatement = new Gson().fromJson(order.ledgerBalanceResponse.BalanceEnquiryResponseView.Message.trim(), LedgerBalanceResponse.LedgerAdvanceStatement.class);
                if (BGStatement != null && BGStatement.Amount != null && BGStatement.Amount.TenderTypes != null && BGStatement.Amount.TenderTypes.size() > 0) {
                    // for(LedgerBalanceResponse.TenderTypes tenderTypes:BGStatement.Amount.TenderTypes) {
                    try {
//                        for (int k = 0; k < (BGStatement.Amount.TenderTypes.size() - 1); k++) {
                        for (LedgerBalanceResponse.TenderTypes TenderType : BGStatement.Amount.TenderTypes) {
                            if (TenderType != null && TenderType.TENDER_NAME != null && TenderType.REM_LIMIT != 0f) {
//                                if (k == 0) {
//                                    String strAdvn = null;
//                                    if (BGStatement.Amount.TenderTypes.get(0).TENDER_NAME.equalsIgnoreCase("ADVN")) {
//                                        strAdvn = "Non Cash Advance";
//                                    }
//                                    advc = strAdvn + ": " + String.format("%.2f", BGStatement.Amount.TenderTypes.get(0).REM_LIMIT);
//                                }
//                                if (k == 1) {
//                                    String strAdvc = null;
//                                    if (BGStatement.Amount.TenderTypes.get(1).TENDER_NAME.equalsIgnoreCase("ADVC")) {
//                                        strAdvc = "Cash Advance";
//                                    }
//                                    advn = strAdvc + ": " + String.format("%.2f", BGStatement.Amount.TenderTypes.get(1).REM_LIMIT);
//                                }

                                if (advcAdvn.trim().length() == 0) {
                                    advcAdvn = getDetailTenderName(TenderType.TENDER_NAME) + ": " + String.format("%.2f", TenderType.REM_LIMIT);
                                } else {
                                    advcAdvn = (advcAdvn) + ",\n " + (getDetailTenderName(TenderType.TENDER_NAME) + ": " + String.format("%.2f", TenderType.REM_LIMIT));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
//            if (advn.isEmpty()) {
//                ledgerBalance.setText(ledgerBalanceFloat + "(" + advc + ")");
//            } else {
            ledgerBalance.setText(ledgerBalanceFloat + "\n(" + advcAdvn + ")");
//            }

        } else {
            ledgerBalance.setText("0");
        }
    }

    private String getDetailTenderName(String tender_name) {
        try {
            if (tender_name != null) {
                if (tender_name.equalsIgnoreCase("ADVC"))
                    return "Cash Advance";
                else if (tender_name.equalsIgnoreCase("ADVN"))
                    return "Non Cash Advance";
                else
                    return tender_name;
            } else {
                return "Ledger";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void CalculateLedgerBalance(Orders orders) {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(DetailOrderActivity.this);
        OrderService orderService = supervisorApplication.getOrderService();
        DeveloperApiHelper GET_LEDGER_BALANCE_URL = new DeveloperApiHelper.Builder().endPoint("Order/BalanceEnquiry").setMethod(APIMethod.POST).build();
        Disposable disposable = orderService.getLedgerBalance(GET_LEDGER_BALANCE_URL.getFinalUrl(), Constants.MERCHANT_ID, "LEDGER", orders.UserId, "")
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LedgerBalanceResponse>() {
                    @Override
                    public void accept(LedgerBalanceResponse ledgerBalanceResponse) throws Exception {
                        float balance = Float.parseFloat(ledgerBalanceResponse.BalanceEnquiryResponseView.Balance);
                        orders.ledgerBalanceResponse = ledgerBalanceResponse;
                        ledgerBalanceSet();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (baseLoadingF != null)
                            baseLoadingF.dismiss();

                        String body = "Failed";
                        BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                        loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {

                            }
                        });
                        loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void validateRemovedItems() {
        Log.d("size", "" + orderLineIdRemovedList.size());

        if (orderLineIdRemovedList != null && orderLineIdRemovedList.size() > 0) {
            for (int k = 0; k < orderLineIdRemovedList.size(); k++) {
                try {
                    SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
                    OrderService orderService = supervisorApplication.getOrderService();
                    DeleteOrderLineItemRequest itemRequest = new DeleteOrderLineItemRequest();
                    itemRequest.setCancelReason("Recalculate Order");
                    itemRequest.setComment("Recalculate Order");
                    itemRequest.setDate(today_date); // TODAY DATE yyyy-MM-dd
                    itemRequest.setDisplayCommentToUser("false"); // False / True
                    itemRequest.setMerchantid(Constants.MERCHANT_ID);
                    itemRequest.setOperatorID(agentId);
                    itemRequest.setOrderId(String.valueOf(orderLineIdRemovedList.get(k).OrderId));

                    DeleteOrderLineItemRequest.TobeCancelledOrderItem item = new DeleteOrderLineItemRequest.TobeCancelledOrderItem();
                    item.setCancelQuantity(orderLineIdRemovedList.get(k).Quantity);
                    item.setOrderItemID(orderLineIdRemovedList.get(k).OrderLineId);
                    List<DeleteOrderLineItemRequest.TobeCancelledOrderItem> items = new ArrayList<>();
                    items.add(item);
                    itemRequest.setTobeCancelledOrderItems(items);

                    Map<String, Object> inputs = new HashMap<>();
                    inputs.put("MerchantId", Constants.MERCHANT_ID);
                    inputs.put("InputFormat", "application/json");
                    inputs.put("InputData", new Gson().toJson(itemRequest)); // TODO: DON'T APPEND MERCHANT ID IN URL
                    Logs.e("***", new Gson().toJson(itemRequest));
                    DeveloperApiHelper DELETE_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/CancelItem").setMethod(APIMethod.POST).build();
                    Logs.e("***", DELETE_ITEM_URL.getFinalUrl());
                    int finalK = k;
                    Disposable delete_item_disposable = orderService.deleteOrderLineItem(DELETE_ITEM_URL.getFinalUrl(), inputs)
                            .subscribeOn(supervisorApplication.subscribeScheduler())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Consumer<DeveloperAPIResponse>() {
                                @Override
                                public void accept(DeveloperAPIResponse response) throws Exception {
                                    Logs.e("***", new Gson().toJson(response));
                                    if ((response != null && response.messageCode != null && (response.messageCode.equalsIgnoreCase("1004") || response.messageCode.equalsIgnoreCase("1002")))) {
                                        //SupervisorAppMessages.showToast("Success", context);
                                        Log.d("Sucess", "Success");
                                        if (finalK == orderLineIdRemovedList.size() - 1) {
                                            validateRemoveandQtyChange(orderLineIdList);
                                        }
                                    } else {
                                        //SupervisorAppMessages.showToast("Failed", context);
                                        Log.d("Failed", "Failed");
                                        if (baseLoadingF != null)
                                            baseLoadingF.dismiss();

                                        String body = "Failed";
                                        if (response != null && response.Message != null && response.Message.trim().length() > 0) {
                                            body = response.Message;
                                        }
                                        BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                                        loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialog) {

                                            }
                                        });
                                        loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                    }

                                }
                            }, new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) throws Exception {
                                    if (finalK == orderLineIdRemovedList.size() - 1) {
                                        validateRemoveandQtyChange(orderLineIdList);
                                    }
                                }
                            });
                    compositeDisposable.add(delete_item_disposable);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            validateRemoveandQtyChange(orderLineIdList);
        }
    }

    private void validateRemoveandQtyChange(List<OrderLineId> orderLineIdList) {
        Handler handler1 = new Handler();
        for (int k = 0; k < orderLineIdList.size(); k++) {
            if (orderLineIdList.get(k).IsAdded) {
                //call add line item with actual qty
                int finalK = k;
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doAddLineItem(finalK, true);
                    }
                }, 1000 * finalK);

            } else if (orderLineIdList.get(k).UpdatedQuantity > orderLineIdList.get(k).Quantity) {
                //call add order line item
                if (orderLineIdList.get(k).UpdatedQuantity != 0 && !orderLineIdList.get(k).IsAdded) {
                    int finalK = k;
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doAddLineItem(finalK, false);
                        }
                    }, 1000 * finalK);
                }
            } else if (orderLineIdList.get(k).UpdatedQuantity < orderLineIdList.get(k).Quantity) {
                // call cancel line item api with change qty
                if (orderLineIdList.get(k).UpdatedQuantity != 0 && !orderLineIdList.get(k).IsAdded) {
                    int finalK = k;
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doDeleteLineItem(finalK);
                        }
                    }, 1000 * finalK);

                }
            }
        }


        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                getOrderInfoAPI();
            }
        }, isAddItemAPICalling ? 6000 : (isDeleteItemAPICalling ? 6000 : 3000));

    }

    private void getOrderInfoAPI() {
        try {
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order").setServiceName(order.OrderId + "").setMethod(APIMethod.GET).build();
            Logs.e("***", "OrderInfoAPI URL " + orderAPIHelper.getFinalUrl());
            Disposable disposable = orderService.getOrderDetails(orderAPIHelper.getFinalUrl())
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<OrderResponse>() {
                        @Override
                        public void accept(OrderResponse orderResponse) throws Exception {
                            Logs.e("***", "OrderInfoAPI Response " + new Gson().toJson(orderResponse));
                            if (orderResponse != null && orderResponse.messageCode.equalsIgnoreCase("1004") && orderResponse.getOrderList() != null && orderResponse.getOrderList().size() > 0) {
                                orderInfoAPIRespAfterAddRemoveItem = orderResponse.getOrderList();
                                if (orderLineIdPriceList != null && orderLineIdPriceList.size() > 0) {
                                    listSizeEditOrderLineItem = orderLineIdPriceList.size();
                                    for (OrderLineId lineId : orderLineIdPriceList) {
                                        if (lineId.UpdatedPrice != lineId.ProductPrice) {
                                            if (lineId.OrderLineId == 0) {
                                                for (OrderLineId lineI : orderInfoAPIRespAfterAddRemoveItem.get(0).OrderLineId) {
                                                    if (lineId.SKU.equalsIgnoreCase(lineI.SKU)) {
                                                        lineId.OrderLineId = lineI.OrderLineId;
                                                        Logs.e("***", "Before PriceOverride API - if orderLine Id is 0, updating from OrderInfo Response");
                                                    }
                                                }
                                            }
                                            if (lineId.OrderLineId != 0) {
                                                editOrderLineAsPriceOverRideAPI(order.OrderId, "false", orderLineIdList, true, lineId);
                                            }
                                        } else {
                                            Logs.e("***", "Updated price & MRP are same so skipping this EditOrderLine API - OrderId: " + lineId.OrderId + " orderLineId: " + lineId.OrderLineId);
                                            updateProcessTransactionAPI(order);
                                        }
                                    }
                                } else {
                                    Logs.e("***", "OrderInfoAPI success - No price Override so direct Update transaction");
                                    updateProcessTransactionAPI(order);
                                }
                            } else {
                                Logs.e("***", "OrderInfoAPI - Orders may null");
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();

                                String body = "Failed";
                                if (orderResponse != null && orderResponse.Message != null && orderResponse.Message.trim().length() > 0) {
                                    body = orderResponse.Message;
                                }
                                BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                                loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logs.e("***", "OrderInfoAPI " + throwable.getMessage());
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(disposable);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isChangeLoss) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage("Your Changes Will Be Lost?");
                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.create().show();
                    return true;
                } else {
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public static Intent launchDetail(Context context, Orders order, String activityName) {
        Intent intent = new Intent(context, DetailOrderActivity.class);
        intent.putExtra(EXTRA_DETAIL_ACTIVITY_NAME, activityName);
        intent.putExtra(EXTRA_ORDER, new Gson().toJson(order, Orders.class).toString());
        return intent;
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void proceedToSearchActivity() {
        // ((Activity) context).finish();
        Log.d("order", "" + new Gson().toJson(order));
        context.startActivity(SearchActivity.launchSearch(context, new Gson().toJson(order)));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        comment.setText(String.valueOf(reasons[position]));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void fetchOrderDetailList() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        String orderID = orderid.getText().toString();
        if (!TextUtils.isEmpty(orderID)) {
            Logger.logInfo("fetchOrderDetail", "--- ORDER ID " + orderID, "");
            DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order").setServiceName(orderID).setMethod(APIMethod.GET).build();
            Logs.e("***", orderAPIHelper.getFinalUrl());
            Disposable disposable = orderService.getOrderDetails(orderAPIHelper.getFinalUrl())
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<OrderResponse>() {
                        @Override
                        public void accept(OrderResponse orderResponse) throws Exception {
                            Logs.e("***", new Gson().toJson(orderResponse));
                            if (orderResponse != null && orderResponse.getOrderList() != null && orderResponse.getOrderList().size() > 0) {
                                Logs.e("***", "GIFT MSG - " + orderResponse.getOrderList().get(0).GiftMessage != null ? orderResponse.getOrderList().get(0).GiftMessage : "GIFT MESSAGE IS NULL");
                                memberComment.setText(orderResponse.getOrderList().get(0).GiftMessage);
                                Log.d("response", "" + new Gson().toJson(orderResponse));

                                OrderService orderService = supervisorApplication.getOrderService();
                                DeveloperApiHelper GET_LEDGER_BALANCE_URL = new DeveloperApiHelper.Builder().endPoint("Order/BalanceEnquiry").setMethod(APIMethod.POST).build();
                                Logs.e("***", "Get Ledger URL - " + GET_LEDGER_BALANCE_URL.getFinalUrl());
                                Logs.e("***", "Get Ledger Inputs - provider=LEDGER, UserID=" + order.UserId);
                                Disposable disposable = orderService.getLedgerBalance(GET_LEDGER_BALANCE_URL.getFinalUrl(), Constants.MERCHANT_ID, "LEDGER", order.UserId, "")
                                        .subscribeOn(supervisorApplication.subscribeScheduler())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Consumer<LedgerBalanceResponse>() {
                                            @Override
                                            public void accept(LedgerBalanceResponse ledgerBalanceResponse) throws Exception {
                                                Logs.e("***", "Get Ledger Response - " + new Gson().toJson(ledgerBalanceResponse));
                                                if (ledgerBalanceResponse != null && ledgerBalanceResponse.messageCode.equalsIgnoreCase("1004")) {
                                                    float balance = Float.parseFloat(ledgerBalanceResponse.BalanceEnquiryResponseView.Balance);
                                                    order.ledgerBalanceResponse = ledgerBalanceResponse;
                                                    ledgerBalanceSet();
                                                }
                                                changeOrderDataSet(orderResponse.getOrderList().get(0).OrderLineId);
                                            }
                                        }, new Consumer<Throwable>() {
                                            @Override
                                            public void accept(Throwable throwable) throws Exception {
                                                changeOrderDataSet(orderResponse.getOrderList().get(0).OrderLineId);
                                            }
                                        });
                                compositeDisposable.add(disposable);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("fetchOrderDetorderResponseail", "--- throwable " + throwable.getMessage(), "");
//                            if (baseLoadingFragment != null)
//                                baseLoadingFragment.dismiss();
                        }
                    });
            compositeDisposable.add(disposable);
        } else {
            Toast.makeText(context, "OrderId is not available", Toast.LENGTH_LONG).show();
        }
    }

    private void changeOrderDataSet(List<OrderLineId> orderLineIdList) {
        this.orderLineIdList.clear();
        this.orderLineIdList.addAll(orderLineIdList);
        order.OrderLineId = orderLineIdList;
//        if (checkForFirstTime(order.OrderId)) {
        getProductsInformation(orderLineIdList);
//        } else {
//            List<Integer> appOrderIds = SupervisorApplication.getAppOrderIds();
//            if (appOrderIds == null) {
//                appOrderIds = new ArrayList<>();
//            }
//            appOrderIds.add(order.OrderId);
//            SupervisorApplication.setAppOrderIds(appOrderIds);
//            Log.d("checkftime", "yes");
//            editOrderLineAsPriceOverRideAPI(order.OrderId, "true", orderLineIdList, false);
//        }
        getGrandTotal();
    }

    private boolean checkForFirstTime(int orderId) {
        List<Integer> appOrderIds = SupervisorApplication.getAppOrderIds();
        return appOrderIds != null && appOrderIds.contains(orderId);
    }

    private void getProductsInformation(List<OrderLineId> orderLineIdList) {
        try {
            for (OrderLineId orderLine : orderLineIdList) {
                int productId = orderLine.ProductId;
                int storeId = orderLine.LocationId;
                SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
                OrderService orderService = supervisorApplication.getOrderService();

                FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("Products").build();
                Logs.e("***", "PDP URL - " + aFEAPIHelper.getFinalUrl() + "?productid=" + productId + "&storeid=" + (storeId != 0 ? (storeId + "") : QueryConstants.STORE_ID) + "&hsid=0&outputformat=graph");
                Disposable disposable = orderService.loadProduct(aFEAPIHelper.getFinalUrl(), productId, storeId != 0 ? (storeId + "") : QueryConstants.STORE_ID, "0", "graph", true)
                        .subscribeOn(supervisorApplication.subscribeScheduler())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ProductResponse>() {
                            @Override
                            public void accept(ProductResponse productResponse) throws Exception {
                                Logs.e("***", "PDP Response " + new Gson().toJson(productResponse));
                                if (productResponse != null && productResponse.MessageCode == 200 && productResponse.Products != null && productResponse.Products.size() > 0) {
                                    orderLine.product = productResponse.Products.get(0);
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Logger.logInfo("orderdetailactivity", "frontapi ", "throwable " + throwable.getMessage());
                                throwable.printStackTrace();
                                if (baseLoadingFragment != null)
                                    baseLoadingFragment.dismiss();
                            }
                        });

                compositeDisposable.add(disposable);

                if (locationCode == null) {
                    if (orderLine.LocationCode != null && orderLine.LocationCode.trim().length() > 0) {
                        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString(Constants.LOCATION_CODE, orderLine.LocationCode);
                        editor.apply();
                        editor.putInt(Constants.LOCATION_ID, orderLine.LocationId);
                        editor.apply();
                        locationCode = orderLine.LocationCode;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                List<OrderLineId> orderLineIds = removeCancelQuantity(orderLineIdList);
                orderLineIdList.clear();
                recyclerView.setVisibility(View.VISIBLE);
                orderLineIdList.addAll(orderLineIds);
                detailOrderItemsAdapter.setOrders(orderLineIdList);
                recyclerView.setAdapter(detailOrderItemsAdapter);
                detailOrderItemsAdapter.notifyDataSetChanged();
                if (baseLoadingFragment != null) {
                    baseLoadingFragment.dismiss();
                }
            }
        }, 900);
        getGrandTotal();
//        if (baseLoadingFragment != null) {
//            baseLoadingFragment.dismiss();
//        }
    }

    private List<OrderLineId> removeCancelQuantity(List<OrderLineId> orderLineIdList) {
        List<OrderLineId> removeCancelQty = new ArrayList<>();
        for (OrderLineId lineId : orderLineIdList) {
            if (lineId.CancelQuantity < lineId.Quantity) {
                removeCancelQty.add(lineId);
            }
        }
        return removeCancelQty;
    }

    private void changeProductDataSet(Product product) {
        this.productList.add(product);
        Gson gson = new Gson();
        Log.d("fapidata", gson.toJson(product));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    private void getGrandTotal() {
        float gt = 0;
        int totalCount = 0;
        float totalPromotion = 0f;
        for (OrderLineId orderLineId : orderLineIdList) {
            if (orderLineId.Quantity > orderLineId.CancelQuantity) {
                totalCount++;
                totalPromotion = totalPromotion + orderLineId.TotalPromotionDiscount;
            }
        }
        order_promotion.setText(String.format("%.2f", totalPromotion));
        recalculated_promotion.setText(String.format("%.2f", totalPromotion));
        noofItem.setText(String.valueOf(totalCount));
        recalculateItem.setText(String.valueOf(totalCount));
        grandTotal.setText(String.format("%.2f", order.AmountPayable));
        recalculategrandTotal.setText(String.format("%.2f", order.AmountPayable));
        if (orderLineIdList.get(0).ShippingCost != 0 && orderLineIdList.get(0).ShippingCost > 0) {
            shippingCost = orderLineIdList.get(0).ShippingCost;
        } else {
            for (OrderLineId lineId : orderLineIdList) {
                if (lineId.ShippingCost != 0 && lineId.ShippingCost > 0) {
                    shippingCost = lineId.ShippingCost;
                    break;
                }
            }
        }
        recalculated_charge.setText(String.format("%.2f", shippingCost));
        orderDeliveryCharge.setText(String.format("%.2f", shippingCost));


        if (orderLineIdList.size() > 0) {
            for (int i = 0; i <= orderLineIdList.size(); i++) {
                for (OrderLineId lineId : orderLineIdList) {
                    gt = gt + (lineId.Quantity * lineId.ProductPrice);
                    lineId.UpdatedQuantity = lineId.Quantity;
                    lineId.UpdatedPrice = lineId.ProductPrice;
                }
//                gt = gt + (orderLineIdList.get(i).Quantity * orderLineIdList.get(i).ProductPrice);
//                orderLineIdList.get(i).UpdatedQuantity = orderLineIdList.get(i).Quantity;
//                orderLineIdList.get(i).UpdatedPrice = orderLineIdList.get(i).ProductPrice;
//                // grandTotal.setText(String.format("%.2f", order.TotalAmount));
//                //recalculategrandTotal.setText(String.format("%.2f", gt));
            }
        }
    }

    private void doCancelOrder() {
        try {
            String orderID = orderid.getText().toString();
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            Map<String, Object> aParams = new HashMap<>();
            aParams.put("MerchantId", merchantID);
            aParams.put("OrderId", orderID);
            if (!TextUtils.isEmpty(orderID)) {

                DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/Cancel").setMethod(APIMethod.POST).build();
                Log.d("cancelurl", aDAPIHelper.getFinalUrl());
                Disposable disposable = orderService.cancelOrder(aDAPIHelper.getFinalUrl(), merchantID, orderID)
                        .subscribeOn(supervisorApplication.subscribeScheduler())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<CancelResponse>() {
                            @Override
                            public void accept(CancelResponse cancelResponse) throws Exception {
                                Logger.logInfo("doCancelOrder", "--- ENDED ordercancel ", "");
                                Logger.logInfo("doCancelOrder", "--- Order cancel Success MESSAGE CODE " + cancelResponse.messageCode, "");
                                Logger.logInfo("doCancelOrder", "--- Order cancel Success MESSAGE " + cancelResponse.Message, "");
                                Gson gson = new Gson();
                                Log.d("response", gson.toJson(cancelResponse));
                                //if (Integer.parseInt(cancelResponse.messageCode) == 1002) {
                                if (cancelResponse != null && cancelResponse.messageCode != null && cancelResponse.messageCode.equalsIgnoreCase("1002")) {
                                    SupervisorAppMessages.showToast("Order Cancelled.", context);
                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                    String location_id = pref.getString("Location_Id", null);
                                    Log.d("lid", location_id);
                                    startActivity(OrderListActivity.launchOrder(context));
                                    finish();
                                    if (baseLoadingFragment != null)
                                        baseLoadingFragment.dismiss();
                                } else {
                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Cancel Order", "Please try again later", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            // code
                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Logger.logInfo("doCancelOrder", "--- ENDED doCancelOrder ", "");
                                // messageLabel.set(context.getString(R.string.error_loading_order));
                                Logger.logInfo("doCancelOrder", "--- Error " + throwable.getMessage(), "");
                                throwable.printStackTrace();
                                if (baseLoadingFragment != null)
                                    baseLoadingFragment.dismiss();
                            }
                        });
                compositeDisposable.add(disposable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void editOrderLineAsPriceOverRideAPI(int orderId, String ConsiderCurrentPriceandMRP, List<OrderLineId> orderLineIdList, boolean isConfirmOrder, OrderLineId priceOverRideOrder) {
        try {
            isEditOrderLinePriceOverrideAPICalled = true;
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            String itemsInput = null;
            Logger.logDebug("***", "EditOrderLineItem/PriceOverride API", "");
//        List<EditOrderLineItemRequest> requests = new ArrayList<>();

//        for (int j = 0; j < orderLineIdList.size(); j++) {
            int countOfLoop = 0;
//            EditOrderLineItemRequest eoliRequest = new EditOrderLineItemRequest();
//            eoliRequest.ConsiderCurrentPriceandMRP = ConsiderCurrentPriceandMRP; // True while 1st time hitting - Dynamic on Confirm Order call
//            eoliRequest.Price = ""; // empty while 1st time hitting - Dynamic on Confirm Order call
//            eoliRequest.ReasonForPriceOverride = "test"; // empty while 1st time hitting - Dynamic on Confirm Order call
//            eoliRequest.MRP = ""; // empty while 1st time hitting - Dynamic on Confirm Order call
//            eoliRequest.OrderID = orderId + "";
//            eoliRequest.ItemAction = "true";
//            eoliRequest.OrderItemID = orderLineIdList.get(j).OrderLineId + ""; // empty while 1st time hitting - Dynamic on Confirm Order call

//            requests.add(eoliRequest);

//            String itemInput = "{\"ConsiderCurrentPriceandMRP\":" + ConsiderCurrentPriceandMRP + ",\"Price\":" + orderLineIdList.get(j).UpdatedPrice + ",\"ReasonForPriceOverride\":\"Update Price by Supervisor\",\"MRP\":" + orderLineIdList.get(j).UpdatedPrice + ",\"OrderID\":" + orderId + ",\"ItemAction\":true,\"OrderItemID\":" + orderLineIdList.get(j).OrderLineId + "}";

            String itemInput = "{\"ConsiderCurrentPriceandMRP\":" + ConsiderCurrentPriceandMRP + ",\"Price\":" + priceOverRideOrder.UpdatedPrice + ",\"ReasonForPriceOverride\":\"Update Price by Supervisor\",\"MRP\":" + (priceOverRideOrder.product.MRP != 0f ? priceOverRideOrder.product.MRP : priceOverRideOrder.ProductPrice) + ",\"OrderID\":" + orderId + ",\"ItemAction\":true,\"OrderItemID\":" + priceOverRideOrder.OrderLineId + "}";

            itemsInput = "[" + itemInput + "]";

//            if (j == 0) {
//                itemsInput = "[" + itemInput + "]";
//            } else {
//                itemsInput = itemsInput.replace("]", "," + itemInput + "]");
//            }
//
//            if (orderLineIdList.size() > 1 && j == orderLineIdList.size() - 1) {
//                if (itemsInput.contains(",]")) {
//                    itemsInput = itemsInput.replace(",]", "]");
//                }
//            }
            // TODO
//            if (itemsInput == null) {
//                itemsInput = new Gson().toJson(requests);
//            }
            Map<String, Object> aParams = new HashMap<>();
            aParams.put("MerchantId", Constants.MERCHANT_ID);
            aParams.put("InputFormat", "application/json");
            aParams.put("InputData", itemsInput);
            Log.d("inputdata", "" + itemsInput);

            DeveloperApiHelper editOrderLineItemURL = new DeveloperApiHelper.Builder().endPoint("Order/EditOrderLineItem").setMethod(APIMethod.POST).build();
            Log.d("inputdata", "" + editOrderLineItemURL.getFinalUrl());
            Logs.e("***", "EditOrderLineItem - URl " + editOrderLineItemURL.getFinalUrl());
            Logs.e("***", "EditOrderLineItem - Request " + itemsInput);
            Disposable disposable = orderService.editOrderLineItem(editOrderLineItemURL.getFinalUrl(), aParams)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<DeveloperAPIResponse>() {
                        @Override
                        public void accept(DeveloperAPIResponse devAPIResponse) throws Exception {
                            listLoopSizeEditOrderLineItem++;
                            Logs.e("***", "EditOrderLineItem - Response " + new Gson().toJson(devAPIResponse));
                            if (devAPIResponse != null && devAPIResponse.messageCode != null) {
                                Log.d("checkftime", "yes");
                                if (devAPIResponse.messageCode.equalsIgnoreCase("1004")) {
                                    if (listSizeEditOrderLineItem == listLoopSizeEditOrderLineItem) {
                                        Logs.e("***", "EditOrderLineItem - success loop is in progress - ListSize: " + listSizeEditOrderLineItem + " loopSize: " + listLoopSizeEditOrderLineItem);
                                        updateProcessTransactionAPI(order);
                                    } else {
                                        Logs.e("***", "EditOrderLineItem - success loop is in progress - ListSize: " + listSizeEditOrderLineItem + " loopSize: " + listLoopSizeEditOrderLineItem);
                                    }
                                } else {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();

                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", devAPIResponse.Message != null ? devAPIResponse.Message : "Please try again later", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {

                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            } else {
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            listLoopSizeEditOrderLineItem++;
                            Logger.logInfo("editOrderLineAsPriceOverRideAPI", "--- throwable " + throwable.getMessage(), "");
                            Log.d("checkftime", "yes");
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();
                        }
                    });
            compositeDisposable.add(disposable);
//        }


//        String input = "[{\"ConsiderCurrentPriceandMRP\":true,\"Price\":44.1,\"ReasonForPriceOverride\":\"Update for Price at Payment\",\"MRP\":\"62\",\"OrderID\":" + orderId + ",\"ItemAction\":true,\"OrderItemID\":" + orderLineIdList.get(0).OrderLineId + "}]";
//        if (isConfirmOrder) {
//            //updateOrderFieldsAPI("testBDA-1234-ZBDA|price");
////            boolean isOnline = false;
////            PaymentLoop:
////            for (int i = 0; i < order.PaymentDetails.size(); i++) {
////                PaymentDetails paymentDetails = order.PaymentDetails.get(i);
////                if (paymentDetails.PaymentType.equalsIgnoreCase("TPG")) {
////                    isOnline = true;
////                    break PaymentLoop;
////                }
////            }
//
//            updateProcessTransactionAPI(order);
//
////            if (isOnline) {
////                processTransactionAPI();
////            } else {
////                updateProcessTransactionAPI(order);
////                //authorizeOrderAPI();
////            }
//        } else {
//            fetchOrderDetailList(); // WONT EXECUTE THIS
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void authorizeOrderAPI() {
        try {
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            Logger.logDebug("***", "authorizeOrder API", "");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
            String date = simpleDateFormat.format(new Date().getTime());
            Log.e("CurrentDate", date);
            Map<String, Object> aParams = new HashMap<>();
            aParams.put("MerchantId", Constants.MERCHANT_ID);
            aParams.put("Date", date);
            aParams.put("OrderId", order.OrderId);
            Logs.e("***", "Authorize order Body " + new Gson().toJson(aParams));

            DeveloperApiHelper editOrderLineItemURL = new DeveloperApiHelper.Builder().endPoint("Order/Authorize").setMethod(APIMethod.POST).build();
            Logs.e("***", "Authorize order URL " + editOrderLineItemURL.getFinalUrl());
            Disposable disposable = orderService.authorizeOrder(editOrderLineItemURL.getFinalUrl(), aParams)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<DeveloperAPIResponse>() {
                        @Override
                        public void accept(DeveloperAPIResponse orderResponse) throws Exception {
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();
                            Logger.logInfo("AuthorizeOrder", "--- throwable ", "");
                            Logs.e("***", "Authorize order Response " + new Gson().toJson(orderResponse));
                            if (orderResponse != null && orderResponse.messageCode.equalsIgnoreCase("1004") || orderResponse.messageCode.equalsIgnoreCase("1005")) {
                                BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order authorized", "Success", false, true);
                                loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                        String location_id = pref.getString("Location_Id", null);
                                        startActivity(OrderListActivity.launchOrder(context));
                                        finish();
                                    }
                                });
                                loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);

                            } else {
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();

                                String body = "Failed";
                                if (orderResponse != null && orderResponse.Message != null && orderResponse.Message.trim().length() > 0) {
                                    body = orderResponse.Message;
                                }
                                BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                                loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("AuthorizeOrder", "--- throwable " + throwable.getMessage(), "");
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(disposable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doAddLineItem(int position, boolean isAdded) {
        try {
            isAddItemAPICalling = true;
            if (locationCode == null) {
                SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                locationCode = pref.getString(Constants.LOCATION_CODE, null);
            }
            if (orderLineIdList.size() > 0 && locationCode != null) {
                //String locationCode = orderLineIdList.get(position).orders.OrderLineId != null ? orders.OrderLineId.size() > 0 ? orders.OrderLineId.get(0).LocationCode != null ? orders.OrderLineId.get(0).LocationCode : "" : "" : "";
                AddOrderLineItemRequest request = new AddOrderLineItemRequest();
                request.setDeliveryMode("H"); // STATIC - WONT CHANGE
                request.setItemAction(1); // STATIC - WONT CHANGE
                request.setLocationCode(locationCode); // DYNAMIC - Get Location code from OrderInfo API
                request.setOrderId(String.valueOf(orderLineIdList.get(position).OrderId)); // DYNAMIC - Set Order ID from OrderInfo API
                //request.setQuantity(1); // STATIC - WONT CHANGE
                request.setQuantity(isAdded ? orderLineIdList.get(position).UpdatedQuantity : (orderLineIdList.get(position).UpdatedQuantity - orderLineIdList.get(position).Quantity));
                request.setSKU(orderLineIdList.get(position).product.Sku); // DYNAMIC - SEARCH RESULT SKU OF PRODUCT
                request.setVarientSKU(""); // DYNAMIC - SEARCH RESULT VARIENT SKU OF PRODUCT
                request.setProductid(orderLineIdList.get(position).ProductId);
                Log.d("orderid", String.valueOf(orderLineIdList.get(position).OrderId));

                SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
                OrderService orderService = supervisorApplication.getOrderService();
                Map<String, Object> aParams = new HashMap<>();
                aParams.put("MerchantId", Constants.MERCHANT_ID);
                aParams.put("InputFormat", "application/json");
                aParams.put("InputData", new Gson().toJson(request));

                DeveloperApiHelper ADD_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/AddOrderLineItem").setMethod(APIMethod.POST).build();
                String URL = ADD_ITEM_URL.getFinalUrl();
                Logs.e("***", "AddOrderLineItem URL - " + URL);
                Logs.e("***", "AddOrderLineItem Request - " + new Gson().toJson(request));
                Disposable add_item_disposable = orderService.addOrderLineItem(URL, aParams)
                        .subscribeOn(supervisorApplication.subscribeScheduler())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<DeveloperAPIResponse>() {
                            @Override
                            public void accept(DeveloperAPIResponse response) throws Exception {
                                Logs.e("***", "AddOrderLineItem Response - " + new Gson().toJson(response));
                                if (response != null && response.messageCode != null && response.messageCode.equalsIgnoreCase(Constants.SUCCESS_CODE)) {
                                    //SupervisorAppMessages.showToast(response.Message, context);
                                    Log.d("Success", "Success");
                                } else if (response.Message != null) {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();
                                    SupervisorAppMessages.showToast(response.Message, context);
                                } else {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();
                                    SupervisorAppMessages.showToast(Constants.Try_Again, context);
                                }
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();

                                String body = "Failed";
                                BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                                loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            }
                        });
                compositeDisposable.add(add_item_disposable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doDeleteLineItem(int pos) {
        try {
            isDeleteItemAPICalling = true;
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            DeleteOrderLineItemRequest itemRequest = new DeleteOrderLineItemRequest();
            itemRequest.setCancelReason("Recalculate Order");
            itemRequest.setComment("Recalculate Order");
            itemRequest.setDate(today_date); // TODAY DATE yyyy-MM-dd
            itemRequest.setDisplayCommentToUser("false"); // False / True
            itemRequest.setMerchantid(Constants.MERCHANT_ID);
            itemRequest.setOperatorID(agentId);
            itemRequest.setOrderId(String.valueOf(orderLineIdList.get(pos).OrderId));

            DeleteOrderLineItemRequest.TobeCancelledOrderItem item = new DeleteOrderLineItemRequest.TobeCancelledOrderItem();
            item.setCancelQuantity(orderLineIdList.get(pos).Quantity - orderLineIdList.get(pos).UpdatedQuantity);
            item.setOrderItemID(orderLineIdList.get(pos).OrderLineId);
            List<DeleteOrderLineItemRequest.TobeCancelledOrderItem> items = new ArrayList<>();
            items.add(item);
            itemRequest.setTobeCancelledOrderItems(items);

            Map<String, Object> inputs = new HashMap<>();
            inputs.put("MerchantId", Constants.MERCHANT_ID);
            inputs.put("InputFormat", "application/json");
            inputs.put("InputData", new Gson().toJson(itemRequest)); // TODO: DON'T APPEND MERCHANT ID IN URL

            DeveloperApiHelper DELETE_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/CancelItem").setMethod(APIMethod.POST).build();
            Logs.e("***", "CancelItem - URL " + DELETE_ITEM_URL.getFinalUrl());
            Logs.e("***", "CancelItem - Request " + new Gson().toJson(itemRequest));
            Disposable delete_item_disposable = orderService.deleteOrderLineItem(DELETE_ITEM_URL.getFinalUrl(), inputs)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<DeveloperAPIResponse>() {
                        @Override
                        public void accept(DeveloperAPIResponse response) throws Exception {
                            Logs.e("***", "CancelItem - Response " + new Gson().toJson(response));
                            if ((response != null && response.messageCode != null && (response.messageCode.equalsIgnoreCase("1004") || response.messageCode.equalsIgnoreCase("1002")))) {
                                //SupervisorAppMessages.showToast("Success", context);
                                Log.d("Sucess", "Success");
                            } else {
                                //SupervisorAppMessages.showToast("Failed", context);
                                Log.d("Failed", "Failed");
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();
                            }

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(delete_item_disposable);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void hideAdapterItem(int pos, OrderLineId orList) {
        // orderLineIdList.get(pos).IsRemoved = true;
        if (!orList.IsAdded) {
            orderLineIdRemovedList.add(orList);
            orderLineIdList.remove(orList);
            confirmOrder.setEnabled(false);
            recalculateOrder.setEnabled(true);
            isChangeLoss = true;
        } else {
            confirmOrder.setEnabled(false);
            recalculateOrder.setEnabled(true);
            isChangeLoss = true;
        }
    }

    @Override
    public void updateQtyChange(int qty, int pos) {
        orderLineIdList.get(pos).UpdatedQuantity = qty;
        confirmOrder.setEnabled(false);
        recalculateOrder.setEnabled(true);
        isChangeLoss = true;
    }

    @Override
    public void refreshProductLineItems(int pos, float updatePrice, float oldPrice, int orderId, int orderLineId, String SKU, Product product) {
//        orderLineIdPriceList.get(pos).UpdatedPrice = updatePrice;
//        orderLineIdPriceList.get(pos).ProductPrice = oldPrice;
//        orderLineIdPriceList.get(pos).OrderId = orderId;
        OrderLineId newOrder = new OrderLineId();
        newOrder.UpdatedPrice = updatePrice;
        newOrder.ProductPrice = oldPrice;
        newOrder.OrderId = orderId;
        newOrder.OrderLineId = orderLineId;
        newOrder.SKU = SKU;
        newOrder.MRP = product.MRP + "";
        newOrder.product = product;

        boolean lineIdIsAvailable = false;
        if (orderLineIdPriceList != null && orderLineIdPriceList.size() > 0) {
            for (OrderLineId lineId : orderLineIdPriceList) {
                if (lineId.OrderId == orderId && lineId.OrderLineId == orderLineId) {
                    Logs.e("***", "Line Item is avaibale so updating price in same item");
                    lineIdIsAvailable = true;
                    lineId.UpdatedPrice = updatePrice;
                    lineId.ProductPrice = oldPrice;
                }
            }
            if (!lineIdIsAvailable) {
                Logs.e("***", "Line Item is not avaibale so adding as new item");
                orderLineIdPriceList.add(newOrder);
            }
        } else {
            Logs.e("***", "Line Item list is empty so creating new list & adding as new item");
            orderLineIdPriceList = new ArrayList<>();
            orderLineIdPriceList.add(newOrder);
        }
//        if (orderLineIdPriceList.contains(newOrder)) {
//            orderLineIdPriceList.remove(newOrder);
//        }


        Logs.e("***", "Line Item list size: " + orderLineIdPriceList.size());
        confirmOrder.setEnabled(false);
        recalculateOrder.setEnabled(true);
        isChangeLoss = true;
    }


    private void updateOrderFieldsAPI(String giftMessage) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
        String supervisor_email = null;
        if (pref != null) {
            supervisor_email = pref.getString("Logged_Email", null);
        }
        SupervisorApplication supervisorApplication = SupervisorApplication.create(DetailOrderActivity.this);
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("MerchantID", Constants.MERCHANT_ID);
        aParams.put("OperatorID", agentId);
        aParams.put("GiftMessage", (giftMessage != null ? giftMessage : "") + "#" + (supervisor_email != null ? supervisor_email : ""));

        DeveloperApiHelper updateOrderFields = new DeveloperApiHelper.Builder().endPoint("Order/UpdateOrderFields").setMethod(APIMethod.POST).build();
        Log.d("URL", "" + updateOrderFields.getFinalUrl());
        Disposable updateOrderFieldsDisposable = orderService.editOrderFields(updateOrderFields.getFinalUrl(), aParams)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        Log.d("Response", "" + new Gson().toJson(orderResponse));
//                        if (title.equalsIgnoreCase(Constants.BDA_COMMENTS_ORDER)) {
//                            if (baseLoadingF != null)
//                                baseLoadingF.dismiss();
//
//                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order Updated", "Success", false, true);
//                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialog) {
//                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                    String location_id = pref.getString("Location_Id", null);
//                                    startActivity(OrderListActivity.launchOrder(context));
//                                    finish();
//                                }
//                            });
//                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
//                            if (baseLoadingF != null)
//                                baseLoadingF.dismiss();
//
//                        } else {
                        if (orderResponse != null && orderResponse.messageCode.equalsIgnoreCase("1004")) {
                            boolean isOnline = false;
                            PaymentLoop:
                            for (int i = 0; i < order.PaymentDetails.size(); i++) {
                                PaymentDetails paymentDetails = order.PaymentDetails.get(i);
                                if (paymentDetails.PaymentType.equalsIgnoreCase("TPG")) {
                                    isOnline = true;
                                    break PaymentLoop;
                                }
                            }

                            if (isOnline) {
                                processTransactionAPI(); // THIS WONT EXCEUTE EVER
                            } else {
                                updateProcessTransactionAPI(order);
                                //authorizeOrderAPI();
                            }
                        } else {
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();
                        }
//                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (baseLoadingF != null)
                            baseLoadingF.dismiss();

                        String body = "Failed";
                        BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                        loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {

                            }
                        });
                        loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                    }
                });
        compositeDisposable.add(updateOrderFieldsDisposable);
    }

    private void updateProcessTransactionAPI(Orders order) {
        try {
            Orders latestOrder = orderInfoAPIRespAfterAddRemoveItem.get(0);
            if (latestOrder == null) {
                latestOrder = order;
            }
            if (latestOrder.PaymentDetails != null && latestOrder.PaymentDetails.size() > 0) {
                orderPaymentListSize = latestOrder.PaymentDetails.size();
                Logs.e("***", "Total Payments Transactions Available for this order - " + orderPaymentListSize);
                for (PaymentDetails paymentDetails : latestOrder.PaymentDetails) {
                    cancelExistingOnTransactionAPI(paymentDetails, latestOrder);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelExistingOnTransactionAPI(PaymentDetails paymentDetails, Orders latestOrder) {
        try {
            SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
            String supervisor_email = null;
            if (pref != null) {
                supervisor_email = pref.getString("Logged_Email", null);
            }

            //String transactionID = order != null ? (order.PaymentDetails.get(0) != null ? (order.PaymentDetails.get(0).PaymentDetailsId != 0 ? (order.PaymentDetails.get(0).PaymentDetailsId + "") : "") : "") : "";
            String transactionID = paymentDetails.PaymentDetailsId + "";

            UpdateTransactionRequest request = new UpdateTransactionRequest();
            request.Transaction = new UpdateTransactionRequest.Transaction();
            request.Transaction.CurrencyCode = "";
            request.Transaction.DeliverOption = "";
            request.Transaction.GatewayOption = "";
            request.Transaction.RespCode = "";
            request.Transaction.PaymentType = "";
            if (isEditOrderLinePriceOverrideAPICalled) {
                request.Transaction.RespMsg = latestOrder != null ? ((latestOrder.GiftMessage != null ? latestOrder.GiftMessage : "") + "#" + (supervisor_email != null ? supervisor_email : "")) : "";
            }
            // request.Transaction.RespMsg = latestOrder != null ? ((latestOrder.GiftMessage != null ? latestOrder.GiftMessage : "") + "#" + (supervisor_email != null ? supervisor_email : "")) : "";
            request.Transaction.TxnID = transactionID + "";
            request.Transaction.OrderId = latestOrder.OrderId + "";
            if (Constants.ISLOTS) {
                request.Transaction.PaymentStatus = "A";
                request.Transaction.Amount = recalculategrandTotal.getText().toString();

            } else {
                request.Transaction.PaymentStatus = "C"; // HERE WE ARE CANCELING EXISTING TRANSACTION & CREATING NEW TRANSACTION
            }
            String CONTENT_TYPE_VALUE = "application/json";
            String PARAM_INPUT_FORMAT_KEY = "InputFormat";
            String PARAM_INPUT_DATA = "InputData";
            Map<String, Object> aParams = new HashMap<>();
            aParams.put(PARAM_INPUT_FORMAT_KEY, CONTENT_TYPE_VALUE);
            aParams.put("MerchantId", Constants.MERCHANT_ID);
            aParams.put(PARAM_INPUT_DATA, new Gson().toJson(request));
            Logs.e("***", "UpdateTransaction Input - " + new Gson().toJson(request));
            SupervisorApplication supervisorApplication = SupervisorApplication.create(DetailOrderActivity.this);
            OrderService orderService = supervisorApplication.getOrderService();
            DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/UpdateTransaction").setMethod(APIMethod.POST).build();
            Logs.e("***", "UpdateTransaction URL - " + aDAPIHelper.getFinalUrl());
            Disposable disposable = orderService.processTransaction(aDAPIHelper.getFinalUrl(), aParams)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<ProcessTransactionResponse>() {
                        @Override
                        public void accept(ProcessTransactionResponse orderResponse) throws Exception {
                            Logs.e("***", "UpdateTransaction Response - " + new Gson().toJson(orderResponse));
                            orderPaymentLoopSize++;
                            if (orderResponse != null && orderResponse.messageCode != null && (orderResponse.messageCode.equalsIgnoreCase("1004") || orderResponse.messageCode.equalsIgnoreCase("1007"))) {
                                if (orderPaymentListSize == orderPaymentLoopSize) {
                                    processTransactionAPI();
                                    Logs.e("***", "UpdateTransaction Success - orderPaymentListSize : orderPaymentLoopSize " + orderPaymentListSize + " : " + orderPaymentLoopSize);
                                } else {
                                    Logs.e("***", "UpdateTransaction Success - orderPaymentListSize : orderPaymentLoopSize " + orderPaymentListSize + " : " + orderPaymentLoopSize);
                                }
                            } else {
                                if (baseLoadingF != null)
                                    baseLoadingF.dismiss();

                                String body = "Failed";
                                if (orderResponse != null && orderResponse.Message != null && orderResponse.Message.trim().length() > 0) {
                                    body = orderResponse.Message;
                                }
                                BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                                loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                    }
                                });
                                loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            orderPaymentLoopSize++;
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(disposable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processTransactionAPI() {
        if (!Constants.ISLOTS) {
            SupervisorApplication supervisorApplication = SupervisorApplication.create(DetailOrderActivity.this);
            OrderService orderService = supervisorApplication.getOrderService();
            ProcessTransactionModel input = new ProcessTransactionModel();
            ProcessTransactionModel.Transaction transaction = new ProcessTransactionModel.Transaction();
            transaction.setGatewayID("");
            transaction.setID("");
            transaction.setMerchantID(Constants.MERCHANT_ID);
            transaction.setOrderID(order.OrderId);
            transaction.setPaymentType("Loyalty");
            transaction.setProvider("LEDGER");
            List<ProcessTransactionModel.PaymentDetail> paymentDetails = new ArrayList<>();
            ProcessTransactionModel.PaymentDetail paymentDetail = new ProcessTransactionModel.PaymentDetail();
            paymentDetail.setBankCode("");
            paymentDetail.setBankEMICharges("");
            paymentDetail.setBankName("");
            paymentDetail.setChequeRefNo("");
            paymentDetail.setDrawnOn("");
            paymentDetail.setGVCode("");
            paymentDetail.setIFSC("");
            paymentDetail.setPaidAmount(recalculategrandTotal.getText().toString());
            paymentDetail.setPGReferenceId("");
            paymentDetail.setPointsBurned("");
            paymentDetail.setRespCode("");
            paymentDetail.setRespMessage("");
            paymentDetails.add(paymentDetail);
            transaction.setPaymentDetails(paymentDetails);
            input.setTransaction(transaction);

            String CONTENT_TYPE_VALUE = "application/json";
            String PARAM_INPUT_FORMAT_KEY = "InputFormat";
            String PARAM_INPUT_DATA = "InputData";
            Map<String, Object> aParams = new HashMap<>();
            aParams.put(PARAM_INPUT_FORMAT_KEY, CONTENT_TYPE_VALUE);
            aParams.put(PARAM_INPUT_DATA, new Gson().toJson(input));

            Logs.e("***", "ProcessTransaction input - " + new Gson().toJson(input));

            DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/ProcessTransaction").setMethod(APIMethod.POST).build();
            Logs.e("***", "ProcessTransaction url - " + aDAPIHelper.getFinalUrl());
            Disposable disposable = orderService.processTransaction(aDAPIHelper.getFinalUrl(), aParams)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<ProcessTransactionResponse>() {
                        @Override
                        public void accept(ProcessTransactionResponse orderResponse) throws Exception {
                            Logs.e("***", "ProcessTransaction response - " + new Gson().toJson(orderResponse));
                            if (orderResponse != null && orderResponse.messageCode != null) {
                                if (orderResponse.messageCode.equalsIgnoreCase("1004")) {
                                    getOrderStatusAPI();
                                } else {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();

                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", "Failed", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
//                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                                        String location_id = pref.getString("Location_Id", null);
//                                        startActivity(OrderListActivity.launchOrder(context));
//                                        finish();
                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(disposable);
        } else {

            getOrderStatusAPI();
        }
    }

    private void getOrderStatusAPI() {
        try {
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/GetOrderStatus").setServiceName(order.OrderId + "").setMethod(APIMethod.GET).build();
            Logs.e("***", "OrderStatusAPI URL " + orderAPIHelper.getFinalUrl());
            Disposable disposable = orderService.getOrderStatus(orderAPIHelper.getFinalUrl())
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<OrderStatusResponse>() {
                        @Override
                        public void accept(OrderStatusResponse orderResponse) throws Exception {
                            Logs.e("***", "OrderStatusAPI Response " + new Gson().toJson(orderResponse));
                            if (orderResponse != null && orderResponse.messageCode.equalsIgnoreCase("1004")) {
                                if (orderResponse.OrderStatus.status != null && orderResponse.OrderStatus.status.equalsIgnoreCase("P")) {
                                    authorizeOrderAPI();
                                } else if (orderResponse.OrderStatus.status != null && orderResponse.OrderStatus.status.equalsIgnoreCase("A")) {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();
                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order authorized", "Success", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                            String location_id = pref.getString("Location_Id", null);
                                            startActivity(OrderListActivity.launchOrder(context));
                                            finish();
                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                } else {
                                    if (baseLoadingF != null)
                                        baseLoadingF.dismiss();
                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order will be authorized in background.", "Processed", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {

                                        }
                                    });
                                    loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logs.e("***", "OrderInfoAPI " + throwable.getMessage());
                            if (baseLoadingF != null)
                                baseLoadingF.dismiss();

                            String body = "Failed";
                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Order is not authorized, Please try again later", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                }
                            });
                            loadingFragment.show(getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                    });
            compositeDisposable.add(disposable);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
