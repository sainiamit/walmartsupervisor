package com.walmart.supervisor.view;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.walmart.supervisor.R;
import com.walmart.supervisor.model.Location;
import com.walmart.supervisor.view.fragments.OrdersFragment;

public class OrderSectionsAdapter extends FragmentPagerAdapter {
    public static final int ORDERS = 0;
    public static final int PICKED_ORDERS = 1;

    private static final int[] TABS = new int[]{ORDERS, PICKED_ORDERS};

    private Context mContext;
    String mLocationId;

    public OrderSectionsAdapter(final Context context, final FragmentManager fragmentManager, String LocationId) {
        super(fragmentManager);
        mContext = context.getApplicationContext();
        this.mLocationId = LocationId;
    }

    @Override
    public Fragment getItem(int position) {
        switch (TABS[position]) {
            case ORDERS:
                return OrdersFragment.getInstance(ORDERS, mLocationId);
            case PICKED_ORDERS:
                return OrdersFragment.getInstance(PICKED_ORDERS, mLocationId);
        }
        return null;
    }

    @Override
    public int getCount() {
        return TABS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (TABS[position]) {
            case ORDERS:
                return mContext.getResources().getString(R.string.orders_tab1_name);
            case PICKED_ORDERS:
                return mContext.getResources().getString(R.string.orders_tab2_name);
        }
        return null;
    }
}