package com.walmart.supervisor.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.utils.Logger;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


public class SplashScreenActivity extends AppCompatActivity {

    private static final int SPLASH_TIMEOUT = 4000;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        launchLoginActivity();

        //getOrderDetail();
    }

    private void getOrderDetail() {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(this);
        OrderService orderService = supervisorApplication.getOrderService();
        DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order").setServiceName("1767238").setMethod(APIMethod.GET).build();
        Disposable disposable = orderService.getOrderDetails(orderAPIHelper.getFinalUrl())
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        if (orderResponse.getOrderList().size() > 0) {
                            Log.d("response", "" + new Gson().toJson(orderResponse));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("fetchOrderDetorderResponseail", "--- throwable " + throwable.getMessage(), "");

                    }
                });
        compositeDisposable.add(disposable);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT < 16) {
            // Hide the status bar
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            // Hide the action bar
            getSupportActionBar().hide();
        } else {
            // Hide the status bar
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
            // Hide the action bar
            getSupportActionBar().hide();
        }
    }

    void launchLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intentToLogin = LoginActivity.launchLogin(SplashScreenActivity.this);
                startActivity(intentToLogin);
//                Intent intent = new Intent(SplashScreenActivity.this,OrderListActivity.class);
//                startActivity(intent);

                overridePendingTransition(R.anim.fadein,R.anim.fadeout);
                finish();
            }
        }, SPLASH_TIMEOUT);
    }

}