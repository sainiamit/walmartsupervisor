package com.walmart.supervisor.view.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.dialogs.ReasonDialog;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.httpclient.FrontEndApiHelper;
import com.walmart.supervisor.model.AddOrderLineItemRequest;
import com.walmart.supervisor.model.DeleteOrderLineItemRequest;
import com.walmart.supervisor.model.DeveloperAPIResponse;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.slabprice.VolumetricLists;
import com.walmart.supervisor.model.slabprice.VolumetricPricing;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.Logs;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.DetailOrderActivity;
import com.walmart.supervisor.view.SearchActivity;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by M1045813 on 7/12/2018.
 */

public class DetailOrderItemsAdapter extends RecyclerView.Adapter<DetailOrderItemsAdapter.DetailOrderItemsAdapterViewHolder> {

    private final FragmentActivity fragment;
    //    private final Activity activity;
    private List<OrderLineId> orderLineIdAdapterList = new ArrayList<>();
    private Activity context;
    int clickcount = 0;
    private Orders order;
    String today_date;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private String agentId;
    private int totalQty;
    private AdapterCallback adapterCallbackCallback;

    public void setOrders(List<OrderLineId> orderLineIdList) {
        this.orderLineIdAdapterList = orderLineIdList;
    }

    public class DetailOrderItemsAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView productMrp, productsku, productTitle, webPrice, orderPrice, slabPrice, inStock, promotionAmount;
        EditText newPrice, quantity;
        LinearLayout slablinaerLayout, volumetric_table_parent, volumetric_table, extraDiscountLinearLayout;
        ImageView qtyPlus, qtyMinus, productImg, imgDelete, slabIcon;
        //slab textview
        TextView txt1, txt2, txt3;
        View view1;
        private SimpleDateFormat dateFormatter;
        RelativeLayout slabIconParent;

        public DetailOrderItemsAdapterViewHolder(View view) {
            super(view);
            productMrp = (TextView) view.findViewById(R.id.mrpLabel);
            productsku = (TextView) view.findViewById(R.id.productSKU);
            productTitle = (TextView) view.findViewById(R.id.productTitle);
            webPrice = (TextView) view.findViewById(R.id.webPriceLabel);
            orderPrice = (TextView) view.findViewById(R.id.ordered_price);
            quantity = (EditText) view.findViewById(R.id.quantity);
            slabPrice = (TextView) view.findViewById(R.id.moreOptions);
            slablinaerLayout = (LinearLayout) view.findViewById(R.id.moreOptionsLayout);
            volumetric_table_parent = (LinearLayout) view.findViewById(R.id.volumetric_table_parent);
            volumetric_table = (LinearLayout) view.findViewById(R.id.volumetric_table);
            newPrice = (EditText) view.findViewById(R.id.newPrice);
            qtyPlus = (ImageView) view.findViewById(R.id.imagePlus);
            qtyMinus = (ImageView) view.findViewById(R.id.imageMinus);
            productImg = (ImageView) view.findViewById(R.id.productImage);
            imgDelete = (ImageView) view.findViewById(R.id.productDelete);
            txt1 = (TextView) view.findViewById(R.id.txt1);
            txt2 = (TextView) view.findViewById(R.id.txt2);
            txt3 = (TextView) view.findViewById(R.id.txt3);
            view1 = (View) view.findViewById(R.id.View1);
            inStock = (TextView) view.findViewById(R.id.in_stock);
            promotionAmount = (TextView) view.findViewById(R.id.promotionAmount);
            extraDiscountLinearLayout = (LinearLayout) view.findViewById(R.id.extra_discount_linearlayout);
            dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            today_date = dateFormatter.format(System.currentTimeMillis());
            slabIconParent = (RelativeLayout) view.findViewById(R.id.slabIconParent);
            slabIcon = (ImageView) view.findViewById(R.id.slabIcon);
            SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
            if (pref != null) {
                agentId = pref.getString("Agent_Id", null);
            }
        }
    }

    public DetailOrderItemsAdapter(FragmentActivity context, List<OrderLineId> orderLineIds, Orders orders, AdapterCallback adapterCallback) {
        this.orderLineIdAdapterList = orderLineIds;
        this.context = context;
        this.fragment = context;
//        this.activity = (Activity) context;
        this.order = orders;
        this.adapterCallbackCallback = adapterCallback;
//        if (orderLineIds.size() > 0)
//            this.orderLineIdAdapterList = DetailOrderActivity.removeDeletedItems(orderLineIdAdapterList);
    }

    @Override
    public DetailOrderItemsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details_order, parent, false);

        return new DetailOrderItemsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailOrderItemsAdapter.DetailOrderItemsAdapterViewHolder holder, int position) {
        OrderLineId orderLineId = orderLineIdAdapterList.get(position);
        //((DetailOrderActivity)context).fe
        holder.setIsRecyclable(false);
        if (orderLineId != null) {
            if (orderLineIdAdapterList.get(position).Quantity == orderLineIdAdapterList.get(position).CancelQuantity) {
                holder.qtyPlus.setEnabled(false);
                holder.qtyMinus.setEnabled(false);
            }
            holder.productTitle.setText(orderLineId.ProductTitle);
            holder.productsku.setText("Item Number: " + orderLineId.SKU);
            Log.d("datacoming", "yes first ");
            if (orderLineId.product != null && orderLineId.product.MRP != 0) {
                holder.productMrp.setText("MRP: " + String.format("%.2f", orderLineId.product.MRP));
                holder.webPrice.setText("Current Price: " + String.format("%.2f", orderLineId.product.WebPrice));
                holder.inStock.setText(String.valueOf(orderLineId.product.Inventory) + " in stock");
            } else {
                holder.productMrp.setText("MRP: " + orderLineId.ProductPrice);
                holder.webPrice.setText("Current Price: " + orderLineId.ProductPrice);
                holder.inStock.setText(String.valueOf("100") + " in stock");
            }


            if (orderLineId.product != null && orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 1) {
                holder.extraDiscountLinearLayout.setVisibility(View.VISIBLE);
//                holder.slabIconParent.setVisibility(View.VISIBLE);
            } else {
                holder.extraDiscountLinearLayout.setVisibility(View.GONE);
//                holder.slabIconParent.setVisibility(View.GONE);
            }

            holder.slabIconParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getVolumetricPricing(orderLineId, holder);
                }
            });

            holder.slabIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getVolumetricPricing(orderLineId, holder);
//                    BaseLoadingFragment.newInstance("Alert", "TESTING", false, true)
//                            .show(fragment.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                }
            });

            holder.quantity.setText(String.valueOf(orderLineId.UpdatedQuantity));
            holder.orderPrice.setText("Ordered Price: " + String.format("%.2f", orderLineId.ProductPrice));
            holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
            holder.promotionAmount.setText("Promotion: " + String.format("%.2f", orderLineId.TotalPromotionDiscount));
            holder.slablinaerLayout.setVisibility(View.GONE);
            Glide.with(context).load(orderLineId.ImageUrl).placeholder(R.mipmap.ic_no_image).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImg);
            holder.slabPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickcount = clickcount + 1;
                    if (clickcount == 1) {
                        holder.slablinaerLayout.setVisibility(View.VISIBLE);
                        getVolumetricPricing(orderLineId, holder);
                    } else {
                        holder.slablinaerLayout.setVisibility(View.GONE);
                        clickcount = 0;
                    }

                }
            });
            holder.qtyMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int q = Integer.parseInt(holder.quantity.getText().toString());
                    if (q != 1) {
                        q--;
                        holder.quantity.setText(String.valueOf(q));
                        totalQty = 1;

                        //new added code for slab price
                        if (orderLineId.product != null && orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 1) {
                            for (int i = 0; i < orderLineId.product.VolumetricPricing.size(); i++) {
                                if (i + 1 == orderLineId.product.VolumetricPricing.size()) {
                                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                                    break;
                                } else if ((orderLineId.UpdatedQuantity >= orderLineId.product.VolumetricPricing.get(i).Quantity) && (orderLineId.UpdatedQuantity < ((orderLineId.product.VolumetricPricing.get(i + 1).Quantity)))) {
                                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                                    break;
                                }
                            }
                        } else {
                            Logs.e("***", "Volumetric Pricing is null");
                        }

                        if (adapterCallbackCallback != null) {
                            adapterCallbackCallback.updateQtyChange(q, position);
                            if (orderLineId.ProductPrice != orderLineId.UpdatedPrice)
                                adapterCallbackCallback.refreshProductLineItems(position, orderLineId.UpdatedPrice, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                            //doDeleteLineItem(position);
                        }


                    }
                }
            });
            holder.qtyPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int q = Integer.parseInt(holder.quantity.getText().toString());
                    int nq = orderLineId.Quantity;
                    if (q <= (orderLineId.product.Inventory)) {
                        q++;
                        holder.quantity.setText(String.valueOf(q));
                        //doAddLineItem(position);---new added code for slab price
                        if (orderLineId.product != null && orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 1) {
                            for (int i = 0; i < orderLineId.product.VolumetricPricing.size(); i++) {
                                if (i + 1 == orderLineId.product.VolumetricPricing.size()) {
                                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                                    break;
                                } else if ((orderLineId.UpdatedQuantity >= orderLineId.product.VolumetricPricing.get(i).Quantity) && (orderLineId.UpdatedQuantity < ((orderLineId.product.VolumetricPricing.get(i + 1).Quantity)))) {
                                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                                    break;
                                }
                            }
                        } else {
                            Logs.e("***", "Volumetric Pricing is null");
                        }
                        if (adapterCallbackCallback != null) {
                            adapterCallbackCallback.updateQtyChange(q, position);

                            if (orderLineId.ProductPrice != orderLineId.UpdatedPrice)
                                adapterCallbackCallback.refreshProductLineItems(position, orderLineId.UpdatedPrice, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                        }
                    }
                }
            });
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Alert", "Are you sure you want to Delete?", false, true);
//                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            orderLineIdAdapterList.remove(position);
//                            notifyDataSetChanged();
//                            if (adapterCallbackCallback != null)
//                                adapterCallbackCallback.hideAdapterItem(position, orderLineId);
//                        }
//                    });
//                    loadingFragment.show(activity.getFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);

                    AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                    dialog.setMessage("Are you sure you want to Delete?");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //doDeleteLineItem(position);
//                            orderLineIdList.get(position).IsRemoved = true;
                            orderLineIdAdapterList.remove(position);
                            notifyDataSetChanged();
//                            orderLineIdRemovedList.add(orderLineId);
                            if (adapterCallbackCallback != null)
                                adapterCallbackCallback.hideAdapterItem(position, orderLineId);
                            dialog.dismiss();
                        }
                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });

            if (orderLineId.product != null && orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 1) {
                holder.newPrice.setEnabled(false);
            } else {
                holder.newPrice.setEnabled(true);
            }

            holder.newPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        String onChangePrice = holder.newPrice.getText().toString().trim();
                        if (onChangePrice.trim().isEmpty()) {
                            Logs.e("***", "empty");
                            if (adapterCallbackCallback != null)
                                adapterCallbackCallback.refreshProductLineItems(position, 0, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                        } else {
                            Logs.e("***", "not empty-" + onChangePrice);
                            orderLineId.UpdatedPrice = Float.parseFloat(onChangePrice);
                            if (adapterCallbackCallback != null)
                                adapterCallbackCallback.refreshProductLineItems(position, orderLineId.UpdatedPrice, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                        holder.newPrice.setText(String.format("%.2f", orderLineId.ProductPrice));
//                        orderLineId.UpdatedPrice = orderLineId.ProductPrice;
                    }
                }
            });
            int currentQuantity = Integer.parseInt(holder.quantity.getText().toString());
            if (orderLineId.product != null) {
                holder.quantity.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        try {
                            String changeQty = holder.quantity.getText().toString().trim();
                            if (changeQty.isEmpty()) {
                                if (adapterCallbackCallback != null)
                                    adapterCallbackCallback.updateQtyChange(0, position);
                            }
                            if (Integer.parseInt(changeQty) > (orderLineId.product.Inventory)) {
                                orderLineId.UpdatedQuantity = orderLineId.product.Inventory;
                                holder.quantity.setText("" + orderLineId.UpdatedQuantity);
                                applyslab(holder, orderLineId);
                                BaseLoadingFragment.newInstance("Please Check Quantity", "Sorry, the product(s) you wish to purchase is(are) currently not available in requested quantity. Please try with reduced quantity of " + orderLineId.product.Inventory + " or less.", false, true)
                                        .show(((DetailOrderActivity) context).getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                if (adapterCallbackCallback != null) {
                                    adapterCallbackCallback.updateQtyChange(orderLineId.UpdatedQuantity, position);
                                    if (orderLineId.ProductPrice != orderLineId.UpdatedPrice)
                                        adapterCallbackCallback.refreshProductLineItems(position, orderLineId.UpdatedPrice, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                                }

                            } else {
                                orderLineId.UpdatedQuantity = Integer.parseInt(changeQty);
                                //holder.quantity.setText(String.valueOf(orderLineId.UpdatedQuantity));
                                applyslab(holder, orderLineId);
                                if (adapterCallbackCallback != null) {
                                    adapterCallbackCallback.updateQtyChange(orderLineId.UpdatedQuantity, position);
                                    if (orderLineId.ProductPrice != orderLineId.UpdatedPrice)
                                        adapterCallbackCallback.refreshProductLineItems(position, orderLineId.UpdatedPrice, orderLineId.ProductPrice, orderLineId.OrderId, orderLineId.OrderLineId, orderLineId.SKU, orderLineId.product);
                                }
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

        }
    }

    private void applyslab(DetailOrderItemsAdapterViewHolder holder, OrderLineId orderLineId) {
        //new added code for slab price
        if (orderLineId.product != null && orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 1) {
            for (int i = 0; i < orderLineId.product.VolumetricPricing.size(); i++) {
                if (i + 1 == orderLineId.product.VolumetricPricing.size()) {
                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                    break;
                } else if ((orderLineId.UpdatedQuantity >= orderLineId.product.VolumetricPricing.get(i).Quantity) && (orderLineId.UpdatedQuantity < ((orderLineId.product.VolumetricPricing.get(i + 1).Quantity)))) {
                    orderLineId.UpdatedPrice = (float) (orderLineId.product.VolumetricPricing.get(i).WebPrice);
                    holder.newPrice.setText(String.format("%.2f", orderLineId.UpdatedPrice));
                    break;
                }
            }
        } else {
            Logs.e("***", "Volumetric Pricing is null");
        }
    }

    private void doDeleteLineItem(int pos) {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        DeleteOrderLineItemRequest itemRequest = new DeleteOrderLineItemRequest();
        itemRequest.setCancelReason("Recalculate Order");
        itemRequest.setComment("Recalculate Order");
        itemRequest.setDate(today_date); // TODAY DATE yyyy-MM-dd
        itemRequest.setDisplayCommentToUser("false"); // False / True
        itemRequest.setMerchantid(Constants.MERCHANT_ID);
        itemRequest.setOperatorID(agentId);
        itemRequest.setOrderId(String.valueOf(orderLineIdAdapterList.get(pos).OrderId));

        DeleteOrderLineItemRequest.TobeCancelledOrderItem item = new DeleteOrderLineItemRequest.TobeCancelledOrderItem();
        item.setCancelQuantity(orderLineIdAdapterList.get(pos).Quantity);
        item.setOrderItemID(orderLineIdAdapterList.get(pos).OrderLineId);
        List<DeleteOrderLineItemRequest.TobeCancelledOrderItem> items = new ArrayList<>();
        items.add(item);
        itemRequest.setTobeCancelledOrderItems(items);

        Map<String, Object> inputs = new HashMap<>();
        inputs.put("MerchantId", Constants.MERCHANT_ID);
        inputs.put("InputFormat", "application/json");
        inputs.put("InputData", new Gson().toJson(itemRequest)); // TODO: DON'T APPEND MERCHANT ID IN URL
        DeveloperApiHelper DELETE_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/CancelItem").setMethod(APIMethod.POST).build();
        Disposable delete_item_disposable = orderService.deleteOrderLineItem(DELETE_ITEM_URL.getFinalUrl(), inputs)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<DeveloperAPIResponse>() {
                    @Override
                    public void accept(DeveloperAPIResponse response) throws Exception {
                        if ((response != null && response.messageCode != null && (response.messageCode.equalsIgnoreCase("1004") || response.messageCode.equalsIgnoreCase("1002")))) {
                            SupervisorAppMessages.showToast("Success", context);
                        } else {
                            SupervisorAppMessages.showToast("Failed", context);
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                });
        compositeDisposable.add(delete_item_disposable);

    }

    private void getVolumetricPricing(OrderLineId orderLineId, DetailOrderItemsAdapterViewHolder holder) {

        if (orderLineId.product.VolumetricPricing != null && orderLineId.product.VolumetricPricing.size() > 0) {
            updateSlabPrice(orderLineId.product.VolumetricPricing, orderLineId, holder);

//            //pickupAppDesign(orderLineId);
//
//            ReasonDialog reasonDialog = new ReasonDialog();
//            VolumetricLists volumetricLists = new VolumetricLists();
//            volumetricLists.volumetricPricing = orderLineId.product.VolumetricPricing;
//            Bundle bundle = new Bundle();
////            bundle.putBoolean("isSlab", true);
//            bundle.putString("Volproduct", new Gson().toJson(volumetricLists));
//            bundle.putString("orderLineId", new Gson().toJson(orderLineId));
//            reasonDialog.setArguments(bundle);
//            reasonDialog.show(fragment.getSupportFragmentManager(), "Reason Dialog");
        } else {
            int storeID = orderLineId.LocationId;
            if (storeID == 0) {
                SharedPreferences pref = context.getSharedPreferences("MyPref", SearchActivity.MODE_PRIVATE);
                storeID = pref.getInt(Constants.LOCATION_ID, 0);
            }

            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();

            FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("VolumetricPricing").build();
            Disposable disposable = orderService.getVolumetricPrice(aFEAPIHelper.getFinalUrl(), storeID + "", String.valueOf(orderLineId.product.ID))
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<VolumetricLists>() {
                        @Override
                        public void accept(VolumetricLists volumetricLists) throws Exception {
                            Gson gson = new Gson();
                            String frontApiResponseString = gson.toJson(volumetricLists, VolumetricLists.class);
                            Logger.logInfo("orderdetailactivity", "frontapi ", "accept " + frontApiResponseString);
                            Log.d("fapidata", frontApiResponseString);
                            if (volumetricLists != null && volumetricLists.MessageCode != null && volumetricLists.MessageCode.equalsIgnoreCase("200") && volumetricLists.volumetricPricing != null && volumetricLists.volumetricPricing.size() > 0) {
                                updateSlabPrice(volumetricLists.volumetricPricing, orderLineId, holder);
                                Logs.e("***", new Gson().toJson(volumetricLists));
//                                orderLineId.product.VolumetricPricing = volumetricLists.volumetricPricing;
//                                pickupAppDesign(orderLineId);
                            } else {
                                Log.d("fapidata", "Slab price API is NULL");
                            }
                        }

                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("orderdetailactivity", "frontapi ", "throwable " + throwable.getMessage());
                            throwable.printStackTrace();
                        }
                    });

            compositeDisposable.add(disposable);
        }

//        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
//        OrderService orderService = supervisorApplication.getOrderService();
//
//        FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("VolumetricPricing").build();
//        Disposable disposable = orderService.getVolumetricPrice(aFEAPIHelper.getFinalUrl(), orderLineId.LocationId+"", String.valueOf(orderLineId.ProductId))
//                .subscribeOn(supervisorApplication.subscribeScheduler())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<VolumetricLists>() {
//                    @Override
//                    public void accept(VolumetricLists volumetricLists) throws Exception {
//                        //ChangeVolumetricDataset(frontApiResponse.volumetricPricing);
//                        Gson gson = new Gson();
//                        String frontApiResponseString = gson.toJson(volumetricLists, VolumetricLists.class);
//                        Logger.logInfo("orderdetailactivity", "frontapi ", "accept " + frontApiResponseString);
//                        Log.d("fapidata", frontApiResponseString);
//                        if (volumetricLists != null && volumetricLists.MessageCode != null && volumetricLists.MessageCode.equalsIgnoreCase("200") && volumetricLists.volumetricPricing != null && volumetricLists.volumetricPricing.size() > 0) {
//                            updateSlabPrice(volumetricLists.volumetricPricing, orderLineId, holder);
//                        } else {
//                            Log.d("fapidata", "Slab price API is NULL");
//                        }
//                    }
//
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Logger.logInfo("orderdetailactivity", "frontapi ", "throwable " + throwable.getMessage());
//                        throwable.printStackTrace();
//                    }
//                });
//
//        compositeDisposable.add(disposable);

    }

    private void pickupAppDesign(OrderLineId orderLineId) {
        ReasonDialog reasonDialog = new ReasonDialog();
        VolumetricLists volumetricLists = new VolumetricLists();
        volumetricLists.volumetricPricing = orderLineId.product.VolumetricPricing;

        Bundle bundle = new Bundle();
        bundle.putBoolean("isSlab", true);
        bundle.putString("Volproduct", new Gson().toJson(volumetricLists));
        bundle.putString("orderLineId", new Gson().toJson(orderLineId));
        reasonDialog.setArguments(bundle);
        reasonDialog.show(fragment.getSupportFragmentManager(), "Reason Dialog");
    }

    @Override
    public int getItemCount() {
        return orderLineIdAdapterList.size();
    }

    private void updateSlabPrice(List<VolumetricPricing> volumetricPricingList, OrderLineId orderLineId, DetailOrderItemsAdapterViewHolder holder) {
        Gson gson = new Gson();
        String Discount = "";
        //orderLineId.product.isVolumetricVisible = true;
        holder.volumetric_table_parent.setVisibility(View.VISIBLE);
        VolumetricPricing volumetricPricing = volumetricPricingList.get(0);
        volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
        volumetricPricing.Discount = String.valueOf(Discount);
        for (int i = 0; i < volumetricPricingList.size(); i++) {
            volumetricPricing = volumetricPricingList.get(i);
          volumetricPricing.Discount = String.format("%.2f", 100 - ((volumetricPricing.WebPrice / orderLineId.product.MRP) * 100));
 //           volumetricPricing.Discount = String.valueOf(Math.round(100 - ((volumetricPricing.WebPrice / orderLineId.product.MRP) * 100))); // NEW - Volumetric discount in Integer roundOff
            if ((i + 1) == volumetricPricingList.size()) {
                volumetricPricing.DisplayQty = volumetricPricing.Quantity + " +";
            } else {
//                    if (volumetricPricing.Quantity == 1) {
//                        volumetricPricing.DisplayQty = String.valueOf(volumetricPricing.Quantity);
//                    } else {
                volumetricPricing.DisplayQty = volumetricPricing.Quantity + " to " + ((volumetricPricingList.get(i + 1).Quantity) - 1);
//                    }
            }

        }
        int i = 0;
        holder.volumetric_table.removeAllViews();
        for (VolumetricPricing volumetricpricing : volumetricPricingList) {
            View volumetric_view = LayoutInflater.from(context).inflate(R.layout.volumetric_table, null);
            TextView txt1 = (TextView) volumetric_view.findViewById(R.id.txt1);
            TextView txt2 = (TextView) volumetric_view.findViewById(R.id.txt2);
            TextView txt3 = (TextView) volumetric_view.findViewById(R.id.txt3);
            txt1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            txt2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            txt3.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
            View View1 = (View) volumetric_view.findViewById(R.id.View1);

            if (i == 0) {
                View1.setVisibility(View.GONE);
//                    product.isVolumetricVisible = false;
            } else {
                View1.setVisibility(View.VISIBLE);
//                    product.isVolumetricVisible = true;

            }
            if (volumetricpricing != null) {
                txt1.setText(String.valueOf(volumetricpricing.DisplayQty));
                txt2.setText(String.valueOf("₹ " + volumetricpricing.WebPrice));
                txt3.setText(String.valueOf(volumetricpricing.Discount + "%"));
                holder.volumetric_table.addView(volumetric_view);

            }
            i++;
            if (i == 3)
                break;
        }
        //orderLineId.product.isLoaded = true;

    }

    private void doAddLineItem(int position) {

        if (orderLineIdAdapterList.size() > 0) {
            //String locationCode = orderLineIdList.get(position).orders.OrderLineId != null ? orders.OrderLineId.size() > 0 ? orders.OrderLineId.get(0).LocationCode != null ? orders.OrderLineId.get(0).LocationCode : "" : "" : "";

            AddOrderLineItemRequest request = new AddOrderLineItemRequest();
            request.setDeliveryMode("H"); // STATIC - WONT CHANGE
            request.setItemAction(1); // STATIC - WONT CHANGE
            request.setLocationCode(orderLineIdAdapterList.get(position).LocationCode); // DYNAMIC - Get Location code from OrderInfo API
            request.setOrderId(String.valueOf(orderLineIdAdapterList.get(position).OrderId)); // DYNAMIC - Set Order ID from OrderInfo API
            request.setQuantity(1); // STATIC - WONT CHANGE
            request.setSKU(orderLineIdAdapterList.get(position).product.Sku); // DYNAMIC - SEARCH RESULT SKU OF PRODUCT
            request.setVarientSKU(""); // DYNAMIC - SEARCH RESULT VARIENT SKU OF PRODUCT
            Log.d("orderid", String.valueOf(orderLineIdAdapterList.get(position).OrderId));

            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();
            Map<String, Object> aParams = new HashMap<>();
            aParams.put("MerchantId", Constants.MERCHANT_ID);
            aParams.put("InputFormat", "application/json");
            aParams.put("InputData", new Gson().toJson(request));

            DeveloperApiHelper ADD_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/AddOrderLineItem").setMethod(APIMethod.POST).build();
            String URL = ADD_ITEM_URL.getFinalUrl();
            Log.e("***", "URL - " + URL);
            Disposable add_item_disposable = orderService.addOrderLineItem(URL, aParams)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<DeveloperAPIResponse>() {
                        @Override
                        public void accept(DeveloperAPIResponse response) throws Exception {
                            if (response != null && response.messageCode != null && response.messageCode.equalsIgnoreCase(Constants.SUCCESS_CODE)) {
                                SupervisorAppMessages.showToast(response.Message, context);
                                context.startActivity(DetailOrderActivity.launchDetail(context, order, ""));
                                ((Activity) context).finish();
                            } else if (response.Message != null) {
                                SupervisorAppMessages.showToast(response.Message, context);

                            } else {
                                SupervisorAppMessages.showToast(Constants.Try_Again, context);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {

                        }
                    });
            compositeDisposable.add(add_item_disposable);
        }
    }
}
