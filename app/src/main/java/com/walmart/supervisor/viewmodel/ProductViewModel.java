/**
 * Copyright 2016 Erik Jhordan Rey. <p/> Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at <p/> http://www.apache.org/licenses/LICENSE-2.0 <p/> Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.data.QueryConstants;
import com.walmart.supervisor.httpclient.FrontEndApiHelper;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.model.product.SearchRefine;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.Logs;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.view.SearchActivity;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ProductViewModel extends Observable {

    public ObservableInt orderProgress;
    public ObservableInt orderRecycler;
    public ObservableInt orderLabel;
    public ObservableField<String> messageLabel;

    private List<Product> productList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private BaseLoadingFragment baseLoadingFragment;

    public ProductViewModel(@NonNull Context context) {
        this.context = context;
        this.productList = new ArrayList<>();
        orderProgress = new ObservableInt(View.GONE);
        orderRecycler = new ObservableInt(View.GONE);
        orderLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_search_label));

    }

    public void onSearchClick(View view) {
        fetchProductList("", 0);
    }

    //It is "public" to show an example of test
    public void initializeViews() {
        orderLabel.set(View.GONE);
        orderRecycler.set(View.GONE);
        orderProgress.set(View.GONE);
    }

    public void onClickClose(View view) {
        ((Activity) context).finish();
    }

    public void fetchProductList(String query, int storeId) {
        if (NetworkUtil.isOnline(context)) {
            initializeViews();
            baseLoadingFragment = BaseLoadingFragment.newInstance("Loading order items", "Please wait...", !Constants.ISLOTS);
            baseLoadingFragment.show(((SearchActivity) context).getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
            SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
            OrderService orderService = supervisorApplication.getOrderService();

            Logger.logInfo("SearchActivity", "ProductViewModel ", "fetchProductList");

            FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("Search/Refine").build();
            Logs.e("***", aFEAPIHelper.getFinalUrl());
            Disposable disposable = orderService.searchRefine(aFEAPIHelper.getFinalUrl(), query,
                    "", "", "", "", "", "", "", "",
                    "", 1, 10, true, storeId + "", QueryConstants.SORT_CATALOG, QueryConstants.OUTPUT_FORMAT)
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<SearchRefine>() {
                        @Override
                        public void accept(SearchRefine searchRefine) throws Exception {
                            changeProductDataSet(searchRefine.getProductList());
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.GONE);
                            orderRecycler.set(View.VISIBLE);
                            Gson gson = new Gson();
                            String searchRefineString = gson.toJson(searchRefine, SearchRefine.class);
                            Logger.logInfo("SearchActivity", "searchRefine ", "accept " + searchRefineString);
                            if (baseLoadingFragment != null) {
                                baseLoadingFragment.dismiss();
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            messageLabel.set(context.getString(R.string.error_loading_order));

                            Logger.logInfo("SearchActivity", "searchRefine ", "throwable " + throwable.getMessage());
                            throwable.printStackTrace();
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.VISIBLE);
                            orderRecycler.set(View.GONE);
                            if (baseLoadingFragment != null) {
                                baseLoadingFragment.dismiss();
                            }
                        }
                    });

            compositeDisposable.add(disposable);
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }
    }

    private void changeProductDataSet(List<Product> products) {
        productList = new ArrayList<>();
        productList.addAll(products);
        setChanged();
        notifyObservers();
    }

    public List<Product> getProductList() {
        return productList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}
