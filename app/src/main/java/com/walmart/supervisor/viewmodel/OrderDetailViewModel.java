/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderFactory;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.bumptech.glide.Glide;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.CancelResponse;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.OrderDetailActivity;
import com.walmart.supervisor.view.SearchActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class OrderDetailViewModel extends Observable implements AdapterView.OnItemSelectedListener {

    private Orders Order;
    public ObservableInt orderProgress;
    public ObservableInt orderRecycler;
    public ObservableInt orderLabel;
    public ObservableField<String> messageLabel;
    public ObservableField<String>grandTotalValue;
    public ObservableField<String>noOfItem;
    public ObservableField<String>recalculatedGrandTotal;
    public ObservableField<String>recalculatedNoOfItem;

    String orderID;
    String merchantID,date,pgresponse;
    List<OrderLineId> orderLineIdList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    String[] country = { "India", "USA", "UK", "Canada", "Other"};
    String usercomment;

    public OrderDetailViewModel(@NonNull Context context, Orders Order) {
        this.context = context;
        this.Order = Order;
        orderID = String.valueOf(Order.OrderId);
        merchantID = String.valueOf(Order.MerchantId);
        date = Order.OrderDate;
        //pgresponse = Order.PaymentDetails.get(0).PaymentResponse;
        this.orderLineIdList = new ArrayList<>();
        this.orderProgress = new ObservableInt(View.GONE);
        this.orderRecycler = new ObservableInt(View.GONE);
        this.orderLabel = new ObservableInt(View.GONE);
        this.messageLabel = new ObservableField<>(context.getString(R.string.default_loading_items));
        this.grandTotalValue = new ObservableField<>(String.valueOf(0));
        this.noOfItem = new ObservableField<>(String.valueOf(0));
        this.recalculatedGrandTotal = new ObservableField<>(String.valueOf(0));
        this.recalculatedNoOfItem = new ObservableField<>(String.valueOf(0));
        initializeViews();
        fetchOrderDetailList();
    }

    private void getgrandtotal() {
        float gt= Float.valueOf(grandTotalValue.get());
        noOfItem.set(String.valueOf(orderLineIdList.size()));
       if(orderLineIdList.size()>0) {
           for (int i = 0; i <= orderLineIdList.size(); i++) {
               gt = gt + (orderLineIdList.get(i).Quantity * orderLineIdList.get(i).ProductPrice);
               grandTotalValue.set(String.valueOf(gt));
            }
         }
    }

    public void initializeViews() {
        orderLabel.set(View.VISIBLE);
        orderRecycler.set(View.GONE);
        orderProgress.set(View.VISIBLE);
    }

    public String getOrderId() {
        return String.valueOf(Order.OrderId);
    }

    public String getOrderDate() {
        if (Order != null && Order.OrderDate != null) {
            String[] aSplittedArray = Order.OrderDate.split("\\(");
            String aTime = aSplittedArray[1].split("\\+")[0];
            return DateHelper.getDate(aTime);
        }
        return "--";
    }

    public String getTotalAmount() {
        return String.valueOf(Order.TotalAmount);
    }

    public List<OrderLineId> getOrderList() {
        return orderLineIdList;
    }

    public void onClickAddNewItem(View view) {
        proceedToSearchActivity();
        // Toast.makeText(view.getContext(), "Not implemented!", Toast.LENGTH_LONG).show();
    }

    private void proceedToSearchActivity() {
       // ((Activity) context).finish();
        context.startActivity(SearchActivity.launchSearch(context, null));
    }

    public void onClickRefresh(View view) {
        this.messageLabel.set(context.getString(R.string.default_refreshing_items));
        initializeViews();
        fetchOrderDetailList();
    }

    public void onClickConfirmOrder(View view) {

    }

    public void onClickRecalculate(View view) {
        ItemOrderDetailViewModel.getRecalculateData(recalculatedGrandTotal,orderLineIdList);
        recalculatedNoOfItem.set(String.valueOf(orderLineIdList.size()));
    }

    public void onClickCancelOrder(View view) {
        // cancel order api call
        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.cancel_order_popup);
        Spinner spinner = (Spinner)dialog.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);

        EditText comment = (EditText)dialog.findViewById(R.id.edit_text_comment);
        Button cancel = (Button)dialog.findViewById(R.id.btn_cancel_order) ;
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinner.isSelected())
                {
                    SupervisorAppMessages.showToast("Please select type",context);
                }
                else if(comment.getText().toString().length()==0)
                {
                    SupervisorAppMessages.showToast("Please comment",context);
                }
                else
                {
                    usercomment = comment.getText().toString();
                    doCancelOrder();
                    Log.d("merchantid",merchantID);
                    Log.d("orderid",orderID);
                    dialog.dismiss();
                   // SupervisorAppMessages.showToast("else part",context);
                }
            }
        });

        ImageButton canceldialog = (ImageButton)dialog.findViewById(R.id.cancel_dialog);
        canceldialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }

    public void fetchOrderDetailList() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();

        if (!TextUtils.isEmpty(orderID)) {
            Logger.logInfo("fetchOrderDetail", "--- ORDER ID " + orderID, "");
            DeveloperApiHelper orderAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order").setServiceName(String.valueOf(orderID)).setMethod(APIMethod.GET).build();
            Disposable disposable = orderService.getOrderDetails(orderAPIHelper.getFinalUrl())
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<OrderResponse>() {
                        @Override
                        public void accept(OrderResponse orderResponse) throws Exception {
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.GONE);
                            orderRecycler.set(View.VISIBLE);
                            if (orderResponse.getOrderList().size() > 0) {
                                changeOrderDataSet(orderResponse.getOrderList().get(0).OrderLineId);
                            }
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("fetchOrderDetail", "--- throwable "+throwable.getMessage(), "");
                        }
                    });
            compositeDisposable.add(disposable);
        } else {
            Toast.makeText(context, "Error Occured!", Toast.LENGTH_LONG).show();
        }
    }

    private void changeOrderDataSet(List<OrderLineId> orderLineIdList) {
        this.orderLineIdList.clear();
        this.orderLineIdList.addAll(orderLineIdList);
        setChanged();
        notifyObservers();
        getgrandtotal();
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    @Deprecated
    private void doCancelOrder() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("MerchantId", merchantID);
       // aParams.put("Date",);
      //  aParams.put("Comment",usercomment);
        aParams.put("OrderId", orderID);
       // aParams.put("PGResponse",usercomment);


        if (!TextUtils.isEmpty(orderID)) {

            DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/Cancel").setMethod(APIMethod.POST).build();
            Log.d("cancelurl",aDAPIHelper.getFinalUrl());
            Disposable disposable = orderService.cancelOrder(aDAPIHelper.getFinalUrl(), "", "")
                    .subscribeOn(supervisorApplication.subscribeScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CancelResponse>() {
                        @Override
                        public void accept(CancelResponse cancelResponse) throws Exception {
                            Logger.logInfo("doCancelOrder", "--- ENDED ordercancel ", "");
                            Logger.logInfo("doCancelOrder", "--- Order cancel Success MESSAGE CODE " + cancelResponse.messageCode, "");
                            Logger.logInfo("doCancelOrder", "--- Order cancel Success MESSAGE " + cancelResponse.Message, "");
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.GONE);
                            orderRecycler.set(View.VISIBLE);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Logger.logInfo("doCancelOrder", "--- ENDED doCancelOrder ", "");
                            messageLabel.set(context.getString(R.string.error_loading_order));
                            Logger.logInfo("doCancelOrder", "--- Error " + throwable.getMessage(), "");
                            throwable.printStackTrace();
                            orderProgress.set(View.GONE);
                            orderLabel.set(View.VISIBLE);
                            orderRecycler.set(View.GONE);
                        }
                    });
            compositeDisposable.add(disposable);
         }


        }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
