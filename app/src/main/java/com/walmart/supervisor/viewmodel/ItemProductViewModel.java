/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.constants.Constants;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.FrontEndApiHelper;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.product.Product;
import com.walmart.supervisor.model.slabprice.VolumetricLists;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.view.SearchActivity;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


public class ItemProductViewModel extends BaseObservable {

    private final Orders orders;
    private Product product;
    private Context context;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    BaseLoadingFragment baseLoadingF;

    public ItemProductViewModel(Product product, Context context, Orders orders) {
        this.product = product;
        this.context = context;
        this.orders = orders;
    }

    public String getProductTitle() {
        return product.Title;
    }

    /* public String getProductImage()
     {
         String imageUrl = product.LargeImage.split(";")[0];
         Log.d("imgurl",product.LargeImage);
         return imageUrl;
     }*/
    public String getProductSKU() {
        return "Item No  : " + product.Sku;
    }

    public String getProductMRP() {
        return "MRP  : " + String.format("%.2f", product.MRP);
    }

    public String getProductWebPrice() {
        return "Our Price  : " + String.format("%.2f", product.WebPrice);
    }

//  public String getCell() {
//    return product.cell;
//  }
//
//  public String getMail() {
//    return product.mail;
//  }
//
//  public String getPictureProfile() {
//    return product.picture.medium;
//  }

   /* @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).placeholder(R.mipmap.ic_no_image).into(imageView);
    }*/

    public void onItemClick(View view) {

    }

    public void onClickAddItem(View view) {
        product.orderId = orders.OrderId;
        int storeID = 0;
        if(orders!=null && orders.OrderLineId!=null && orders.OrderLineId.size()>0) {
            if( orders.OrderLineId.get(0).LocationId!=0) {
                storeID = orders.OrderLineId.get(0).LocationId;
            }
        }
        if (storeID == 0) {
            SharedPreferences pref = context.getSharedPreferences("MyPref", SearchActivity.MODE_PRIVATE);
            storeID = pref.getInt(Constants.LOCATION_ID, 0);
        }

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();

        FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("VolumetricPricing").build();
        Disposable disposable = orderService.getVolumetricPrice(aFEAPIHelper.getFinalUrl(), storeID + "", String.valueOf(product.ID))
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<VolumetricLists>() {
                    @Override
                    public void accept(VolumetricLists volumetricLists) throws Exception {
                        //ChangeVolumetricDataset(frontApiResponse.volumetricPricing);
                        Gson gson = new Gson();
                        String frontApiResponseString = gson.toJson(volumetricLists, VolumetricLists.class);
                        Logger.logInfo("orderdetailactivity", "frontapi ", "accept " + frontApiResponseString);
                        Log.d("fapidata", frontApiResponseString);
                        if (volumetricLists != null && volumetricLists.MessageCode != null && volumetricLists.MessageCode.equalsIgnoreCase("200") && volumetricLists.volumetricPricing != null && volumetricLists.volumetricPricing.size() > 1) {
                            product.VolumetricPricing = volumetricLists.volumetricPricing;
                        } else {
                            Log.d("fapidata", "Slab price API is NULL");
                        }
                        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        String prductData = new Gson().toJson(product);
                        editor.putString("productInfo", prductData);
                        editor.apply();

                        ((Activity) context).finish();

                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("orderdetailactivity", "frontapi ", "throwable " + throwable.getMessage());
                        throwable.printStackTrace();

                        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        String prductData = new Gson().toJson(product);
                        editor.putString("productInfo", prductData);
                        editor.apply();

                        ((Activity) context).finish();
                    }
                });

        compositeDisposable.add(disposable);


//        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        String prductData = new Gson().toJson(product);
//        editor.putString("productInfo", prductData);
//        editor.apply();
//
//        ((Activity) context).finish();
//        if (NetworkUtil.isOnline(context)) {
//            if (orders != null) {
//                String locationCode = orders.OrderLineId != null ? orders.OrderLineId.size() > 0 ? orders.OrderLineId.get(0).LocationCode != null ? orders.OrderLineId.get(0).LocationCode : "" : "" : "";
//                baseLoadingF = BaseLoadingFragment.newInstance("Adding Order item", "Please wait...", true);
//                baseLoadingF.show(((SearchActivity)context).getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
//                AddOrderLineItemRequest request = new AddOrderLineItemRequest();
//                request.setDeliveryMode("H"); // STATIC - WONT CHANGE
//                request.setItemAction(1); // STATIC - WONT CHANGE
//                request.setLocationCode(locationCode); // DYNAMIC - Get Location code from OrderInfo API
//                request.setOrderId(String.valueOf(orders.OrderId)); // DYNAMIC - Set Order ID from OrderInfo API
//                request.setQuantity(1); // STATIC - WONT CHANGE
//                request.setSKU(product.Sku); // DYNAMIC - SEARCH RESULT SKU OF PRODUCT
//                request.setVarientSKU(""); // DYNAMIC - SEARCH RESULT VARIENT SKU OF PRODUCT
//
//                SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
//                OrderService orderService = supervisorApplication.getOrderService();
//                Map<String, Object> aParams = new HashMap<>();
//                aParams.put("MerchantId", Constants.MERCHANT_ID);
//                aParams.put("InputFormat", "application/json");
//                aParams.put("InputData", new Gson().toJson(request));
//
//                DeveloperApiHelper ADD_ITEM_URL = new DeveloperApiHelper.Builder().endPoint("Order/AddOrderLineItem").setMethod(APIMethod.POST).build();
//                String URL = ADD_ITEM_URL.getFinalUrl();
//                Log.e("***", "URL - " + URL);
//                Disposable add_item_disposable = orderService.addOrderLineItem(URL, aParams)
//                        .subscribeOn(supervisorApplication.subscribeScheduler())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(new Consumer<DeveloperAPIResponse>() {
//                            @Override
//                            public void accept(DeveloperAPIResponse response) throws Exception {
//                                if (response != null && response.messageCode != null && response.messageCode.equalsIgnoreCase(Constants.SUCCESS_CODE)) {
//                                    ((Activity) context).finish();
//                                    baseLoadingF.dismiss();
//                                } else if(response.Message!=null) {
//                                    SupervisorAppMessages.showToast(response.Message,context);
//                                    baseLoadingF.dismiss();
//                                }
//                                else {
//                                    SupervisorAppMessages.showToast(Constants.Try_Again,context);
//                                    baseLoadingF.dismiss();
//                                }
//                            }
//                        }, new Consumer<Throwable>() {
//                            @Override
//                            public void accept(Throwable throwable) throws Exception {
//                                baseLoadingF.dismiss();
//
//                            }
//                        });
//                compositeDisposable.add(add_item_disposable);
//            }
//        }
//        else {
//            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
//            noInternetDialog.show();
//        }
    }

    public void setProduct(Product product) {
        this.product = product;
        notifyChange();
    }
}
