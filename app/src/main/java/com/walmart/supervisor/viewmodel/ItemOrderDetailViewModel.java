/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.data.QueryConstants;
import com.walmart.supervisor.httpclient.FrontEndApiHelper;
import com.walmart.supervisor.model.order.OrderLineId;
import com.walmart.supervisor.model.slabprice.VolumetricLists;
import com.walmart.supervisor.model.slabprice.VolumetricPricing;
import com.walmart.supervisor.utils.Logger;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


public class ItemOrderDetailViewModel extends BaseObservable {

    private OrderLineId orderLineId;
    private Context context;
    private int edtQty;
    public static ObservableField<String> quantity;
    //public static   ObservableField<String>newPrice;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    public ObservableInt visibleStatus;
    int clickcount=0;
    List<VolumetricPricing> volumetricPricingList;

    public ItemOrderDetailViewModel(OrderLineId orderLineId, Context context) {
        this.orderLineId = orderLineId;
        this.context = context;
        quantity = new ObservableField<>(String.valueOf(orderLineId.Quantity));
        //newPrice = new ObservableField<>(String.valueOf(orderLineId.ProductPrice));
        visibleStatus = new ObservableInt(View.GONE);
    }

    public String getProductTitle() {
        if (orderLineId.ProductTitle != null) {
            return orderLineId.ProductTitle;
        }
        return context.getString(R.string.not_available);
    }

    public String getMRP() {
        if (orderLineId.MRP != null) {
            return "MRP : " + String.valueOf(orderLineId.MRP);
        }
        return "MRP : " + context.getString(R.string.not_available);
    }

    /*public String getQuantity() {
        //edtQty = orderLineId.Quantity;
        quantity = new ObservableField<>(String.valueOf(orderLineId.Quantity));
        return quantity.get();
    }*/
    public  String getNewPrice()
    {
        return String.valueOf(orderLineId.ProductPrice);
    }

    public String getSKU() {
        return "Item Number : " + String.valueOf(orderLineId.SKU);
    }

    public String getProductPrice() {
        if (orderLineId.ProductPrice >= 0) {
           // Log.d("quantity","qty"+orderLineId.Quantity);
            return "Current Price  : " + String.valueOf(orderLineId.ProductPrice);
        }
        return "Current Price  : " + context.getString(R.string.not_available);
    }

    public String getOrderedPrice() {
        if (orderLineId.ProductPrice >= 0) {
            System.out.println(orderLineId.ProductId);
            Log.d("pid",String.valueOf(orderLineId.ProductId));
            return "Ordered Price  : " + String.valueOf(orderLineId.ProductPrice);
        }
        return "Ordered Price  : " + context.getString(R.string.not_available);
    }

    public String getProductImage() {
        Logger.logInfo("ItemOrderDetailViewModel", "getPictureProfile", "" + orderLineId.ImageUrl);
        String imageUrl = orderLineId.ImageUrl.split(";")[0];
        Logger.logInfo("ItemOrderDetailViewModel", "filtered getPictureProfile", "" + imageUrl);
        return imageUrl;
    }

    public String getTotalAmount() {
        return String.valueOf(orderLineId.ProductPrice);
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).placeholder(R.mipmap.ic_no_image).into(imageView);
    }

    public void setOrderLineId(OrderLineId orderLineId) {
        this.orderLineId = orderLineId;
        notifyChange();
    }

    public void onClickLess(View View)
    {
        int q = Integer.parseInt(quantity.get());
        if(q!=1)
        {
            q--;
            quantity.set(String.valueOf(orderLineId.Quantity));
           // updatedPrice.set("Rs "+String.valueOf(Integer.parseInt(quantity.get())*(orderLineId.ProductPrice)));
        }

    }

    public void onClickMore(View view)
    {
        int q = Integer.parseInt(quantity.get());
        q++;
        quantity.set(String.valueOf(orderLineId.Quantity));
       // updatedPrice.set("RS "+String.valueOf(Integer.parseInt(quantity.get())*(orderLineId.ProductPrice)));
    }
    public void getSlabPrice(View view)
    {
        clickcount = clickcount + 1;
        if(clickcount == 1 )
        {
            visibleStatus.set(View.VISIBLE);
            getVolumetricpricing();
        }
        else
        {
            visibleStatus.set(View.GONE);
            clickcount = 0;
        }

    }

    private void getVolumetricpricing() {

        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();

        FrontEndApiHelper aFEAPIHelper = new FrontEndApiHelper.Builder().endPoint("VolumetricPricing").build();
        Disposable disposable = orderService.getVolumetricPrice(aFEAPIHelper.getFinalUrl(),QueryConstants.STORE_ID,String.valueOf(orderLineId.ProductId))
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<VolumetricLists>() {
                    @Override
                    public void accept(VolumetricLists frontApiResponse) throws Exception {
                        //ChangeVolumetricDataset(frontApiResponse.volumetricPricing);
                        Gson gson = new Gson();
                        String frontApiResponseString = gson.toJson(frontApiResponse, VolumetricLists.class);
                        Logger.logInfo("orderdetailactivity", "frontapi ", "accept " + frontApiResponseString);
                        Log.d("fapidata",frontApiResponseString);
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("orderdetailactivity", "frontapi ", "throwable " + throwable.getMessage());
                        throwable.printStackTrace();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void ChangeVolumetricDataset(List<VolumetricPricing> volumetricPricing) {
        volumetricPricingList.addAll(volumetricPricing);
        notifyChange();
    }


    public static void getRecalculateData(ObservableField<String> grandTotal, List<OrderLineId> orderLineIdList)
    {
        float gt = 0;
        if(orderLineIdList.size()>0) {
            int orderlength = orderLineIdList.size()-1;
            for (int i = 0; i <= orderlength; i++) {
              //  gt = gt + (Float.valueOf(quantity.get()) * Float.valueOf(newPrice.get()));
                Log.d("gt", "" + gt);
            }
        }
        grandTotal.set(String.valueOf(gt));

    }

}
