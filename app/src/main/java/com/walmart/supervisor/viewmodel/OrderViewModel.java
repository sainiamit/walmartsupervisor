/**
 * Copyright 2016 Erik Jhordan Rey. <p/> Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at <p/> http://www.apache.org/licenses/LICENSE-2.0 <p/> Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderResponse;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.databinding.OrderFragmentBinding;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.model.order.PaymentDetails;
import com.walmart.supervisor.utils.Logger;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.view.OrderSectionsAdapter;
import com.walmart.supervisor.view.fragments.OrdersFragment;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class OrderViewModel extends Observable {

    public ObservableInt orderProgress;
    public ObservableInt orderRecycler;
    public ObservableInt orderLabel;
    public ObservableField<String> messageLabel;
    private int fragmentPosition;
    private List<Orders> orderList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    String mLocationId;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    Date date = new Date();
    private SimpleDateFormat dateFormatter;
    private CrystalRangeSeekbar rangeSeekbar;
    String astartDate,aendDate;


    private List<Orders> backUpAllOrders;

    public OrderViewModel(@NonNull Context context, int fragmentPosition, String LocationId) {
        this.context = context;
        this.orderList = new ArrayList<>();
        this.mLocationId = LocationId;
        orderProgress = new ObservableInt(View.GONE);
        orderRecycler = new ObservableInt(View.GONE);
        orderLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_order));
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,-30);
        aendDate = dateFormatter.format(date);
        astartDate = dateFormatter.format(calendar.getTime());
        Log.d("dates",""+astartDate+""+aendDate);
        this.fragmentPosition = fragmentPosition;
        initializeViews();
        if (NetworkUtil.isOnline(context)) {
            fetchOrderList();
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }

    }
    /*public void onClickFabLoad(View view) {
        filterOrders(view.getContext());
    }*/

    public void onClickFabLoad(View view) {
        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.filter_popup);
        //dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        ImageButton canceldialog = (ImageButton)dialog.findViewById(R.id.cancel_filter_dialog);
        canceldialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView txtMin =(TextView)dialog.findViewById(R.id.min_range);
        TextView txtMax =(TextView)dialog.findViewById(R.id.max_range);
        rangeSeekbar = (CrystalRangeSeekbar)dialog.findViewById(R.id.rangeSeekbar);
        rangeSeekbar.setDataType(CrystalRangeSeekbar.DataType.DOUBLE);
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                double testMin = (Math.round(minValue.doubleValue() * 100.0) / 100.0);
                double testMax = (Math.round(maxValue.doubleValue() * 100.0) / 100.0);

//                Float minPriceFloat = Float.parseFloat(minPriceString);
//                Float maxPriceFloat = Float.parseFloat(maxPriceString);
                DecimalFormat df = new DecimalFormat("0");
                // df.setMaximumFractionDigits(2);
                txtMin.setText(df.format(testMin));
                txtMax.setText(df.format(testMax));
            }
        });

        Button startDate =(Button)dialog.findViewById(R.id.start_date);
        startDate.setText(dateFormatter.format(date));
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStartDateDilog(startDate);
                fromDatePickerDialog.show();

            }
        });
        Button endDate = (Button)dialog.findViewById(R.id.end_date);
        endDate.setText(dateFormatter.format(date));
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndDateDialog(endDate);
                toDatePickerDialog.show();
            }
        });


        Button reset = (Button)dialog.findViewById(R.id.btn_reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Button applyFilter = (Button)dialog.findViewById(R.id.btn_apply_filter);
        applyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //filterorders(startDate.getText().toString(),endDate.getText().toString());
                //filterOrders(context);
                astartDate = startDate.getText().toString();
                aendDate = endDate.getText().toString();
                filterOrders(context);
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    private void filterOrders(Context context) {
        this.context = context;
        List<Orders> temp_arraylist = new ArrayList<Orders>();

        for (int i = 0; i < orderList.size(); i++) {
            float price = orderList.get(i).AmountPayable;
            if (price >= 2000 && price <= 3000) {
                //backUpAllOrders
                temp_arraylist.add(orderList.get(i));
            }
        }
        Log.d("list",""+temp_arraylist);
        updateOrderList(temp_arraylist);
    }

    private void showEndDateDialog(Button endDate) {
        Calendar endCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                endDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONTH), endCalendar.get(Calendar.DAY_OF_MONTH));
        toDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        toDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-(2592000000L));
    }

    private void showStartDateDilog(Button startDate) {
        Calendar startCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                startDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis()));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-(2592000000L));
    }

    //It is "public" to show an example of test
    public void initializeViews() {
        orderLabel.set(View.VISIBLE);
        orderRecycler.set(View.GONE);
        orderProgress.set(View.VISIBLE);
    }

    public void fetchOrderList() {
        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        Map<String, Object> aParams = new HashMap<>();
        aParams.put("FromDate", "2018-05-30");
        aParams.put("ToDate", "2018-06-19");
        aParams.put("UserId", "");
        aParams.put("LocationId", mLocationId);
        //aParams.put("LocationId", ",248,250,212");


        Logger.logInfo("fetchOrderList", "--- aParams " + aParams, "");
        Logger.logInfo("fetchOrderList", "--- START fetchOrderHistory ", "");

        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("Order/History").setMethod(APIMethod.POST).build();
        Disposable disposable = orderService.fetchOrderHistory(aDAPIHelper.getFinalUrl(), aParams)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderResponse>() {
                    @Override
                    public void accept(OrderResponse orderResponse) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        updateOrderList(orderResponse.getOrderList());
                        backUpAllOrders = orderResponse.getOrderList();
                        Logger.logInfo("fetchOrderHistory", "--- Order History Success MESSAGE CODE " + orderResponse.messageCode, "");
                        orderProgress.set(View.GONE);
                        orderLabel.set(View.GONE);
                        orderRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Logger.logInfo("fetchOrderHistory", "--- ENDED fetchOrderHistory ", "");
                        messageLabel.set(context.getString(R.string.error_loading_order));
                        Logger.logInfo("fetchOrderHistory", "--- Error " + throwable.getMessage(), "");
                        throwable.printStackTrace();
                        orderProgress.set(View.GONE);
                        orderLabel.set(View.VISIBLE);
                        orderRecycler.set(View.GONE);
                    }
                });
        compositeDisposable.add(disposable);
    }

    public void updateOrderList(List<Orders> ordersList) {
        Logger.logInfo("updateOrderList", "--- ordersList.size() " + ordersList.size(), "");
        List<Orders> filteredOrdersList = new ArrayList<>();
        for (Orders order : ordersList) {
            String paymentStatus = "P";
            if (fragmentPosition == OrderSectionsAdapter.PICKED_ORDERS) {
                paymentStatus = "A";
            }
            if (order.Status.equalsIgnoreCase(paymentStatus)) {
                boolean isPaymentCredit = false;
                PaymentLoop:
                for (int i = 0; i < order.PaymentDetails.size(); i++) {
                    PaymentDetails paymentDetails = order.PaymentDetails.get(i);
                   // Logger.logInfo("updateOrderList", "--- paymentDetails.PaymentTypes " + paymentDetails.PaymentType, "");
                   // Logger.logInfo("updateOrderList", "--- paymentDetails.PaymentOption " + paymentDetails.PaymentOption, "");
                    if (paymentDetails.PaymentType.equalsIgnoreCase("TPG")) {
                        isPaymentCredit = true;
                        break PaymentLoop;
                    }
                }
                if (!isPaymentCredit)
                    filteredOrdersList.add(order);
            }
        }
       // Logger.logInfo("updateOrderList", "--- filteredOrdersList.size() " + filteredOrdersList.size(), "");
        changeOrderDataSet(filteredOrdersList);
    }


    private void changeOrderDataSet(List<Orders> orders) {
        orderList.addAll(orders);
       // Logger.logInfo("updateOrderList", "--- filteredOrdersList1.size() " + orderList.size(), "");
        setChanged();
        notifyObservers();
    }


    public List<Orders> getOrderList() {
        return orderList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

}
