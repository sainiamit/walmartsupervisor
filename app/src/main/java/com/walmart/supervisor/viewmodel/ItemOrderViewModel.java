/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.walmart.supervisor.model.Order;
import com.walmart.supervisor.model.order.Orders;
import com.walmart.supervisor.utils.DateHelper;
import com.walmart.supervisor.view.DetailOrderActivity;
import com.walmart.supervisor.view.OrderDetailActivity;


public class ItemOrderViewModel extends BaseObservable {

    private Orders order;
    private Context context;

    public ItemOrderViewModel(Orders order, Context context) {
        this.order = order;
        this.context = context;
    }

    public String getOrderId() {
        return String.valueOf(order.OrderId);
    }

    public String getOrderDate() {
        String[] aSplittedArray = order.OrderDate.split("\\(");
        String aTime = aSplittedArray[1].split("\\+")[0];
        return DateHelper.getDate(aTime);
    }

    public String getTotalAmount() {
        return String.format("%.2f",order.TotalAmount);
    }
    public String getMemberName()
    {
        if(order.BillFirstname != null && order.BillFirstname.trim().length() > 0)
        {
            return order.BillFirstname+" "+ order.BillLastname;
        }
        else if(order.ShipFirstname!= null && order.ShipFirstname.trim().length() > 0)
        {
            return order.ShipFirstname+" "+ order.ShipLastname;
        }
        else
        {
            return "Name not available";
        }


    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    public void onItemClick(View view) {
        context.startActivity(DetailOrderActivity.launchDetail(view.getContext(), this.order, ""));
    }

    public void setOrder(Orders Order) {
        this.order = Order;
        notifyChange();
    }
}
