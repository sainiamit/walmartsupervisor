package com.walmart.supervisor.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.walmart.supervisor.R;
import com.walmart.supervisor.SupervisorApplication;
import com.walmart.supervisor.data.OrderService;
import com.walmart.supervisor.httpclient.APIMethod;
import com.walmart.supervisor.httpclient.DeveloperApiHelper;
import com.walmart.supervisor.model.LoginResponse;
import com.walmart.supervisor.model.login.RoleDetails;
import com.walmart.supervisor.utils.AppUtils;
import com.walmart.supervisor.utils.Logs;
import com.walmart.supervisor.utils.NetworkUtil;
import com.walmart.supervisor.utils.SupervisorAppMessages;
import com.walmart.supervisor.view.OrderListActivity;
import com.walmart.supervisor.view.fragments.BaseLoadingFragment;

import java.util.Observable;

import am.appwise.components.ni.NoInternetDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class LoginViewModel extends Observable {

    public ObservableInt loginProgress;
    public ObservableField<String> messageLabel;

    public ObservableField<String> username;
    public ObservableField<String> password;
    private boolean checked;
    private Context context;
    private FragmentActivity fragmentActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LoginViewModel(@NonNull Context context, FragmentActivity fragmentActivity) {
        this.context = context;
        this.fragmentActivity = fragmentActivity;
        loginProgress = new ObservableInt(View.GONE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_order));
//        username = new ObservableField<>("vijay.vemana@walmart.com");
//        password = new ObservableField<>("Vijay5677**");
        username = new ObservableField<>("");
        password = new ObservableField<>("");
        SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
        if (pref != null) {
            boolean isLogged = pref.getBoolean("isLogged", false);
            String sLocationid = pref.getString("Location_Id", null);
            if (isLogged == true) {
                proceedToOrdersList(sLocationid);
            }
        }


    }

    public void onClickLogin(View view) {
        String usernameStr = username.get();
        String passwordStr = password.get();
        if (NetworkUtil.isOnline(context)) {
            if (isUserDataValid(usernameStr, passwordStr)) {
                doUserLogin(usernameStr, passwordStr);
                loginProgress.set(View.VISIBLE);
            }
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }
    }

    boolean isUserDataValid(String username, String password) {
        if (!AppUtils.isValidEmail(username)) {
            //new SupervisorAppMessages().showStyleableToast(context, "Enter valid Username!", R.color.white, R.color.toast_bg);
            Toast.makeText(context,"Enter valid Username!",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!(password.length() > 0)) {
            //new SupervisorAppMessages().showStyleableToast(context, "Enter valid Password!", R.color.white, R.color.toast_bg);
            Toast.makeText(context,"Enter valid Password!",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void onCheckRememberMe(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked == true) {
            String ustr = username.get();
            String pstr = password.get();
            SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("userName", ustr);
            editor.putString("password", pstr);
            editor.putBoolean("isLogged", true);
            editor.commit();
        } else {
            SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
            if (pref != null) {
                SharedPreferences.Editor editor = pref.edit();
                editor.remove("userName");
                editor.remove("password");
                editor.remove("isLogged");
                editor.commit();
            }
        }
    }


    private void proceedToOrdersList(String locationID) {
        //context.startActivity(OrderActivity.launchOrder(context, locationID));
        if (NetworkUtil.isOnline(context)) {
            ((Activity) context).finish();
            context.startActivity(OrderListActivity.launchOrder(context));
        } else {
            NoInternetDialog noInternetDialog = new NoInternetDialog.Builder(context).build();
            noInternetDialog.show();
        }

    }

    public void doUserLogin(String user_name, String user_password) {

        DeveloperApiHelper aDAPIHelper = new DeveloperApiHelper.Builder().endPoint("customer").setServiceName("agentlogin").setMethod(APIMethod.POST).build();
        SupervisorApplication supervisorApplication = SupervisorApplication.create(context);
        OrderService orderService = supervisorApplication.getOrderService();
        Logs.e("***", aDAPIHelper.getFinalUrl());
        Disposable disposable = orderService.checkLoginValidity(aDAPIHelper.getFinalUrl(), user_name, user_password)
                .subscribeOn(supervisorApplication.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse loginResponse) throws Exception {
                        loginProgress.set(View.GONE);
                        if (loginResponse.messageCode.equalsIgnoreCase("1004")) {
                            if (loginResponse.AgentDetails != null && loginResponse.AgentDetails.LocationDetails != null) {
                                String LocationId = getLocationIDFromResponse(loginResponse);
                                String AgentId = loginResponse.AgentDetails.AgentId;
                                Logs.e("***", "Login Response - " + new Gson().toJson(loginResponse));
                                if (loginResponse.AgentDetails.RoleDetails != null && loginResponse.AgentDetails.RoleDetails.size() > 0) {
                                    //checkRole(loginResponse);
                                        for (RoleDetails roleDetails : loginResponse.AgentDetails.RoleDetails) {
                                            if (roleDetails.RoleName.equalsIgnoreCase("supervisor")) {
                                                SharedPreferences pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = pref.edit();
                                                editor.putString("Location_Id", LocationId);
                                                Log.d("locationid", LocationId);
                                                editor.putString("Agent_Id", AgentId);
                                                editor.putString("Logged_Email", user_name);
                                                editor.apply();
                                                proceedToOrdersList(LocationId);
                                                break;
                                            }

                                        }

                                } else {
                                    // Toast.makeText(context, "These credential is invalid please try with valid credential", Toast.LENGTH_LONG).show();
                                    BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Alert", "The username or password you have entered is invalid. Please try again.", false, true);
                                    loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialog) {
                                          //  username = new ObservableField<>("");
                                          //  password = new ObservableField<>("");
                                            username.set("");
                                            password.set("");
                                        }
                                    });
                                    loadingFragment.show(fragmentActivity.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                                }
                            }
                            // Toast.makeText(context, loginResponse.Message, Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(context, "" + loginResponse.Message, Toast.LENGTH_LONG).show();

                            String body = (loginResponse.Message != null && loginResponse.Message.trim().length() > 0) ? (loginResponse.Message) : "Something went wrong, Please try again later";

                            BaseLoadingFragment loadingFragment = BaseLoadingFragment.newInstance("Alert", body, false, true);
                            loadingFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    username.set("");
                                    password.set("");
                                }
                            });
                            loadingFragment.show(fragmentActivity.getSupportFragmentManager(), BaseLoadingFragment.FRAGMENT_TAG);
                        }
                        Log.i("accept", "--- ResponseBody " + loginResponse.toString());
                        //Remove when you get Valid credentials
                        // proceedToOrdersList();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        messageLabel.set(context.getString(R.string.error_loading_order));
                        Toast.makeText(context, "Failed - " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                        loginProgress.set(View.GONE);
                        Log.i("accept", "--- Throwable throwable " + throwable.getMessage() + "\n" + throwable.toString());
                        throwable.printStackTrace();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void checkRole(LoginResponse loginResponse) {
        for (RoleDetails roleDetails : loginResponse.AgentDetails.RoleDetails) {
            if (roleDetails.RoleName.equalsIgnoreCase("supervisor")) {

            }
        }
    }

    String getLocationIDFromResponse(LoginResponse loginResponse) {
        String LocationId = "";
        for (int i = 0; i < loginResponse.AgentDetails.LocationDetails.size(); i++) {
            if (i == loginResponse.AgentDetails.LocationDetails.size() - 1) {
                LocationId += loginResponse.AgentDetails.LocationDetails.get(i).LocationId;
            } else {
                LocationId += loginResponse.AgentDetails.LocationDetails.get(i).LocationId + ",";
            }
        }
        return LocationId;
    }


}
