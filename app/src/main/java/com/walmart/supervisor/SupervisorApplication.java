/**
 * Copyright 2016 Erik Jhordan Rey.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.walmart.supervisor;

import android.app.Application;
import android.content.Context;

import com.walmart.supervisor.data.OrderFactory;
import com.walmart.supervisor.data.OrderService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class SupervisorApplication extends Application {

    private OrderService orderService;
    private Scheduler scheduler;
    private static List<Integer> orders = new ArrayList<>();

    private static SupervisorApplication get(Context context) {
        return (SupervisorApplication) context.getApplicationContext();
    }

    public static SupervisorApplication create(Context context) {
        return SupervisorApplication.get(context);
    }

    public OrderService getOrderService() {
        if (orderService == null) {
            orderService = OrderFactory.create();
        }
        return orderService;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }
        return scheduler;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public static List<Integer> getAppOrderIds() {
        return orders;
    }

    public static void setAppOrderIds(List<Integer> appOrderIds) {
        if (orders != null) {
            orders = appOrderIds;
        } else {
            orders = new ArrayList<>();
            orders = appOrderIds;
        }
    }
}
